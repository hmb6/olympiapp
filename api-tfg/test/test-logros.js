var server = require('../api');
var supertest = require('supertest');
var assert = require('assert');

describe('testLogros', function(){
	
	it('POST /admin/logros debe devolver un mensaje de exito', function(done){
        supertest(server)
            .post('/admin/logros')
            .field('nombre', 'test') 
            .field('descripcion', 'test')     
            .expect(200)
            .expect('Logro creado.',done);
    });
	
	it('GET /admin/logros debe devolver array de logros', function(done){
        supertest(server)
            .get('/admin/logros')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });
	
	it('GET /admin/logros/999 debe devolver de logro test', function(done){
        supertest(server)
            .get('/admin/logros/999')
            .expect('Content-Type', /json/)
			.expect(function(respuesta){
				assert(respuesta.body.nombre,'test');
                assert(respuesta.body.descripcion,'test');
			})
            .expect(200,done);
    });
	
	it('POST /admin/users/999/logros debe devolver un mensaje de exito', function(done){
        supertest(server)
            .post('/admin/users/999/logros')
            .field('LogroId', 999)     
            .expect(200)
            .expect('Logro añadido a usuario',done);
    });
	
	it('GET /admin/users/999/logros/999 debe devolver true', function(done){
        supertest(server)
            .get('/admin/users/999/logros/999')
            .expect(function(respuesta){
                assert(respuesta.body,true)
            })
            .expect(200,done);
    });
	
	
});


