var server = require('../api');
var supertest = require('supertest');
var assert = require('assert');

describe('testRetos', function(){
	
	it('POST /admin/retos debe devolver un mensaje de exito', function(done){
        supertest(server)
            .post('/admin/retos')
            .field('nombre', 'test') 
            .field('descripcion', 'test') 
            .field('duracion', 1) 
            .field('tipo', 'test') 
            .field('descanso', 1) 
            .field('progresion', 'test')			
            .expect(200)
            .expect('Reto añadido.',done);
    });
	
	it('POST /admin/retos/999/exercises debe devolver un mensaje de exito', function(done){
        supertest(server)
            .post('/admin/retos/999/exercises')
            .field('ExerciseId', 999)			
            .expect(200)
            .expect('Ejercicio añadido a reto.',done);
    });
	
	it('GET /admin/retos debe devolver array de retos', function(done){
        supertest(server)
            .get('/admin/retos')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });
	
	it('GET /admin/retos/999/exercises debe devolver de array de ejercicios del reto', function(done){
        supertest(server)
            .get('/admin/retos/999/exercises')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });

	
	
});


