var server = require('../api');
var supertest = require('supertest');
var assert = require('assert');

describe('testComentarios ', function(){
	
	it('POST /admin/trainings/999/comments debe devolver un mensaje de exito', function(done){
        supertest(server)
            .post('/admin/trainings/999/comments')
            .field('bodycomment', 'test2') 
            .field('nick', 'test') 
            .field('UserId', 999)     
            .expect(200)
            .expect('Comentario añadido.',done);
    });
	
	it('GET /admin/trainings/999/comments debe devolver array de categorias con mets', function(done){
        supertest(server)
            .get('/admin/trainings/999/comments')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });

});