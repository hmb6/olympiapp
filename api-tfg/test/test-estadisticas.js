var server = require('../api');
var supertest = require('supertest');
var assert = require('assert');

describe('testEstadisticas ', function(){
	
	it('POST /admin/users/999/stats debe devolver un mensaje de exito', function(done){
        supertest(server)
            .post('/admin/users/999/stats')
            .field('kcal', 72.5) 
            .field('tiempo', 2542) 
            .field('TrainingId', 999)     
            .expect(200)
            .expect('Estadistica añadida.',done);
    });
	
	it('POST /admin/users/999/pesos debe devolver un mensaje de exito', function(done){
        supertest(server)
            .post('/admin/users/999/pesos')
            .field('peso', 72.5)    
            .expect(200)
            .expect('Estadistica de peso añadida.',done);
    });
	
	it('GET /admin/users/999/stats debe devolver array de estadisticas', function(done){
        supertest(server)
            .get('/admin/users/999/stats')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });
	
	it('GET /admin/users/999/pesos debe devolver array de estadisticas de pesos', function(done){
        supertest(server)
            .get('/admin/users/999/pesos')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });
	
	
});


