var server = require('../api');
var supertest = require('supertest');
var assert = require('assert');

describe('testUsers', function(){
    
    it('GET /admin/users debe devolver array de users', function(done){
        supertest(server)
            .get('/admin/users')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });

    it('GET /admin/users/999 debe devolver el usuario test', function(done){
        supertest(server)
            .get('/admin/users/999')
            .expect('Content-Type', /json/)            
            .expect(function(respuesta){
                assert(respuesta.body.nick,'test');
                assert(respuesta.body.password,'test');
                assert(respuesta.body.email,'test@olympiapp.com');
                assert(respuesta.body.peso,75);
            })
            .expect(200,done);
    });

    it('POST /admin/users debe devolver un mensaje de éxito', function(done){
        supertest(server)
            .post('/admin/users')
            .field('nick', 'test2')
            .field('email', 'test2@olympiapp.com')        
            .expect(200)
            .expect('User añadido.',done);
    });   

    it('POST /admin/users/find debe devolver un usuario buscado por nick', function(done){
        supertest(server)
            .post('/admin/users/find')
            .field('nick', 'test')
            .expect('Content-Type', /json/)            
            .expect(function(respuesta){
                assert(respuesta.body.nick,'test');
                assert(respuesta.body.password,'test');
                assert(respuesta.body.email,'test@olympiapp.com');
                assert(respuesta.body.peso,75);
            })       
            .expect(200,done);
    });  

    it('PUT /admin/users/999 debe actualizar el usuario test', function(done){
        supertest(server)
            .put('/admin/users/9999')
            .field('nick', 'testUpdate') 
            .field('password', 'e') 
            .field('email', 'i') 
            .field('peso', 1000)          
            .expect(200)
            .expect('Usuario actualizado.',done)
    });

    it('GET /admin/users/9999 debe devolver el user actualizado por el PUT',function(done){
        supertest(server)
            .get('/admin/users/9999')  
            .expect(function(respuesta){
                assert(respuesta.body.nick,'testUpdate');
                assert(respuesta.body.password,'e');
                assert(respuesta.body.email,'i');
                assert(respuesta.body.peso,1000);
            })
            .expect(200,done)
    }); 

    it('DELETE /admin/users/9999 debe eliminar el usuario test 9999', function(done){
        supertest(server)
            .del('/admin/users/9999')  
            .expect(200)
            .expect('User eliminado.',done)
    });

    it('GET /admin/users/9999 debe dar status 404 por haber sido eliminado',function(done){
        supertest(server)
            .get('/admin/users/9999')  
            .expect(404)
            .expect('No se encuentra el usuario',done)
    }); 

    it('POST /admin/users/999/amigos debe devolver un mensaje de éxito', function(done){
        supertest(server)
            .post('/admin/users/999/amigos')
            .field('UserEmisor', 999)
            .field('UserAgregado', 0)        
            .expect(200)
            .expect('User añadido.',done);
    });

    it('GET /admin/users/999/amigos/0 debe devolver true', function(done){
        supertest(server)
            .get('/admin/users/999/amigos/0') 
            .expect(function(respuesta){
                assert(respuesta.body,true)
            })      
            .expect(200,done)
    });


});
