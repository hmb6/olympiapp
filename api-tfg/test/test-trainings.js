var server = require('../api');
var supertest = require('supertest');
var assert = require('assert');

describe('testTrainings', function(){
    
    it('GET /admin/trainings debe devolver array de trainings', function(done){
        supertest(server)
            .get('/admin/trainings')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });
    
    it('GET /admin/trainingsPublicos debe devolver array de trainings publicos', function(done){
        supertest(server)
            .get('/admin/trainingsPublicos')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });

    it('GET /admin/trainingsPred debe devolver array de trainings predefinidos', function(done){
        supertest(server)
            .get('/admin/trainingsPred')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });

    it('POST /admin/trainingsPers debe devolver array de trainings personalizados', function(done){
            supertest(server)
                .post('/admin/trainingsPers')
                .field('UserId', '0')
                .expect('Content-Type', /json/)
                .expect(200,done);
        });

    it('GET /admin/trainings/999 debe devolver el training test', function(done){
        supertest(server)
            .get('/admin/trainings/999')
            .expect('Content-Type', /json/)            
            .expect(function(respuesta){
                assert(respuesta.body.nombre,'test');
                assert(respuesta.body.descripcion,'test');
                assert(respuesta.body.publico,'te');
            })
            .expect(200,done);
    });

    it('POST /admin/trainings debe devolver un mensaje de éxito', function(done){
        supertest(server)
            .post('/admin/trainings')
            .field('nombre', 'test3')
            .field('descripcion', 'test2@olympiapp.com') 
            .field('publico', 'test2')
            .field('dificultad', 'test2')
            .field('UserId', 999)       
            .expect(200)
            .expect('Entrenamiento añadido.',done);
    });   

    it('PUT /admin/trainings/9999 debe actualizar el entrenamiento test', function(done){
        supertest(server)
            .put('/admin/trainings/9999')
            .field('nombre', 'testUpdate') 
            .field('descripcion', 'e') 
            .field('publico', 'i') 
            .field('dificultad', 'p') 
            .field('votos', 2555)          
            .expect(200)
            .expect('Entrenamiento actualizado.',done)
    });

    it('GET /admin/trainings/9999 debe devolver el training actualizado por el PUT',function(done){
        supertest(server)
            .get('/admin/trainings/9999')  
            .expect(function(respuesta){
                assert(respuesta.body.nombre,'testUpdate');
                assert(respuesta.body.descripcion,'e');
                assert(respuesta.body.publico,'i');
                assert(respuesta.body.dificultad,'p');
                assert(respuesta.body.votos,2555);
            })
            .expect(200,done)
    }); 

    it('DELETE /admin/trainings/9999 debe eliminar el training test 9999', function(done){
        supertest(server)
            .del('/admin/trainings/9999')  
            .expect(200)
            .expect('Training eliminado.',done)
    });

    it('GET /admin/trainings/9999 debe dar status 404 por haber sido eliminado',function(done){
        supertest(server)
            .get('/admin/trainings/9999')  
            .expect(404)
            .expect('No se encuentra el entrenamiento',done)
    }); 

    it('GET /admin/exercises debe devolver un array de ejercicios',function(done){
        supertest(server)
            .get('/admin/exercises')  
            .expect('Content-Type', /json/)
            .expect(200,done)
    });

    it('GET /admin/exercises/999 debe devolver detalle de ejercicio test',function(done){
        supertest(server)
            .get('/admin/exercises/999')  
            .expect('Content-Type', /json/)
            .expect(function(respuesta){
                assert(respuesta.body.nombre,'test');
                assert(respuesta.body.descripcion,'test');
                assert(respuesta.body.tipo,'test');
            })
            .expect(200,done)
    });

    it('POST /admin/exercises debe devolver un mensaje de éxito', function(done){
        supertest(server)
            .post('/admin/exercises')
            .field('nombre', 'test2')
            .field('descripcion', 'test2')
            .field('tipo', 'test')
            .field('UserId', 999)        
            .expect(200)
            .expect('Ejercicio añadido.',done);
    });

    it('PUT /admin/exercises/9999 debe actualizar el ejercicio test', function(done){
        supertest(server)
            .put('/admin/exercises/9999')
            .field('nombre', 'testUpdate') 
            .field('descripcion', 'e') 
            .field('tipo', 'i') 
            .field('img', 2)          
            .expect(200)
            .expect('Ejercicio actualizado.',done)
    });

    it('GET /admin/exercises/9999 debe devolver el ejercicio actualizado por el PUT',function(done){
        supertest(server)
            .get('/admin/exercises/9999')  
            .expect(function(respuesta){
                assert(respuesta.body.nombre,'testUpdate');
                assert(respuesta.body.descripcion,'e');
                assert(respuesta.body.tipo,'i');
                assert(respuesta.body.img,2);
            })
            .expect(200,done)
    }); 

    it('DELETE /admin/exercises/9999 debe eliminar el training test 9999', function(done){
        supertest(server)
            .del('/admin/exercises/9999')  
            .expect(200)
            .expect('Ejercicio eliminado.',done)
    });

    it('GET /admin/exercises/9999 debe dar status 404 por haber sido eliminado',function(done){
        supertest(server)
            .get('/admin/exercises/9999')  
            .expect(404)
            .expect('No se encuentra el ejercicio',done)
    }); 

    it('POST /admin/trainings/999 debe devolver un mensaje de éxito', function(done){
        supertest(server)
            .post('/admin/trainings/999')
            .field('repeticiones', 999)
            .field('series', 999)
            .field('peso', 999)
            .field('tiempo', 999)
            .field('ExerciseId', 999)        
            .expect(200)
            .expect('Ejercicio asignado a entrenamiento.',done);
    });

    it('GET /admin/trainings/999/exercises/999 debe devolver el detalle del ejercicio añadido en el anterior',function(done){
        supertest(server)
            .get('/admin/trainings/999/exercises/999')  
            .expect(function(respuesta){
                assert(respuesta.body.repeticiones,999);
                assert(respuesta.body.series,999);
                assert(respuesta.body.peso,999);
                assert(respuesta.body.tiempo,999);
            })
            .expect(200,done)
    }); 

    it('POST /admin/users/999/suscripciones debe devolver un mensaje de éxito', function(done){
            supertest(server)
                .post('/admin/users/999/suscripciones')
                .field('TrainingId', 999)       
                .expect(200)
                .expect('Suscripcion creada con exito.',done);
        });
    
    it('GET /admin/users/999/suscripciones debe dar un arrat de suscripciones',function(done){
        supertest(server)
            .get('/admin/users/999/suscripciones')  
            .expect('Content-Type', /json/)
            .expect(200,done)
    }); 
    
    it('GET /admin/users/999/suscripciones/999 debe devolver true',function(done){
            supertest(server)
                .get('/admin/users/999/suscripciones/999')  
                .expect(function(respuesta){
                    assert(respuesta.body,true)
                })
                .expect(200,done)
        });



});
