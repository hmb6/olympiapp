var server = require('../api');
var supertest = require('supertest');
var assert = require('assert');

describe('testMensajes', function(){
	
	it('POST /admin/users/999/mensajes debe devolver un mensaje de exito', function(done){
        supertest(server)
            .post('/admin/users/999/mensajes')
            .field('nickEmisor', 'Admin') 
            .field('nickReceptor', 'test') 
			.field('UserEmisor',0)
            .field('bodymessage', 'test')     
            .expect(200)
            .expect('Mensaje enviado.',done);
    });
	
	it('GET /admin/users/999/mensajesEnviados debe devolver array de mensajes', function(done){
        supertest(server)
            .get('/admin/users/999/mensajesEnviados')
            .expect(200,done);
    });
	
	it('GET /admin/users/999/mensajesRecibidos debe devolver array de mensajes', function(done){
        supertest(server)
            .get('/admin/users/999/mensajesRecibidos')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });
	
	
});


