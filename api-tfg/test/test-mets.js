var server = require('../api');
var supertest = require('supertest');
var assert = require('assert');

describe('testMets', function(){

	it('GET /admin/mets debe devolver array de categorias con mets', function(done){
        supertest(server)
            .get('/admin/mets')
            .expect('Content-Type', /json/)
            .expect(200,done);
    });
	
	 it('POST /admin/mets debe devolver un objeto json tipo MET', function(done){
        supertest(server)
            .post('/admin/mets')
            .field('categoria', 'test2')
            .field('mets', 8)        
            .expect(200)
            .expect('Categoria con mets añadida.',done);
    });   

});