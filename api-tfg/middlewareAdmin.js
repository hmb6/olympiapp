// middleware.js
var jwt = require('jwt-simple');  
var moment = require('moment');  
var config = require('./configAdmin');

exports.ensureAuthenticated = function(req, res, next) {  
    if(!req.headers.authorization) {
        return res
        .status(403)
        .send({message: "Tu petición no tiene cabecera de autorización"});
    }

    var token = req.headers.authorization.split(" ")[1];
    try{
        var payload = jwt.decode(token, config.TOKEN_SECRET);
    } catch(err){
       if(err.message.indexOf('expired') > -1){
            return res
                .status(401)
                .send({message: "Token caducado"});
        }
    }

    //req.user = payload.sub;
    next();
}