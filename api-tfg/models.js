var Sequelize = require('sequelize');
var sequelize = new Sequelize('apidb', '', '', {
    dialect: 'sqlite',
    storage: 'apidb.sqlite'
});

exports.User = sequelize.define('User', {
    nick: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    peso: Sequelize.DOUBLE
}, {
    name: {singular: 'User', plural: 'Users'}
});


exports.Training = sequelize.define('Training',{
    nombre: Sequelize.STRING,
    descripcion: Sequelize.STRING,
    publico: Sequelize.STRING,
    dificultad: Sequelize.STRING,
    votos: Sequelize.INTEGER
},{
    name: {singular: 'Training', plural: 'Trainings'}
});

exports.Exercise = sequelize.define('Exercise',{
    nombre: Sequelize.STRING,
    descripcion: Sequelize.STRING,
    tipo: Sequelize.STRING,
    img: Sequelize.INTEGER
},{
    name: {singular: 'Exercise', plural: 'Exercises'}
});

exports.Ejer_contenidos_entren = sequelize.define('Ejer_contenidos_entren',{
    repeticiones : Sequelize.INTEGER,
    series: Sequelize.INTEGER,
    peso: Sequelize.STRING,
    tiempo: Sequelize.INTEGER
},{
    name: {singular: 'Ejer_contenidos_entren',plural: 'Ejers_contenidos_entrens'}
});

exports.Met = sequelize.define('Met',{
    categoria: Sequelize.STRING,
    mets: Sequelize.DOUBLE
},{
    name: {singular: 'Met', plural: 'Mets'}
});


exports.Comentario = sequelize.define('Comentario',{
    nick: Sequelize.STRING,
    bodycomment: Sequelize.TEXT
}, {
    name: {singular: 'Comentario', plural: 'Comentarios'}
});

exports.User_vota_entren = sequelize.define('User_vota_entren',{
},{
    name: {singular: 'User_vota_entren', plural: 'User_vota_entrens'}
});

exports.Estadistica = sequelize.define('Estadistica',{
    tiempo: Sequelize.INTEGER,
    kcal: Sequelize.DOUBLE
},{
    name: {singular: 'Estadistica', plural: 'Estadisticas'}
});

exports.Peso = sequelize.define('Peso',{
    peso: Sequelize.DOUBLE
},{
    name: {singular: 'Peso', plural: 'Pesos'}
});

exports.Amigo = sequelize.define('Amigo',{
},{
    name: {singular: 'Amigo', plural: 'Amigoss'}
});

exports.Mensaje = sequelize.define('Mensaje',{
    nickEmisor: Sequelize.STRING,
    nickReceptor: Sequelize.STRING,
    bodymessage: Sequelize.TEXT
},{
    name: {singular: 'Mensaje', plural: 'Mensajes'}
});

exports.Logro = sequelize.define('Logro',{
    nombre: Sequelize.STRING,
    descripcion: Sequelize.TEXT
},{
    nombre: { singular: 'Logro', plural: 'Logros'}
});

exports.User_tiene_logro = sequelize.define('User_tiene_logro',{
},{
    nombre: {singular: 'User_tiene_logro', plural: 'User_tiene_logros'}
});

exports.Reto = sequelize.define('Reto',{
    nombre: Sequelize.STRING,
    descripcion: Sequelize.TEXT,
    duracion: Sequelize.INTEGER,
    tipo: Sequelize.STRING,
    descanso: Sequelize.INTEGER,
    progresion: Sequelize.STRING
},{
    nombre: {singular: 'reto', plural: 'Retos'}
});

exports.Reto_tiene_ejer = sequelize.define('Reto_tiene_ejer',{
},{
    nombre: {singular: 'Reto_tiene_ejer', plural: 'Reto_tiene_ejers'}
});

exports.User_suscrito_entren = sequelize.define('User_suscrito_entren',{
},{
    nombre:{singular:'User_suscrito_entren', plural: 'User_suscrito_entrens'}
});