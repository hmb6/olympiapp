//Servidor API rest - Backend
var express = require('express');
var fileUpload = require('express-fileupload');
var fs = require("fs");
var moment = require('moment');
var jwt = require('jwt-simple');
var cors = require('cors')
var Sequelize = require('sequelize');
var bcrypt = require('bcrypt');
var Models = require('./models.js') ;
var sequelize = new Sequelize('apidb', '', '', {
    dialect: 'sqlite',
    storage: 'apidb.sqlite'
});
var methodOverride = require('method-override');
var path = require('path');
var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');

var config = require('./config');
var configAdmin = require('./configAdmin');

var service = require('./services');
var serviceAdmin = require('./servicesAdmin');

var middleware = require('./middleware');
var middlewareAdmin = require('./middlewareAdmin');




//Modelos
var User = Models.User;
var Training = Models.Training;
var Exercise = Models.Exercise;
var Ejer_contenidos_entren = Models.Ejer_contenidos_entren;
var Met = Models.Met;
var Comentario = Models.Comentario;
var User_vota_entren = Models.User_vota_entren;
var Estadistica = Models.Estadistica;
var Peso = Models.Peso;
var Amigo = Models.Amigo;
var Mensaje = Models.Mensaje;
var Logro = Models.Logro;
var User_tiene_logro = Models.User_tiene_logro;
var Reto = Models.Reto;
var Reto_tiene_ejer = Models.Reto_tiene_ejer;
var User_suscrito_entren = Models.User_suscrito_entren;

////////////////////////////////RELACIONES DE MODELOS///////////////////
//Entren_creado_por
User.hasMany(Training);
Training.belongsTo(User, {
    //constraints:false,
    foreignKey: 'UserId',
    onDelete: 'cascade' 
});

//Ejer_generado_por
User.hasMany(Exercise);
Exercise.belongsTo(User, {
    //constraints: false,
    foreignKey: 'UserId',
    onDelete: 'cascade'
});

//Ejer_contenidos_entren
//Training.belongsToMany(Exercise, {through: Ejer_contenidos_entren});
Training.belongsToMany(Exercise, {  through: Ejer_contenidos_entren,
                                    foreignKey: 'ExerciseId',
                                    onDelete: 'cascade'});

Exercise.belongsToMany(Training, {  through: Ejer_contenidos_entren,
                                    foreignKey: 'TrainingId',
                                    onDelete: 'cascade'});

 //Relaciones de usuarios y trainings con comentarios
User.hasMany(Comentario);
Training.hasMany(Comentario,{
                                onDelete: 'cascade'
                            });
Comentario.belongsTo(User, {
  constraints:false
});
Comentario.belongsTo(Training,{
    constraints:false
});

User.belongsToMany(Training, {      through: User_vota_entren,
                                    foreignKey: 'TrainingId',
                                    onDelete: 'cascade'
})
Training.belongsToMany(User, {      through: User_vota_entren,
                                    foreignKey: 'UserId',
                                    onDelete: 'cascade'
});

User.belongsToMany(Training, {      through: User_suscrito_entren,
                                    foreignKey: 'TrainingId',
                                    onDelete: 'cascade'
})
Training.belongsToMany(User, {      through: User_suscrito_entren,
                                    foreignKey: 'UserId',
                                    onDelete: 'cascade'
});

User.hasMany(Estadistica);
Estadistica.belongsTo(User, {
    constraints:false
});
Training.hasMany(Estadistica);
Estadistica.belongsTo(Training, {
    constraints:false
});

User.hasMany(Peso);
Peso.belongsTo(User,{
    constraints:false
});

//User.hasMany(Amigo);
Amigo.belongsTo(User,{foreignKey: 'UserEmisor'});
Amigo.belongsTo(User,{foreignKey: 'UserAgregado'});

//User.hasMany(Mensaje);
Mensaje.belongsTo(User,{foreignKey: 'UserEmisor'});
Mensaje.belongsTo(User,{foreignKey: 'UserReceptor'});

User.belongsToMany(Logro,{      through: User_tiene_logro,
                                    foreignKey: 'LogroId',
                                    onDelete: 'cascade'
});
Logro.belongsToMany(User,{      through: User_tiene_logro,
                                    foreignKey: 'UserId',
                                    onDelete: 'cascade'
});

Reto.belongsToMany(Exercise,{      through: Reto_tiene_ejer,
                                    foreignKey: 'ExerciseId',
                                    onDelete: 'cascade'
});
Exercise.belongsToMany(Reto,{      through: Reto_tiene_ejer,
                                    foreignKey: 'RetoId',
                                    onDelete: 'cascade'
});


//IMPLEMENTACION DEL API REST
var app = express();
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
      //respond with 200
      res.send(200);
    }
    else {
    //move on
      next();
    }
});


app.options("/*", function(req, res, next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.send(200);
});

app.use(methodOverride());
var bodyParser = require('body-parser').json();

//autentificacion
app.use('/api/*',middleware.ensureAuthenticated);
//app.use('/admin/*',middlewareAdmin.ensureAuthenticated);

// ## CORS middleware
app.use('*',cors());

app.use(fileUpload());


//inicio
app.get('/', function(pet, res){
    res.send("Hola soy express");
});
 
//*********************************************************************************ADMINISTRACION







//////////////////////////////////////////////////////CRUD USUARIOS
//Listado users
app.get('/admin/users',function(pet, resp){
    User.findAll({order: 'nick ASC',where:{id:{gt: 0} }}).then(function(results){
        resp.send(results);
    });
});
 
//Creacion de usuario
app.post('/admin/users',bodyParser,function(pet, resp){
    User.create({
        nick: pet.body.nick,
        email: pet.body.email,
        password: pet.body.password
    }).then(function(){
        resp.status(200).send("User añadido.")
    });
});

//Detalle de usuario
app.get('/admin/users/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User.findById(pet.params.id).then(function(user){
            if(!user){
                resp.status(404).send("No se encuentra el usuario")
            }
            else
            {       
                resp.status(200).send(user) 
            }
         
        })
    }
});


//Detalle de usuario (pornombre)
app.post('/admin/users/find',bodyParser,function(pet,resp){
    
        User.findOne({where: {nick: pet.body.nick}}).then(function(user){
            if(!user){
                resp.status(404).send("No se encuentra el usuario")
            }
            else
            {       
                resp.status(200).send(user) 
            }
         
        })
    });

//Actualizar usuario
app.put('/admin/users/:id',bodyParser,function(pet,resp){
	if(isNaN(pet.params.id))
	{
		resp.status(400).send("Parametro erroneo.")
	}
	else
	{
	User.findById(pet.params.id).then(function(user){
		if(!user){
			resp.status(404).send("No se encuentra el profesor a borrar")
		}
		else
		{
                user.nick= pet.body.nick,
                user.email= pet.body.email,
                user.password= pet.body.password,
                user.peso = pet.body.peso,
				user.save().then(function(){
					resp.status(200).send("Usuario actualizado.")
				})
		}
		})
	}
});

//Borrar un usuario
app.delete('/admin/users/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User.findById(pet.params.id).then(function(user){
            if(!user){
                resp.status(404).send("No se encuentra el user a borrar")
            }
            else
            {
                user.destroy().then(function(){
                    resp.status(200).send("User eliminado.")
                });
            }
         
        })
    }
});

//Añadir USUARIO a AMIGOS
app.post('/admin/users/:id/amigos',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Amigo.create({
            UserEmisor: pet.params.id,
            UserAgregado: pet.body.UserAgregado
        }).then(function(){
             resp.status(200).send("User añadido.")
        });
    }
});

//Comprobar amistad
app.get('/admin/users/:id/amigos/:amigo',function(pet,resp){
    if(isNaN(pet.params.id)||isNaN(pet.params.amigo))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Amigo.findOne({where:{UserEmisor: pet.params.id,
                              UserAgregado: pet.params.amigo}})
        .then(function(result){
            if(!result)
            {
                return resp.status(200).send(false);
            }
             resp.status(200).send(true);
        });
    }
});

//////////////////////////////////////////////////////CRUD ENTRENAMIENTOS

//Lista de ENTRENAMIENTOS
app.get('/admin/trainings',function(pet, resp){
    Training.findAll().then(function(results){
        resp.send(results);
    })});


app.post('/admin/trainings', bodyParser,function(pet, resp){
    Training.create({
        nombre: pet.body.nombre,
        descripcion: pet.body.descripcion,
        publico: pet.body.publico,
        dificultad: pet.body.dificultad,
        UserId: pet.body.UserId
    }).then(function(){
        resp.status(200).send("Entrenamiento añadido.")
    });
});

//Lista de ENTRENAMIENTOS PREDEFINIDOS
app.get('/admin/trainingsPred',function(pet, resp){
    Training.findAll({where:{publico:"pr"}}).then(function(results){
        resp.send(results);
    })});

//TOP 10 ENTRENAMIENTOS PUBLICOS
app.get('/admin/trainingsPublicos',function(pet, resp){
    Training.findAll({limit:10,order: 'votos DESC',where:{publico:"si"}}).then(function(results){
        resp.send(results);
    })});

//Detalle de un ENTRENAMIENTO
app.get('/admin/trainings/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Training.findById(pet.params.id).then(function(training){
            if(!training){
                resp.status(404).send("No se encuentra el entrenamiento")
            }
            else
            {       
                resp.status(200).send(training) 
            }
         
        })
    }
});

//Actualizacion de ENTRENAMIENTOS
app.put('/admin/trainings/:id', bodyParser,function(pet,resp){
	if(isNaN(pet.params.id))
	{
		resp.status(400).send("Parametro erroneo.")
	}
	else
	{
	Training.findById(pet.params.id).then(function(training){
		if(!training){
			resp.status(404).send("No se encuentra el entrenamiento a modificar")
		}
		else
		{
                training.nombre = pet.body.nombre,
                training.descripcion = pet.body.descripcion,
                training.publico = pet.body.publico,
                training.dificultad = pet.body.dificultad,
                training.votos = pet.body.votos,
				training.save().then(function(){
					resp.status(200).send("Entrenamiento actualizado.")
				})
		}
		})
	}
});

//Borrado de ENTRENAMIENTOS
app.delete('/admin/trainings/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Training.findById(pet.params.id).then(function(training){
            if(!training){
                resp.status(404).send("No se encuentra el entrenamiento a borrar")
            }
            else
            {
                //Borrado en cascada
                Ejer_contenidos_entren.destroy({where:{TrainingId: pet.params.id}}).then(()=>{
                    Training.findById(pet.params.id).then(function(training){
                        training.destroy().then(function(){
                                            resp.status(200).send("Training eliminado.")
                                        });
                    });
            });
        }
    });
}});


//Detalle de un ENTRENAMIENTO
app.get('/admin/trainings/:id/exercises',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Ejer_contenidos_entren.findAll({where: {TrainingId: pet.params.id}}).then(function(result){
            if(!result){
                resp.status(404).send("Este entrenamiento no tiene ejercicios")
            }
            else
            {       
                resp.status(200).send(result) 
            }
         
        })
    }
});


//Lista de ENTRENAMIENTOS PERSONALIZADOS
app.post('/admin/trainingsPers', bodyParser,function(pet, resp){ 
        Training.findAll({where: {UserId: pet.body.UserId}}).then(function(result){
            if(!result){
                resp.status(404).send("No se encontraron entrenamientos")
            }
            else
            {       
                resp.status(200).send(result) 
            }       
    });
});

//Crear un ENTRENAMIENTO PERSONALIZADO
app.post('/admin/trainingsPers/:id', bodyParser,function(pet, resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Training.create({
            nombre: pet.body.nombre,
            descripcion: pet.body.descripcion,
            publico: pet.body.publico,
            dificultad: pet.body.dificultad,
            UserId: pet.params.id
        }).then(function(){
            resp.status(200).send("Ejercicio añadido.")
        });
    }
});

//Comprobar voto ENTRENAMIENTO
app.get('/admin/trainings/:id/voto/:usu',function(pet,resp){
 
    if(isNaN(pet.params.id)||isNaN(pet.params.usu))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_vota_entren.findOne({where:{UserId: pet.params.usu,TrainingId: pet.params.id}}).then((result)=>{
            if(!result){
                resp.status(200).send(false);
            }else{
                resp.status(200).send(true);
            }                            })
    }
});

//Votar ENTRENAMIENTO
app.post('/admin/trainings/:id/voto/:usu',function(pet,resp){
    if(isNaN(pet.params.id)||isNaN(pet.params.usu))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_vota_entren.create({
            UserId: pet.params.usu,
            TrainingId: pet.params.id
        }).then(function(){
            resp.status(200).send("Training votado.")
        });
    }
})

//////////////////////////////////////////////////////CRUD EJERCICIOS
//Creacion de EJERCICIOS
app.post('/admin/exercises', bodyParser,function(pet, resp){
    Exercise.create({
        nombre: pet.body.nombre,
        descripcion: pet.body.descripcion,
        tipo: pet.body.tipo,
        UserId: pet.body.UserId
    }).then(function(){
        resp.status(200).send("Ejercicio añadido.")
    });
});



//Lista de EJERCICIOS predefinidos
app.get('/admin/exercises',function(pet, resp){
    Exercise.findAll({where:{UserId: 0}}).then(function(results){
        resp.send(results);
    })});

//Detalle de un EJERCICIO
app.get('/admin/exercises/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Exercise.findById(pet.params.id).then(function(exercise){
            if(!exercise){
                resp.status(404).send("No se encuentra el ejercicio")
            }
            else
            {       
                resp.status(200).send(exercise) 
            }
         
        })
    }
});

//Actualizacion de EJERCICIOS
app.put('/admin/exercises/:id', bodyParser,function(pet,resp){
	if(isNaN(pet.params.id))
	{
		resp.status(400).send("Parametro erroneo.")
	}
	else
	{
	Exercise.findById(pet.params.id).then(function(exercise){
		if(!exercise){
			resp.status(404).send("No se encuentra el ejercicio a modificar")
		}
		else
		{
                exercise.nombre = pet.body.nombre,
                exercise.descripcion = pet.body.descripcion,
                exercise.tipo = pet.body.tipo,
                exercise.img = pet.body.img,
				exercise.save().then(function(){
					resp.status(200).send("Ejercicio actualizado.")
				})
		}
		})
	}
});

//Borrado de EJERCICIOS
app.delete('/admin/exercises/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Exercise.findById(pet.params.id).then(function(exercise){
            if(!exercise){
                resp.status(404).send("No se encuentra el ejercicio a borrar")
            }
            else
            {   //Borrado en cascada
                Ejer_contenidos_entren.destroy({where:{ExerciseId: pet.params.id}}).then(()=>{
                    Exercise.findById(pet.params.id).then(function(exercise){
                        exercise.destroy().then(function(){
                                            resp.status(200).send("Ejercicio eliminado.")
                                        });
                    });
            });
        }
    });
}});


//Lista EJERCICIOS de un usuario
app.post('/admin/exercises/findByUser',bodyParser,function(pet,resp){
    Exercise.findAll({where:{UserId: pet.body.UserId}}).then(function(exercises){
        if(!exercises){
                resp.status(404).send("No se encuentra ejercicios")
            }
            else
            {       
                resp.status(200).send(exercises)
            }
    });
});

//////////////////////////////////////////////////////RELACION EJERCICIOS-ENTRENAMIENTOS
//Asignar EJERCICIO a ENTRENAMIENTO
app.post('/admin/trainings/:id', bodyParser,function(pet, resp){
        Ejer_contenidos_entren.create({
            repeticiones: pet.body.repeticiones,
            series: pet.body.series,
            peso: pet.body.peso,
            tiempo: pet.body.tiempo,
            TrainingId: pet.params.id,
            ExerciseId: pet.body.ExerciseId,
        }).then(function(){
        resp.status(200).send("Ejercicio asignado a entrenamiento.")
    });
});

//Detalle de un EJERCICIO en un ENTRENAMIENTO
app.get('/admin/trainings/:id/exercises/:idEx',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Ejer_contenidos_entren.findOne({where: {TrainingId: pet.params.id,
                                                ExerciseId: pet.params.idEx}})
        .then(function(exercise){
            if(!exercise){
                resp.status(404).send("No se encuentra el ejercicio")
            }
            else
            {       
                resp.status(200).send(exercise)
            }
         
        })
    }
});

//Lista de suscripciones
app.get('/admin/users/:id/suscripciones',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_suscrito_entren.findAll({where: {UserId:pet.params.id}})
        .then(function(results){
            if(!results){
                resp.status(404).send("No se encuentran suscripciones")
            }
            else
            {       
                resp.status(200).send(results)
            }
         
        })
    }
});

//Suscripcion USUARIO a ENTRENAMIENTO PUBLICO
app.post('/admin/users/:id/suscripciones',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
       User_suscrito_entren.create({
           UserId: pet.params.id,
           TrainingId: pet.body.TrainingId
       }).then(function(){
           resp.status(200).send("Suscripcion creada con exito.");
       });
    }
});

//Eliminar suscripcion
app.delete('/admin/users/:id/suscripciones/:entren',function(pet,resp){
    if(isNaN(pet.params.id)||isNaN(pet.params.entren))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_suscrito_entren.delete({where:{UserId: pet.params.id,
                                            TrainingId:pet.params.entren}})
        .then(function(){
            resp.status(200).send("Suscripcion borrada.");
        });
    }
});

//ComprobarSuscripcion
app.get('/admin/users/:id/suscripciones/:entren',function(pet,resp){
    if(isNaN(pet.params.id)||isNaN(pet.params.entren))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_suscrito_entren.findOne({where:{UserId: pet.params.id,
                                            TrainingId:pet.params.entren}})
        .then(function(results){
            if(!results){
                return resp.status(200).send(false);
            }
            else{
                resp.status(200).send(true);
            }
        });
    }
});

//////////////////////////////////////////////////////CRUD METS
//añadir categoria con mets
app.post('/admin/mets', bodyParser,function(pet, resp){
    Met.create({
        categoria: pet.body.categoria,
        mets: pet.body.mets,
    }).then(function(){
        resp.status(200).send("Categoria con mets añadida.")
    });
});

//ver un met concreto 
app.post('/admin/mets', bodyParser,function(pet, resp){ 
        Met.findOne({where: {categoria: pet.body.categoria}})
        .then(function(met){
            if(!met){
                resp.status(404).send("No se encuentra la categoria")
            }
            else
            {       
                resp.status(200).send(met)
            }
         
        })
});

app.get('/admin/mets',function(pet,resp){
    Met.findAll().then(function(results){
        if(!results){
                resp.status(404).send("No se encontraron mets.")
        }else{
            resp.status(200).send(results);
        }
    })
})


//////////////////////////////////////////////////////CRUD COMENTARIOS
//Listado de comentarios de un training
app.get('/admin/trainings/:id/comments', function(pet, resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
    Comentario.findAll({where: { TrainingId:pet.params.id }}).then(function(comments){
        if(!comments){
                resp.status(404).send("No se encontraron comentarios en este entrenamiento.")
        }else{
            resp.status(200).send(comments);
        }
        });
    }
});



//Creacion de un comentario
app.post('/admin/trainings/:id/comments', bodyParser, function(pet, resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
        Comentario.create({
            bodycomment: pet.body.bodycomment,
            nick: pet.body.nick,
            UserId: pet.body.UserId,
            TrainingId: pet.params.id
        }).then(function(){
            resp.status(200).send("Comentario añadido.")
        });
    }
});

//////////////////////////////////////////////////////CRUD ESTADISTICAS
app.get('/admin/users/:id/stats',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
    Estadistica.findAll({order: 'createdAt ASC',where: { UserId:pet.params.id }}).then(function(stats){
        if(!stats){
                resp.status(404).send("No se han encontrado estadisticas.")
        }else{
            resp.status(200).send(stats);
        }
        });
    }
});


//Crear ESTADISTICA
app.post('/admin/users/:id/stats', bodyParser, function(pet, resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
        Estadistica.create({
            kcal: pet.body.kcal,
            tiempo: pet.body.tiempo,
            UserId: pet.params.id,
            TrainingId: pet.body.TrainingId
        }).then(function(){
            resp.status(200).send("Estadistica añadida.")
        });
    }
});

//Crear registro de peso
app.post('/admin/users/:id/pesos',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
        Peso.create({
            peso: pet.body.peso,
            UserId: pet.params.id
        }).then(function(){
            resp.status(200).send("Estadistica de peso añadida.")
        });
    }
});

//Lista de pesos de un usuario
app.get('/admin/users/:id/pesos',function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Peso.findAll({order: 'createdAt ASC',where: {UserId:pet.params.id}})
            .then(function(results){
                resp.status(200).send(results);
            });
        }
});

///////////////////////////////////////////////CRUD MENSAJESPRIVADOS
//Crear mensaje privado
app.post('/admin/users/:id/mensajes',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Mensaje.create({
                bodymessage: pet.body.bodymessage,
                nickEmisor: pet.body.nick,
                nickReceptor: pet.body.nickReceptor,
                UserEmisor: pet.body.UserEmisor,
                UserReceptor: pet.params.id
            })
            .then(function(){
                resp.status(200).send("Mensaje enviado.");
            });
        }
});

//Listar mensajes enviados
app.get('/admin/users/:id/mensajesEnviados',function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Mensaje.findAll({
                order: 'createdAt DESC',
                where: {UserEmisor: pet.params.id}
            })
            .then(function(results){
                if(!results){
                    return resp.status(404).send("No hay mensajes enviados");
                }else{
                    resp.status(200).send(results);
                }
            });
        }
});

//Listar mensajes recibidos
app.get('/admin/users/:id/mensajesRecibidos',function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Mensaje.findAll({
                order: 'createdAt DESC',
                where: {UserReceptor: pet.params.id}
            })
            .then(function(results){
                if(!results){
                    return resp.status(404).send("No hay mensajes recibidos");
                }else{
                    resp.status(200).send(results);
                }
            });
        }
});


///////////////////////////////////////////////////////////CRUD LOGROS
app.post('/admin/logros',bodyParser,function(pet,resp){
    Logro.create({
        nombre:pet.body.nombre,
        descripcion:pet.body.descripcion
    }).then(function(){
        resp.status(200).send("Logro creado.")
    });
});

app.get('/admin/logros/:id',function(pet,resp){
     if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Logro.findOne({
                where: {id: pet.params.id}
            })
            .then(function(results){
                if(!results){
                    return resp.status(404).send("No hay logros con ese id");
                }else{
                    resp.status(200).send(results);
                }
            });
        }
});

//Logros en general
app.get('/admin/logros',function(pet,resp){
    Logro.findAll().then(function(results){
        if(!results){
            return resp.status(404).send("No se han encontrado logros.")
        }else{
            resp.status(200).send(results);
        }
    });
});

//Logros de un user
app.get('/admin/users/:id/logros',function(pet,resp){
    User_tiene_logro.findAll({order: 'id DESC',where:{UserId:pet.params.id}}).then(function(results){
        if(!results){
            return resp.status(404).send("No se han encontrado logros.")
        }else{
            resp.status(200).send(results);
        }
    });
});

//Logro de un user en concreto
app.get('/admin/users/:id/logros/:log',function(pet,resp){
    User_tiene_logro.findOne({where:{UserId:pet.params.id,LogroId:pet.params.log}}).then(function(results){
        if(!results){
            return resp.status(200).send(false)
        }else{
            resp.status(200).send(true);
        }
    });
});

//añadir logro a un user
app.post('/admin/users/:id/logros',bodyParser,function(pet,resp){
    User_tiene_logro.create({
            UserId: pet.params.id,
            LogroId: pet.body.LogroId
    }).then(function(){
        resp.status(200).send("Logro añadido a usuario");
    });
});

///////////////////////////////////////////////////////////CRUD RETOS
//Añadir retos
app.post('/admin/retos',bodyParser,function(pet,resp){
    Reto.create({
        nombre: pet.body.nombre,
        descripcion: pet.body.descripcion,
        duracion: pet.body.duracion,
        tipo: pet.body.tipo,
        descanso: pet.body.descanso,
        progresion: pet.body.progresion
    }).then(function(){
        resp.status(200).send("Reto añadido.");
    });
});

//Añadir ejercicios al reto
app.post('/admin/retos/:id/exercises',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Reto_tiene_ejer.create({
            RetoId: pet.params.id,
            ExerciseId: pet.body.ExerciseId
        }).then(function(){
            resp.status(200).send("Ejercicio añadido a reto.")
        });
    }
});

//Lista de retos
app.get('/admin/retos',function(pet,resp){
    Reto.findAll().then(function(results){
        if(!results){
            return resp.status(404).send("No se han encontrado retos.")
        }else{
            resp.status(200).send(results);
        }
    });
});

//Ejercicios de un reto
app.get('/admin/retos/:id/exercises',function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Reto_tiene_ejer.findAll({where:{RetoId:pet.params.id}})
            .then(function(results){
                if(!results){
                    return resp.status(404).send("No hay retos con ese id");
                }else{
                    resp.status(200).send(results);
                }
            });
        }
});



//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////AUTENTICACION USUARIO

app.post('/login',bodyParser,function(pet,resp){
    
        User.findOne({where: {nick: pet.body.nick}})
        .then(function(user){
            if(!user){
                    resp.status(400).send("Usuario erróneo")
            } else {
                console.log(pet.body.password);
                console.log(user.password);
                bcrypt.compare(pet.body.password, user.password, function(err, doesMatch){
                if (doesMatch){
                    resp.status(200).send({token: service.createToken(user)});
                }else{
                    resp.status(400).send("Contraseña errónea")
                }
            }
                )}});
});

app.post('/register',bodyParser,function(pet,resp){  
        bcrypt.hash(pet.body.password, 5, function( err,bcryptedPassword ) {
            User.create({
            nick: pet.body.nick,
            email: pet.body.email,
            password: bcryptedPassword,
            peso:pet.body.peso
        }).then(function(){
        resp.status(200).send("User añadido.")
    });
})});






//***********************************************APLICACION*******************************************************





//////////////////////////////////////////////////////CRUD USUARIOS
//Listado users
app.get('/api/users',function(pet, resp){
    User.findAll({order: 'nick ASC',where:{id:{gt: 0} }}).then(function(results){
        resp.send(results);
    });
});
 
//Creacion de usuario
app.post('/api/users',bodyParser,function(pet, resp){
    User.create({
        nick: pet.body.nick,
        email: pet.body.email,
        password: pet.body.password
    }).then(function(){
        resp.status(200).send("User añadido.")
    });
});

//Detalle de usuario
app.get('/api/users/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User.findById(pet.params.id).then(function(user){
            if(!user){
                resp.status(404).send("No se encuentra el usuario")
            }
            else
            {       
                resp.status(200).send(user) 
            }
         
        })
    }
});

app.get('/avatar/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User.findById(pet.params.id).then(function(user){
            if(!user){
                resp.status(404).send("No se encuentra el usuario")
            }
            else
            {       
                resp.status(200).sendFile('/home/hector/Desarrollo/tfg/api-tfg/img/avatars/'+pet.params.id+'.jpg')
            }
         
        })
    }
});

//Detalle de usuario (pornombre)
app.post('/api/users/find',bodyParser,function(pet,resp){
    
        User.findOne({where: {nick: pet.body.nick}}).then(function(user){
            if(!user){
                resp.status(404).send("No se encuentra el usuario")
            }
            else
            {       
                resp.status(200).send(user) 
            }
         
        })
    });

//Actualizar usuario
app.put('/api/users/:id',bodyParser,function(pet,resp){
	if(isNaN(pet.params.id))
	{
		resp.status(400).send("Parametro erroneo.")
	}
	else
	{
	User.findById(pet.params.id).then(function(user){
		if(!user){
			resp.status(404).send("No se encuentra el profesor a borrar")
		}
		else
		{
                user.nick= pet.body.nick,
                user.email= pet.body.email,
                user.password= pet.body.password,
                user.peso = pet.body.peso,
				user.save().then(function(){
					resp.status(200).send("Usuario actualizado.")
				})
		}
		})
	}
});

//Borrar un usuario
app.delete('/api/users/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User.findById(pet.params.id).then(function(user){
            if(!user){
                resp.status(404).send("No se encuentra el user a borrar")
            }
            else
            {
                user.destroy().then(function(){
                    resp.status(200).send("User eliminado.")
                });
            }
         
        })
    }
});

//Añadir USUARIO a AMIGOS
app.post('/api/users/:id/amigos',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Amigo.create({
            UserEmisor: pet.params.id,
            UserAgregado: pet.body.UserAgregado
        }).then(function(){
             resp.status(200).send("User añadido.")
        });
    }
});

//Comprobar amistad
app.get('/api/users/:id/amigos/:amigo',function(pet,resp){
    if(isNaN(pet.params.id)||isNaN(pet.params.amigo))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Amigo.findOne({where:{UserEmisor: pet.params.id,
                              UserAgregado: pet.params.amigo}})
        .then(function(result){
            if(!result)
            {
                return resp.status(200).send(false);
            }
             resp.status(200).send(true);
        });
    }
});

///////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////CRUD ENTRENAMIENTOS
app.post('/api/trainings', bodyParser,function(pet, resp){
    Training.create({
        nombre: pet.body.nombre,
        descripcion: pet.body.descripcion,
        publico: pet.body.publico,
        dificultad: pet.body.dificultad,
        UserId: pet.body.UserId
    }).then(function(){
        resp.status(200).send("Entrenamiento añadido.")
    });
});

//Lista de ENTRENAMIENTOS
app.get('/api/trainings',function(pet, resp){
    Training.findAll().then(function(results){
        resp.send(results);
    })});

//Lista de ENTRENAMIENTOS PREDEFINIDOS
app.get('/api/trainingsPred',function(pet, resp){
    Training.findAll({where:{publico:"pr"}}).then(function(results){
        resp.send(results);
    })});

//TOP 10 ENTRENAMIENTOS PUBLICOS
app.get('/api/trainingsPublicos',function(pet, resp){
    Training.findAll({limit:10,order: 'votos DESC',where:{publico:"si"}}).then(function(results){
        resp.send(results);
    })});

//Detalle de un ENTRENAMIENTO
app.get('/api/trainings/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Training.findById(pet.params.id).then(function(training){
            if(!training){
                resp.status(404).send("No se encuentra el entrenamiento")
            }
            else
            {       
                resp.status(200).send(training) 
            }
         
        })
    }
});

//Actualizacion de ENTRENAMIENTOS
app.put('/api/trainings/:id', bodyParser,function(pet,resp){
	if(isNaN(pet.params.id))
	{
		resp.status(400).send("Parametro erroneo.")
	}
	else
	{
	Training.findById(pet.params.id).then(function(training){
		if(!training){
			resp.status(404).send("No se encuentra el entrenamiento a modificar")
		}
		else
		{
                training.nombre = pet.body.nombre,
                training.descripcion = pet.body.descripcion,
                training.publico = pet.body.publico,
                training.dificultad = pet.body.dificultad,
                training.votos = pet.body.votos,
				training.save().then(function(){
					resp.status(200).send("Entrenamiento actualizado.")
				})
		}
		})
	}
});

//Borrado de ENTRENAMIENTOS
app.delete('/api/trainings/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Training.findById(pet.params.id).then(function(training){
            if(!training){
                resp.status(404).send("No se encuentra el entrenamiento a borrar")
            }
            else
            {
                //Borrado en cascada
                Ejer_contenidos_entren.destroy({where:{TrainingId: pet.params.id}}).then(()=>{
                    Training.findById(pet.params.id).then(function(training){
                        training.destroy().then(function(){
                                            resp.status(200).send("Training eliminado.")
                                        });
                    });
            });
        }
    });
}});


//Detalle de un ENTRENAMIENTO
app.get('/api/trainings/:id/exercises',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Ejer_contenidos_entren.findAll({where: {TrainingId: pet.params.id}}).then(function(result){
            if(!result){
                resp.status(404).send("Este entrenamiento no tiene ejercicios")
            }
            else
            {       
                resp.status(200).send(result) 
            }
         
        })
    }
});


//Lista de ENTRENAMIENTOS PERSONALIZADOS
app.post('/api/trainingsPers', bodyParser,function(pet, resp){ 
        Training.findAll({where: {UserId: pet.body.UserId}}).then(function(result){
            if(!result){
                resp.status(404).send("No se encontraron entrenamientos")
            }
            else
            {       
                resp.status(200).send(result) 
            }       
    });
});

//Crear un ENTRENAMIENTO PERSONALIZADO
app.post('/api/trainingsPers/:id', bodyParser,function(pet, resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Training.create({
            nombre: pet.body.nombre,
            descripcion: pet.body.descripcion,
            publico: pet.body.publico,
            dificultad: pet.body.dificultad,
            UserId: pet.params.id
        }).then(function(){
            resp.status(200).send("Ejercicio añadido.")
        });
    }
});

//Comprobar voto ENTRENAMIENTO
app.get('/api/trainings/:id/voto/:usu',function(pet,resp){
 
    if(isNaN(pet.params.id)||isNaN(pet.params.usu))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_vota_entren.findOne({where:{UserId: pet.params.usu,TrainingId: pet.params.id}}).then((result)=>{
            if(!result){
                resp.status(200).send(false);
            }else{
                resp.status(200).send(true);
            }                            })
    }
});

//Votar ENTRENAMIENTO
app.post('/api/trainings/:id/voto/:usu',function(pet,resp){
    if(isNaN(pet.params.id)||isNaN(pet.params.usu))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_vota_entren.create({
            UserId: pet.params.usu,
            TrainingId: pet.params.id
        }).then(function(){
            resp.status(200).send("Training votado.")
        });
    }
})

//////////////////////////////////////////////////////CRUD EJERCICIOS
//Creacion de EJERCICIOS
app.post('/api/exercises', bodyParser,function(pet, resp){
    Exercise.create({
        nombre: pet.body.nombre,
        descripcion: pet.body.descripcion,
        tipo: pet.body.tipo,
        UserId: pet.body.UserId
    }).then(function(){
        resp.status(200).send("Ejercicio añadido.")
    });
});



//Lista de EJERCICIOS predefinidos
app.get('/api/exercises',function(pet, resp){
    Exercise.findAll({where:{UserId: 0}}).then(function(results){
        resp.send(results);
    })});

//Detalle de un EJERCICIO
app.get('/api/exercises/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Exercise.findById(pet.params.id).then(function(exercise){
            if(!exercise){
                resp.status(404).send("No se encuentra el ejercicio")
            }
            else
            {       
                resp.status(200).send(exercise) 
            }
         
        })
    }
});

//Actualizacion de EJERCICIOS
app.put('/api/exercises/:id', bodyParser,function(pet,resp){
	if(isNaN(pet.params.id))
	{
		resp.status(400).send("Parametro erroneo.")
	}
	else
	{
	Exercise.findById(pet.params.id).then(function(exercise){
		if(!exercise){
			resp.status(404).send("No se encuentra el ejercicio a modificar")
		}
		else
		{
                exercise.nombre = pet.body.nombre,
                exercise.descripcion = pet.body.descripcion,
                exercise.tipo = pet.body.tipo,
                exercise.img = pet.body.img,
				exercise.save().then(function(){
					resp.status(200).send("Ejercicio actualizado.")
				})
		}
		})
	}
});

//Borrado de EJERCICIOS
app.delete('/api/exercises/:id',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Exercise.findById(pet.params.id).then(function(exercise){
            if(!exercise){
                resp.status(404).send("No se encuentra el ejercicio a borrar")
            }
            else
            {   //Borrado en cascada
                Ejer_contenidos_entren.destroy({where:{ExerciseId: pet.params.id}}).then(()=>{
                    Exercise.findById(pet.params.id).then(function(exercise){
                        exercise.destroy().then(function(){
                                            resp.status(200).send("Ejercicio eliminado.")
                                        });
                    });
            });
        }
    });
}});

//Imagenes de EJERCICIO
app.get('/exercises/:id/images/:num',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Exercise.findById(pet.params.id).then(function(exercise){
            if(!exercise){
                resp.status(404).send("No se encuentra el ejercicio")
            }
            else
            {       
                resp.status(200).sendFile('/home/hector/Desarrollo/tfg/api-tfg/img/exercises/'+pet.params.id+'-'+pet.params.num+'.png')
            }
         
        })
    }
});

//Upload imagen de EJERCICIO
app.post('/exercises/:id/images/:num',function(pet,resp){
    if (!pet.files){
        resp.status(400).send('No hay archivos que subir.');
    }
    else{
        let ilustracion = pet.files.file;
        console.log(pet.files.file);
        ilustracion.mv('/home/hector/Desarrollo/tfg/api-tfg/img/exercises/'+pet.params.id+'-'+pet.params.num+'.png', function(err) {
        if (err){
            resp.status(500).send(err);
        }else{
            resp.send('File uploaded!');
            }
        });
    }
});

app.delete('/api/exercises/:id/images',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Exercise.findById(pet.params.id).then(function(exercise){
            if(!exercise){
                resp.status(404).send("No se encuentra el ejercicio")
            }
            else
            {     
                for(var i=1;i<=exercise.img;i++){
                    fs.unlink("/home/hector/Desarrollo/tfg/api-tfg/img/exercises/"+exercise.id+"-"+exercise.i,(err) => {
                        if (err) {
                            console.log("Error al borrar una imagen:"+err);
                            return resp.status(400).send("Error al borrar el archivo.")
                        } 
                });
                }
                console.log('Imagenes borradas con éxito.');  
                exercise.img = 0,
				exercise.save().then(function(){
					resp.status(200).send("Imagenes borradas y ejercicio actualizado.")
				});                            
            }
         
        })
    }
});

//Lista EJERCICIOS de un usuario
app.post('/api/exercises/findByUser',bodyParser,function(pet,resp){
    Exercise.findAll({where:{UserId: pet.body.UserId}}).then(function(exercises){
        if(!exercises){
                resp.status(404).send("No se encuentra ejercicios")
            }
            else
            {       
                resp.status(200).send(exercises)
            }
    });
});

//////////////////////////////////////////////////////RELACION EJERCICIOS-ENTRENAMIENTOS
//Asignar EJERCICIO a ENTRENAMIENTO
app.post('/api/trainings/:id', bodyParser,function(pet, resp){
        Ejer_contenidos_entren.create({
            repeticiones: pet.body.repeticiones,
            series: pet.body.series,
            peso: pet.body.peso,
            tiempo: pet.body.tiempo,
            TrainingId: pet.params.id,
            ExerciseId: pet.body.ExerciseId,
        }).then(function(){
        resp.status(200).send("Ejercicio asignado a entrenamiento.")
    });
});

//Detalle de un EJERCICIO en un ENTRENAMIENTO
app.get('/api/trainings/:id/exercises/:idEx',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        Ejer_contenidos_entren.findOne({where: {TrainingId: pet.params.id,
                                                ExerciseId: pet.params.idEx}})
        .then(function(exercise){
            if(!exercise){
                resp.status(404).send("No se encuentra el ejercicio")
            }
            else
            {       
                resp.status(200).send(exercise)
            }
         
        })
    }
});

//Lista de suscripciones
app.get('/api/users/:id/suscripciones',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_suscrito_entren.findAll({where: {UserId:pet.params.id}})
        .then(function(results){
            if(!results){
                resp.status(404).send("No se encuentran suscripciones")
            }
            else
            {       
                resp.status(200).send(results)
            }
         
        })
    }
});

//Suscripcion USUARIO a ENTRENAMIENTO PUBLICO
app.post('/api/users/:id/suscripciones',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
       User_suscrito_entren.create({
           UserId: pet.params.id,
           TrainingId: pet.body.TrainingId
       }).then(function(){
           resp.status(200).send("Suscripcion creada con exito.");
       });
    }
});

//Eliminar suscripcion
app.delete('/api/users/:id/suscripciones/:entren',function(pet,resp){
    if(isNaN(pet.params.id)||isNaN(pet.params.entren))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_suscrito_entren.delete({where:{UserId: pet.params.id,
                                            TrainingId:pet.params.entren}})
        .then(function(){
            resp.status(200).send("Suscripcion borrada.");
        });
    }
});

//ComprobarSuscripcion
app.get('/api/users/:id/suscripciones/:entren',function(pet,resp){
    if(isNaN(pet.params.id)||isNaN(pet.params.entren))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {
        User_suscrito_entren.findOne({where:{UserId: pet.params.id,
                                            TrainingId:pet.params.entren}})
        .then(function(results){
            if(!results){
                return resp.status(200).send(false);
            }
            else{
                resp.status(200).send(true);
            }
        });
    }
});

//////////////////////////////////////////////////////CRUD METS
app.post('/api/mets/', bodyParser,function(pet, resp){ 
        Met.findOne({where: {categoria: pet.body.categoria}})
        .then(function(met){
            if(!met){
                resp.status(404).send("No se encuentra la categoria")
            }
            else
            {       
                resp.status(200).send(met)
            }
         
        })
});

app.get('/api/mets',function(pet,resp){
    Met.findAll().then(function(results){
        if(!results){
                resp.status(404).send("No se encontraron mets.")
        }else{
            resp.status(200).send(results);
        }
    })
})


//////////////////////////////////////////////////////CRUD COMENTARIOS
//Listado de comentarios de un training
app.get('/api/trainings/:id/comments', function(pet, resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
    Comentario.findAll({where: { TrainingId:pet.params.id }}).then(function(comments){
        if(!comments){
                resp.status(404).send("No se encontraron comentarios en este entrenamiento.")
        }else{
            resp.status(200).send(comments);
        }
        });
    }
});



//Creacion de un comentario
app.post('/api/trainings/:id/comments', bodyParser, function(pet, resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
        Comentario.create({
            bodycomment: pet.body.bodycomment,
            nick: pet.body.nick,
            UserId: pet.body.UserId,
            TrainingId: pet.params.id
        }).then(function(){
            resp.status(200).send("Comentario añadido.")
        });
    }
});

//////////////////////////////////////////////////////CRUD ESTADISTICAS
app.get('/api/users/:id/stats',function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
    Estadistica.findAll({order: 'createdAt ASC',where: { UserId:pet.params.id }}).then(function(stats){
        if(!stats){
                resp.status(404).send("No se han encontrado estadisticas.")
        }else{
            resp.status(200).send(stats);
        }
        });
    }
});


//Crear ESTADISTICA
app.post('/api/users/:id/stats', bodyParser, function(pet, resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
        Estadistica.create({
            kcal: pet.body.kcal,
            tiempo: pet.body.tiempo,
            UserId: pet.params.id,
            TrainingId: pet.body.TrainingId
        }).then(function(){
            resp.status(200).send("Estadistica añadida.")
        });
    }
});

//Crear registro de peso
app.post('/api/users/:id/pesos',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
    {
        resp.status(400).send("Parametro erroneo.")
    }
    else
    {   
        Peso.create({
            peso: pet.body.peso,
            UserId: pet.params.id
        }).then(function(){
            resp.status(200).send("Estadistica de peso añadida.")
        });
    }
});

//Lista de pesos de un usuario
app.get('/api/users/:id/pesos',function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Peso.findAll({order: 'createdAt ASC',where: {UserId:pet.params.id}})
            .then(function(results){
                resp.status(200).send(results);
            });
        }
});

///////////////////////////////////////////////CRUD MENSAJESPRIVADOS
//Crear mensaje privado
app.post('/api/users/:id/mensajes',bodyParser,function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Mensaje.create({
                bodymessage: pet.body.bodymessage,
                nickEmisor: pet.body.nick,
                nickReceptor: pet.body.nickReceptor,
                UserEmisor: pet.body.UserEmisor,
                UserReceptor: pet.params.id
            })
            .then(function(){
                resp.status(200).send("Mensaje enviado.");
            });
        }
});

//Listar mensajes enviados
app.get('/api/users/:id/mensajesEnviados',function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Mensaje.findAll({
                order: 'createdAt DESC',
                where: {UserEmisor: pet.params.id}
            })
            .then(function(results){
                if(!results){
                    return resp.status(404).send("No hay mensajes enviados");
                }else{
                    resp.status(200).send(results);
                }
            });
        }
});

//Listar mensajes recibidos
app.get('/api/users/:id/mensajesRecibidos',function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Mensaje.findAll({
                order: 'createdAt DESC',
                where: {UserReceptor: pet.params.id}
            })
            .then(function(results){
                if(!results){
                    return resp.status(404).send("No hay mensajes recibidos");
                }else{
                    resp.status(200).send(results);
                }
            });
        }
});


///////////////////////////////////////////////////////////CRUD LOGROS
app.get('/api/logros/:id',function(pet,resp){
     if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Logro.findOne({
                where: {id: pet.params.id}
            })
            .then(function(results){
                if(!results){
                    return resp.status(404).send("No hay logros con ese id");
                }else{
                    resp.status(200).send(results);
                }
            });
        }
});

//Logros en general
app.get('/api/logros',function(pet,resp){
    Logro.findAll().then(function(results){
        if(!results){
            return resp.status(404).send("No se han encontrado logros.")
        }else{
            resp.status(200).send(results);
        }
    });
});

//Logros de un user
app.get('/api/users/:id/logros',function(pet,resp){
    User_tiene_logro.findAll({order: 'id DESC',where:{UserId:pet.params.id}}).then(function(results){
        if(!results){
            return resp.status(404).send("No se han encontrado logros.")
        }else{
            resp.status(200).send(results);
        }
    });
});

//Logro de un user en concreto
app.get('/api/users/:id/logros/:log',function(pet,resp){
    User_tiene_logro.findOne({where:{UserId:pet.params.id,LogroId:pet.params.log}}).then(function(results){
        if(!results){
            return resp.status(200).send(false)
        }else{
            resp.status(200).send(true);
        }
    });
});

//añadir logro a un user
app.post('/api/users/:id/logros',bodyParser,function(pet,resp){
    User_tiene_logro.create({
            UserId: pet.params.id,
            LogroId: pet.body.LogroId
    }).then(function(){
        resp.status(200).send("Logro añadido a usuario");
    });
});

///////////////////////////////////////////////////////////CRUD RETOS
//Lista de retos
app.get('/api/retos',function(pet,resp){
    Reto.findAll().then(function(results){
        if(!results){
            return resp.status(404).send("No se han encontrado retos.")
        }else{
            resp.status(200).send(results);
        }
    });
});

//Ejercicios de un reto
app.get('/api/retos/:id/exercises',function(pet,resp){
    if(isNaN(pet.params.id))
        {
            resp.status(400).send("Parametro erroneo.")
        }
        else
        {   
            Reto_tiene_ejer.findAll({where:{RetoId:pet.params.id}})
            .then(function(results){
                if(!results){
                    return resp.status(404).send("No hay retos con ese id");
                }else{
                    resp.status(200).send(results);
                }
            });
        }
});









module.exports = app;

 
//Puesta en escucha de express
sequelize.sync({force:false})
.then(function(){
    app.listen(3000,"0.0.0.0", function(){
        console.log('Express en el puerto 3000');
    })
});