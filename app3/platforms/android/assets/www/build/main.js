webpackJsonp([24],{

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RetosService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the RetosProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var RetosService = (function () {
    function RetosService(http, storage) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        this.pathRetos = 'http://192.168.1.5:3000/api/retos';
        this.pathExercises = 'http://192.168.1.5:3000/api/exercises';
        this.user = null;
        this.storage.get('token').then(function (value) {
            _this.token = value;
        });
    }
    RetosService.prototype.getRetos = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathRetos, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    RetosService.prototype.getExercisesReto = function (reto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathRetos + '/' + reto.id + '/exercises', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return RetosService;
}());
RetosService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], RetosService);

//# sourceMappingURL=retos.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, userService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userService = userService;
        this.newUser = {
            nick: '',
            email: '',
            password: '',
            peso: 0.0
        };
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.registerUser = function () {
        this.userService.postUser(this.newUser);
    };
    RegisterPage.prototype.mostrarUser = function () {
        console.log(this.newUser);
    };
    return RegisterPage;
}());
RegisterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-register',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/register/register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Registro</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <form (ngSubmit)="registerUser()">\n    <ion-list>\n      <ion-item>\n        <ion-label floating>Nick</ion-label>\n        <ion-input type="text" name="nick" [(ngModel)]="newUser.nick"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Email</ion-label>\n        <ion-input type="email" name="email" [(ngModel)]="newUser.email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Password</ion-label>\n        <ion-input type="password" name="password" [(ngModel)]="newUser.password"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Peso</ion-label>\n        <ion-input type="text" name="peso" [(ngModel)]="newUser.peso"></ion-input>\n      </ion-item>\n    </ion-list>\n    <div padding>\n      <button ion-button block type="submit">Registrarse</button>\n    </div>\n  </form>\n</ion-content>'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/register/register.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserService */]])
], RegisterPage);

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingFinishedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_menu__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_training_training__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the TrainingFinishedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TrainingFinishedPage = (function () {
    function TrainingFinishedPage(navCtrl, navParams, alertCtrl, storage, trainingService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.trainingService = trainingService;
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        });
        this.totalKcal = this.navParams.get('totalKcal');
        this.totalTime = this.navParams.get('totalTime');
        this.training = this.navParams.get('training');
        this.formatTime();
        this.totalKcal = Math.round(this.totalKcal * 100) / 100;
    }
    TrainingFinishedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrainingFinishedPage');
    };
    TrainingFinishedPage.prototype.formatTime = function () {
        if (Math.floor((this.totalTime / 60) / 60) < 10) {
            this.horas = "0" + (Math.floor((this.totalTime / 60) / 60)).toString();
        }
        else {
            this.horas = (Math.floor((this.totalTime / 60) / 60)).toString();
        }
        if (Math.floor((this.totalTime / 60) % 60) < 10) {
            this.minutos = "0" + Math.floor((this.totalTime / 60) % 60).toString();
        }
        else {
            this.minutos = Math.floor((this.totalTime / 60) % 60).toString();
        }
        if (this.totalTime % 60 < 10) {
            this.segundos = "0" + this.totalTime % 60;
        }
        else {
            this.segundos = this.totalTime % 60;
        }
    };
    TrainingFinishedPage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: '¿Quieres guardar tu entrenamiento?',
            message: '¡Enhorabuena por finalizar el entrenamiento! ¿Desea guardar el entrenamiento en sus estadísticas?',
            buttons: [
                {
                    text: 'SI',
                    handler: function () {
                        console.log('SI clicked');
                        _this.guardarEstadistica();
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        console.log('NO clicked');
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__menu_menu__["a" /* MenuPage */], {
                            user: _this.user
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    TrainingFinishedPage.prototype.guardarEstadistica = function () {
        var _this = this;
        this.trainingService.crearEstadistica(this.training, this.totalTime, this.totalKcal, this.user)
            .then(function (data) {
            //mandar toast
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__menu_menu__["a" /* MenuPage */], {
                user: _this.user
            });
        });
    };
    return TrainingFinishedPage;
}());
TrainingFinishedPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-training-finished',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/training-finished/training-finished.html"*/'<!--\n  Generated template for the TrainingFinishedPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Resumen del entrenamiento</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>{{training.nombre}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-12 style="text-align: center">\n          <!--<img src="">-->\n          <p>Tiempo total: {{horas}}:{{minutos}}:{{segundos}}</p>\n          <p>Total Kcal quemadas: {{totalKcal}} Kcal</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-12 style="text-align: center">\n          <button ion-button (click)="showConfirm()">Salir</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/training-finished/training-finished.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__providers_training_training__["a" /* TrainingService */]])
], TrainingFinishedPage);

//# sourceMappingURL=training-finished.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detail_training_detail_training__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the TrainingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TrainingsPage = (function () {
    function TrainingsPage(navCtrl, navParams, trainingService, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.storage = storage;
        this.storage.get('user').then(function (data) {
            _this.user = data;
        });
        this.getTrainings();
    }
    TrainingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrainingsPage');
    };
    TrainingsPage.prototype.getTrainings = function () {
        var _this = this;
        this.trainingService.getAllTrainings().then(function (data) {
            if (data) {
                _this.trainings = data;
                console.log('Trainings descargados de la api.');
            }
            else {
                console.log('No hay o no se ha conseguido recoger trainings.');
            }
        });
    };
    TrainingsPage.prototype.goDetail = function (training) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detail_training_detail_training__["a" /* DetailTrainingPage */], {
            training: training,
            pers: false,
            user: this.user
        });
    };
    return TrainingsPage;
}());
TrainingsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-trainings',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/trainings/trainings.html"*/'<!--\n  Generated template for the TrainingsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n      <ion-title>Entrenamientos predefinidos</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n      <ion-list>\n        <ion-item *ngFor="let training of trainings" (click)="goDetail(training)">\n          <ion-avatar item-start>\n            <img src="assets/img/pesa.png">\n          </ion-avatar>\n          <h2>{{training.nombre}}</h2>\n          <p>Dificultad: {{training.dificultad}}</p>\n        </ion-item>\n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/trainings/trainings.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
], TrainingsPage);

//# sourceMappingURL=trainings.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayTrainingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__training_finished_training_finished__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the PlayTrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PlayTrainingPage = PlayTrainingPage_1 = (function () {
    function PlayTrainingPage(navCtrl, navParams, trainingService, storage, file, transfer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.storage = storage;
        this.file = file;
        this.transfer = transfer;
        this.detailedExercises = [];
        this.time = 0;
        this.started = false;
        this.kcal = 0.0;
        this.lastEx = false;
        this.images = [];
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        });
        this.training = this.navParams.get('training');
        this.exercises = this.navParams.get('exercises');
        this.detailedExercises = this.navParams.get('detailedExercises');
        this.currentEx = this.navParams.get('currentEx');
        this.totalTime = this.navParams.get('totalTime');
        this.totalKcal = this.navParams.get('totalKcal');
        if (this.currentEx == this.detailedExercises.length - 1) {
            this.lastEx = true;
        }
        this.trainingService.getMets(this.detailedExercises[this.currentEx].tipo).then(function (data) {
            if (data) {
                _this.MET = data;
                console.log('METS descargados.');
            }
            else {
                console.log('No hay o no se ha conseguido recoger METS.');
            }
        });
        var fileTransfer = this.transfer.create();
        if (this.detailedExercises[this.currentEx].img > 0) {
            for (var i = 1; i <= this.detailedExercises[this.currentEx].img; i++) {
                fileTransfer.download('http://192.168.1.5:3000/exercises/' + this.detailedExercises[this.currentEx].id + '/images/' + i, this.file.dataDirectory + this.detailedExercises[this.currentEx].id + '-' + i + '.png').then(function (entry) {
                    _this.images.push(entry.toURL());
                }, function (error) {
                    console.log(error);
                });
            }
        }
    }
    PlayTrainingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PlayTrainingPage');
    };
    PlayTrainingPage.prototype.startTimer = function () {
        var _this = this;
        this.started = true;
        this.subscription = __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].interval(10).subscribe(function (x) {
            // the number 1000 is on miliseconds so every second is going to have an iteration of what is inside this code.
            console.log(_this.time);
            if (Math.floor((_this.time / 60) / 60) < 10) {
                _this.horas = "0" + (Math.floor((_this.time / 60) / 60)).toString();
            }
            else {
                _this.horas = (Math.floor((_this.time / 60) / 60)).toString();
            }
            if (Math.floor((_this.time / 60) % 60) < 10) {
                _this.minutos = "0" + Math.floor((_this.time / 60) % 60).toString();
            }
            else {
                _this.minutos = Math.floor((_this.time / 60) % 60).toString();
            }
            if (_this.time % 60 < 10) {
                _this.segundos = "0" + _this.time % 60;
            }
            else {
                _this.segundos = _this.time % 60;
            }
            _this.time++;
        });
    };
    PlayTrainingPage.prototype.stopTimer = function () {
        this.started = false;
        this.subscription.unsubscribe();
        console.log('El tiempo ha sido: ' + this.time);
    };
    PlayTrainingPage.prototype.calculateKcal = function () {
        var total = (this.time / 60) * (this.MET.mets * 0.0175 * this.user.peso);
        console.log('Quemadas ' + total + ' KCAL.');
        return total;
    };
    PlayTrainingPage.prototype.goNext = function () {
        this.kcal = this.calculateKcal();
        this.navCtrl.push(PlayTrainingPage_1, {
            training: this.training,
            exercises: this.exercises,
            detailedExercises: this.detailedExercises,
            currentEx: this.currentEx + 1,
            totalTime: this.totalTime + this.time,
            totalKcal: this.totalKcal + this.kcal
        });
    };
    PlayTrainingPage.prototype.goFinish = function () {
        this.calculateKcal();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__training_finished_training_finished__["a" /* TrainingFinishedPage */], {
            training: this.training,
            totalTime: this.totalTime + this.time,
            totalKcal: this.totalKcal + this.kcal
        });
    };
    PlayTrainingPage.prototype.goToSlide = function () {
        this.slides.slideTo(2, 500);
    };
    PlayTrainingPage.prototype.slideChanged = function () {
        var currentIndex = this.slides.getActiveIndex();
        console.log('Current index is', currentIndex);
    };
    return PlayTrainingPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */])
], PlayTrainingPage.prototype, "slides", void 0);
PlayTrainingPage = PlayTrainingPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-play-training',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/play-training/play-training.html"*/'<!--\n  Generated template for the PlayTrainingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Entrenamiento en curso...</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n        <ion-col col-12>\n            <h5 style="text-align: center">{{detailedExercises[currentEx].nombre}}</h5>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-12>\n        <ion-slides (ionSlideDidChange)="slideChanged()">\n          <ion-slide *ngFor="let image of images">\n            <img [src]="image">\n          </ion-slide>\n        </ion-slides>\n        <p *ngIf="images.length<1">Sin imagenes...</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4></ion-col>\n        <ion-col col-4>\n            <p>Series: {{exercises.series}}</p>\n            <p>Repeticiones: {{exercises.repeticiones}}</p>\n            <p>Peso: {{exercises.peso}}</p>\n            <h5>Tiempo: \n              <span *ngIf="time>0">{{horas}}:{{minutos}}:{{segundos}}</span>\n              <span *ngIf="!time>0">00:00:00</span>\n            </h5>\n        </ion-col>\n        <ion-col col-4></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n          <button ion-button (click)="startTimer()" *ngIf="!started" style="text-align: center !important">Empezar</button>\n          <button ion-button (click)="stopTimer()" *ngIf="started" style="text-align: center !important">Terminar</button>     \n      </ion-col>\n      <ion-col col-6>\n          <button ion-button *ngIf="!lastEx" (click)="goNext()" style="text-align: center !important">Siguiente</button>\n          <button ion-button *ngIf="lastEx" (click)="goFinish()" style="text-align: center !important">Finalizar</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/play-training/play-training.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["a" /* Transfer */]])
], PlayTrainingPage);

var PlayTrainingPage_1;
//# sourceMappingURL=play-training.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExercisesPersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__create_exercise_create_exercise__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_ex_tr_add_ex_tr__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__edit_exercise_edit_exercise__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__detail_exercise_detail_exercise__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the ExercisesPersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ExercisesPersPage = (function () {
    function ExercisesPersPage(navCtrl, navParams, trainingService, storage, viewController, alertCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.storage = storage;
        this.viewController = viewController;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.onlyDetail = navParams.get('onlyDetail');
        this.training = navParams.get('training');
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        }).then(function () {
            _this.getExercises();
        });
    }
    ExercisesPersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ExercisesPersPage');
    };
    ExercisesPersPage.prototype.getExercises = function () {
        var _this = this;
        this.trainingService.getExercisesByUser(this.user.id).then(function (data) {
            if (data) {
                _this.exercises = data;
                console.log('Exercises personalizados descargados de la api.');
            }
            else {
                console.log('No hay o no se ha conseguido recoger exercises.');
            }
        });
    };
    ExercisesPersPage.prototype.goCreate = function () {
        var _this = this;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__create_exercise_create_exercise__["a" /* CreateExercisePage */]);
        this.events.subscribe('reloadExercisesPersPage', function () {
            _this.getExercises();
        });
    };
    ExercisesPersPage.prototype.goDetailExercise = function (detailedExercise) {
        var exercise = null;
        this.trainingService.getExerciseInTraining(this.training.id, detailedExercise.id)
            .then(function (data) {
            exercise = data;
        });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__detail_exercise_detail_exercise__["a" /* DetailExercisePage */], {
            exercise: detailedExercise,
            exerciseCustom: {
                series: "",
                repeticiones: "",
                peso: "",
                tiempo: ""
            },
            enEntrenamiento: false,
            pers: true
        });
    };
    ExercisesPersPage.prototype.selectExercise = function (exercise) {
        var _this = this;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__add_ex_tr_add_ex_tr__["a" /* AddExTrPage */], {
            exercise: exercise,
            training: this.training,
            user: this.user
        });
        this.events.subscribe('itemSelected', function () {
            _this.events.publish('reloadDetailTrainingPage');
            _this.viewController.dismiss();
        });
    };
    ExercisesPersPage.prototype.goEdit = function (exercise) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__edit_exercise_edit_exercise__["a" /* EditExercisePage */], {
            exercise: exercise,
            user: this.user
        });
    };
    ExercisesPersPage.prototype.deleteExercise = function (id) {
        var _this = this;
        console.log('La id es: ' + id);
        this.trainingService.deleteExercise(id).then(function (data) {
            console.log(data);
            _this.getExercises();
        });
    };
    ExercisesPersPage.prototype.showDelete = function (id) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'DELETE',
            message: '¿Seguro que quieres borrar el ejercicio?',
            buttons: [
                {
                    text: 'SI',
                    handler: function () {
                        console.log('SI clicked');
                        _this.deleteExercise(id);
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        console.log('NO clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    return ExercisesPersPage;
}());
ExercisesPersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-exercises-pers',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/exercises-pers/exercises-pers.html"*/'<!--\n  Generated template for the ExercisesPersPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Ejercicios personalizados</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n            <ion-item-sliding *ngFor="let exercise of exercises">\n               <ion-item (click)="selectExercise(exercise)">\n                <ion-avatar item-start>\n                <img src="assets/img/pesa.png">\n              </ion-avatar>\n              <h2>{{exercise.nombre}}</h2>\n              <p>Tipo: {{exercise.tipo}}</p>\n            </ion-item>\n            <ion-item-options side="right">\n              <button ion-button color="secondary" (click)="goDetailExercise(exercise)"> \n                <ion-icon name="eye"></ion-icon>\n                View\n              </button>\n              <button ion-button color="primary" (click)="goEdit(exercise)">\n                <ion-icon name="create"></ion-icon>\n                Edit\n              </button>\n              <button ion-button color="danger" (click)="showDelete(exercise.id)"> \n                <ion-icon name="trash"></ion-icon>\n                Delete\n              </button>\n            </ion-item-options>\n        </ion-item-sliding>\n      </ion-list>\n  <button ion-fab color="primary" (click)="goCreate()"><ion-icon name="add"></ion-icon></button>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/exercises-pers/exercises-pers.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], ExercisesPersPage);

//# sourceMappingURL=exercises-pers.js.map

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateExercisePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CreateExercisePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CreateExercisePage = (function () {
    function CreateExercisePage(navCtrl, navParams, trainingService, storage, toastCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.newExercise = {
            nombre: '',
            descripcion: '',
            tipo: '',
            UserId: ''
        };
        //this.training = this.navParams.get('training');
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        }).then(function () {
            _this.newExercise = {
                nombre: '',
                descripcion: '',
                tipo: '',
                UserId: _this.user.id
            };
            /*this.newExTr = {
                repeticiones: '',
                series:'',
                peso:'',
                tiempo:'',
                TrainingId:this.training.id,
                ExerciseId:''
            };*/
        }, function (error) {
            console.log(error);
        });
        this.getAllMets();
    }
    CreateExercisePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateExercisePage');
    };
    CreateExercisePage.prototype.getAllMets = function () {
        var _this = this;
        this.trainingService.getAllMets().then(function (results) {
            _this.mets = results;
        });
    };
    CreateExercisePage.prototype.createExercise = function () {
        var _this = this;
        this.trainingService.createExercise(this.newExercise).then(function () {
            _this.events.publish('reloadExercisesPersPage');
            _this.presentToast('Ejercicio creado con éxito');
            _this.navCtrl.pop();
        });
    };
    CreateExercisePage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 4000,
            position: 'top'
        });
        toast.present();
    };
    return CreateExercisePage;
}());
CreateExercisePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-create-exercise',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/create-exercise/create-exercise.html"*/'<!--\n  Generated template for the CreateExercisePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Creación de ejercicio</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <form (ngSubmit)="createExercise()">\n    <ion-list>\n      <ion-item>\n        <ion-label floating>Nombre</ion-label>\n        <ion-input type="text" name="nombre" [(ngModel)]="newExercise.nombre"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Descripcion</ion-label>\n        <ion-input type="text" name="descripcion" [(ngModel)]="newExercise.descripcion"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Tipo</ion-label>\n        <ion-select  name="tipo" [(ngModel)]="newExercise.tipo">\n              <ion-option value="{{met.categoria}}" *ngFor="let met of mets">{{met.categoria}}</ion-option>\n        </ion-select>      \n      </ion-item>\n    </ion-list>\n    <div padding>\n      <button ion-button block type="submit">Guardar</button>\n    </div>\n  </form>\n\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/create-exercise/create-exercise.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], CreateExercisePage);

//# sourceMappingURL=create-exercise.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddExTrPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddExTrPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AddExTrPage = (function () {
    function AddExTrPage(navCtrl, navParams, trainingService, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.events = events;
        this.user = navParams.get('user');
        this.training = navParams.get('training');
        this.exercise = navParams.get('exercise');
        this.newExTr = {
            repeticiones: '',
            series: '',
            peso: '',
            tiempo: '',
            TrainingId: this.training.id,
            ExerciseId: this.exercise.id
        };
    }
    AddExTrPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddExTrPage');
    };
    AddExTrPage.prototype.saveResult = function () {
        var _this = this;
        this.trainingService.addToTraining(this.training.id, this.newExTr).then(function () {
            _this.events.publish('itemSelected');
            _this.navCtrl.pop();
        });
    };
    return AddExTrPage;
}());
AddExTrPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-add-ex-tr',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/add-ex-tr/add-ex-tr.html"*/'<!--\n  Generated template for the AddExTrPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Añadir a entrenamiento</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<form (ngSubmit)="saveResult()">\n    <ion-list>\n     <ion-item>\n        <ion-label floating>Series</ion-label>\n        <ion-input type="text" name="series" [(ngModel)]="newExTr.series"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Repeticiones</ion-label>\n        <ion-input type="text" name="repeticiones" [(ngModel)]="newExTr.repeticiones"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Peso</ion-label>\n        <ion-input type="text" name="peso" [(ngModel)]="newExTr.peso"></ion-input>\n      </ion-item>\n    </ion-list>\n    <div padding>\n      <button ion-button block type="submit">Guardar</button>\n    </div>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/add-ex-tr/add-ex-tr.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], AddExTrPage);

//# sourceMappingURL=add-ex-tr.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditExercisePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EditExercisePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var EditExercisePage = (function () {
    function EditExercisePage(navCtrl, navParams, events, alertCtrl, trainingService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.trainingService = trainingService;
        this.user = navParams.get('user');
        this.exercise = navParams.get('exercise');
        this.getAllMets();
    }
    EditExercisePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditExercisePage');
    };
    EditExercisePage.prototype.getAllMets = function () {
        var _this = this;
        this.trainingService.getAllMets().then(function (results) {
            _this.mets = results;
        });
    };
    EditExercisePage.prototype.editExercise = function (exercise) {
        var _this = this;
        this.trainingService.editExercise(exercise).then(function (data) {
            console.log(data);
            _this.events.publish('refreshExercises');
            _this.navCtrl.pop();
        });
    };
    EditExercisePage.prototype.showConfirm = function (exercise) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Edit',
            message: '¿Seguro que quieres editar el ejercicio?',
            buttons: [
                {
                    text: 'SI',
                    handler: function () {
                        console.log('SI clicked');
                        _this.editExercise(exercise);
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        console.log('NO clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    return EditExercisePage;
}());
EditExercisePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-edit-exercise',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/edit-exercise/edit-exercise.html"*/'<!--\n  Generated template for the EditExercisePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Edición de ejercicio</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <form (ngSubmit)="showConfirm(exercise)">\n    <ion-list>\n      <ion-item>\n        <ion-label floating>Nombre</ion-label>\n        <ion-input type="text" name="nombre" [(ngModel)]="exercise.nombre"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Descripcion</ion-label>\n        <ion-input type="text" name="descripcion" [(ngModel)]="exercise.descripcion"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Tipo</ion-label>\n        <ion-select  name="tipo" [(ngModel)]="exercise.tipo">\n              <ion-option value="{{met.categoria}}" *ngFor="let met of mets">{{met.categoria}}</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-list> \n    <div padding>\n      <button ion-button block type="submit">Guardar</button>\n    </div>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/edit-exercise/edit-exercise.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */]])
], EditExercisePage);

//# sourceMappingURL=edit-exercise.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditTrainingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EditTrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var EditTrainingPage = (function () {
    function EditTrainingPage(navCtrl, navParams, trainingService, alertCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.user = navParams.get('user');
        this.training = navParams.get('training');
    }
    EditTrainingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditTrainingPage');
    };
    EditTrainingPage.prototype.showConfirm = function (training) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Edit',
            message: '¿Seguro que quieres editar el entrenamiento?',
            buttons: [
                {
                    text: 'SI',
                    handler: function () {
                        console.log('SI clicked');
                        _this.editTraining(training);
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        console.log('NO clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    EditTrainingPage.prototype.editTraining = function (training) {
        var _this = this;
        this.trainingService.editExercise(training).then(function (data) {
            console.log(data);
            _this.events.publish('refreshTraining');
            _this.navCtrl.pop();
        });
    };
    return EditTrainingPage;
}());
EditTrainingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-edit-training',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/edit-training/edit-training.html"*/'<!--\n  Generated template for the EditTrainingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Edición de entrenamiento</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <form (ngSubmit)="showConfirm(training)">\n    <ion-list>\n      <ion-item>\n        <ion-label floating>Nombre</ion-label>\n        <ion-input type="text" name="nombre" [(ngModel)]="training.nombre"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Descripcion</ion-label>\n        <ion-input type="text" name="descripcion" [(ngModel)]="training.descripcion"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Publico</ion-label>\n        <ion-select  name="tipo" [(ngModel)]="training.publico">\n              <ion-option value="si">Si</ion-option>\n              <ion-option value="no">No</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n            <ion-label floating>Dificultad</ion-label>\n            <ion-select  name="dificultad" [(ngModel)]="training.dificultad">\n              <ion-option value="facil">Fácil</ion-option>\n              <ion-option value="intermedio">Intermedio</ion-option>\n              <ion-option value="dificil">Difícil</ion-option>\n            </ion-select>\n      </ion-item>\n    </ion-list> \n    <div padding>\n      <button ion-button block type="submit">Guardar</button>\n    </div>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/edit-training/edit-training.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], EditTrainingPage);

//# sourceMappingURL=edit-training.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingsPersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detail_training_detail_training__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__create_training_create_training__ = __webpack_require__(151);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the TrainingsPersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TrainingsPersPage = (function () {
    function TrainingsPersPage(navCtrl, navParams, trainingService, storage, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.storage = storage;
        this.events = events;
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        }).then(function () {
            console.log(_this.user);
            _this.getTrainingsPers();
        }, function (error) {
            console.log(error);
        });
    }
    TrainingsPersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrainingsPersPage');
    };
    TrainingsPersPage.prototype.getTrainingsPers = function () {
        var _this = this;
        this.trainingService.getAllTrainingsPers(this.user.id).then(function (data) {
            if (data) {
                _this.trainings = data;
                console.log('Trainings descargados de la api.');
            }
            else {
                console.log('No hay o no se ha conseguido recoger trainings.');
            }
        });
    };
    TrainingsPersPage.prototype.goDetail = function (training) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detail_training_detail_training__["a" /* DetailTrainingPage */], {
            training: training,
            pers: true,
            user: this.user
        });
    };
    TrainingsPersPage.prototype.goCreate = function () {
        var _this = this;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__create_training_create_training__["a" /* CreateTrainingPage */], {
            user: this.user
        });
        this.events.subscribe('reloadTrainingsPersPage', function () {
            //this.events.publish('reloadTrainingsMenu');
            _this.getTrainingsPers();
        });
    };
    return TrainingsPersPage;
}());
TrainingsPersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-trainings-pers',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/trainings-pers/trainings-pers.html"*/'<!--\n  Generated template for the TrainingsPersPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n      <ion-title>Entrenamientos personalizados</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n      <ion-list>\n        <ion-item *ngFor="let training of trainings" (click)="goDetail(training)">\n          <ion-avatar item-start>\n            <img src="assets/img/pesa.png">\n          </ion-avatar>\n          <h2>{{training.nombre}}</h2>\n          <p>Dificultad: {{training.dificultad}}</p>\n        </ion-item>\n        <ion-item>\n          <button ion-fab color="primary" (click)="goCreate()"><ion-icon name="add"></ion-icon></button>\n        </ion-item>\n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/trainings-pers/trainings-pers.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], TrainingsPersPage);

//# sourceMappingURL=trainings-pers.js.map

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateTrainingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the CreateTrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CreateTrainingPage = (function () {
    function CreateTrainingPage(navCtrl, navParams, trainingService, toastCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.user = navParams.get('user');
        this.newTraining = {
            nombre: '',
            descripcion: '',
            publico: '',
            dificultad: '',
            UserId: this.user.id
        };
    }
    CreateTrainingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateTrainingPage');
    };
    CreateTrainingPage.prototype.createTraining = function () {
        var _this = this;
        this.trainingService.createTraining(this.newTraining).then(function () {
            _this.events.publish('reloadTrainingsPersPage');
            _this.presentToast('Entrenamiento creado con éxito.');
            _this.navCtrl.pop();
        });
    };
    CreateTrainingPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 4000,
            position: 'top'
        });
        toast.present();
    };
    return CreateTrainingPage;
}());
CreateTrainingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-create-training',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/create-training/create-training.html"*/'<!--\n  Generated template for the CreateTrainingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Creación de entrenamiento</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <form (ngSubmit)="createTraining()">\n        <ion-list>\n          <ion-item>\n            <ion-label floating>Nombre</ion-label>\n            <ion-input type="text" name="nombre" [(ngModel)]="newTraining.nombre"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating>Descripción</ion-label>\n            <ion-input type="text" name="descripcion" [(ngModel)]="newTraining.descripcion"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label floating>Público</ion-label>\n            <ion-select  name="publico" [(ngModel)]="newTraining.publico">\n              <ion-option value="si">Sí</ion-option>\n              <ion-option value="no">No</ion-option>\n            </ion-select>\n          </ion-item>\n          <ion-item>\n            <ion-label floating>Dificultad</ion-label>\n            <ion-select  name="dificultad" [(ngModel)]="newTraining.dificultad">\n              <ion-option value="facil">Fácil</ion-option>\n              <ion-option value="intermedio">Intermedio</ion-option>\n              <ion-option value="dificil">Difícil</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-list>\n        <div padding>\n          <button ion-button block type="submit">Guardar</button>\n        </div>\n      </form>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/create-training/create-training.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], CreateTrainingPage);

//# sourceMappingURL=create-training.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingsPublicsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detail_training_detail_training__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the TrainingsPublicsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TrainingsPublicsPage = (function () {
    function TrainingsPublicsPage(navCtrl, navParams, storage, trainingService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.trainingService = trainingService;
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        });
        this.trainingService.getAllTrainingsPublicos().then(function (results) {
            _this.trainings = results;
        });
    }
    TrainingsPublicsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrainingsPublicsPage');
    };
    TrainingsPublicsPage.prototype.goDetail = function (training) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detail_training_detail_training__["a" /* DetailTrainingPage */], {
            training: training,
            pers: false,
            user: this.user
        });
    };
    return TrainingsPublicsPage;
}());
TrainingsPublicsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-trainings-publics',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/trainings-publics/trainings-publics.html"*/'<!--\n  Generated template for the TrainingsPublicsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n      <ion-title>TOP10 Entrenamientos públicos</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n      <ion-list>\n        <ion-item *ngFor="let training of trainings" (click)="goDetail(training)">\n          <ion-avatar item-start>\n            <img src="assets/img/pesa.png">\n          </ion-avatar>\n          <h2>{{training.nombre}}</h2>\n          <p>Dificultad: {{training.dificultad}}</p>\n          <ion-icon name="thumbs-up" item-end></ion-icon>\n          <ion-badge item-end>{{training.votos}}</ion-badge>\n        </ion-item>\n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/trainings-publics/trainings-publics.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */]])
], TrainingsPublicsPage);

//# sourceMappingURL=trainings-publics.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuariosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__perfil_user_perfil_user__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the UsuariosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var UsuariosPage = (function () {
    function UsuariosPage(navCtrl, navParams, userService, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userService = userService;
        this.storage = storage;
        this.searchQuery = '';
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        });
        this.getUsers();
    }
    UsuariosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UsuariosPage');
    };
    UsuariosPage.prototype.getUsers = function () {
        var _this = this;
        this.userService.getUsers().then(function (data) {
            _this.userList = data;
        });
    };
    UsuariosPage.prototype.goProfile = function (userDetail) {
        console.log(userDetail);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__perfil_user_perfil_user__["a" /* PerfilUserPage */], {
            user: this.user,
            userDetail: userDetail
        });
    };
    UsuariosPage.prototype.getItems = function (ev) {
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.userList = this.userList.filter(function (otherUser) {
                return (otherUser.nick.toLowerCase().includes(val.toLowerCase()));
            });
        }
        else {
            this.getUsers();
        }
    };
    return UsuariosPage;
}());
UsuariosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-usuarios',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/usuarios/usuarios.html"*/'<!--\n  Generated template for the UsuariosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n  <ion-header>\n    <ion-navbar>\n      <ion-title>Lista de usuarios</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n  </ion-header>\n\n\n\n<ion-content padding>\n      <ion-list>\n        <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n        <ion-item *ngFor="let otherUser of userList" (click)="goProfile(otherUser)">\n          <ion-avatar item-start>\n            <img src="assets/img/user.png">\n          </ion-avatar>\n          <h2>{{otherUser.nick}}</h2>\n          <p>{{otherUser.email}}</p>\n        </ion-item>\n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/usuarios/usuarios.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
], UsuariosPage);

//# sourceMappingURL=usuarios.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_transfer__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PerfilUserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PerfilUserPage = (function () {
    function PerfilUserPage(navCtrl, navParams, userService, storage, file, transfer, alertCtrl, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userService = userService;
        this.storage = storage;
        this.file = file;
        this.transfer = transfer;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.esAmigo = false;
        this.bodymessage = "";
        this.tiempos = [];
        this.fechas = [];
        this.kcal = [];
        this.fechasPesos = [];
        this.pesos = [];
        this.urlLogros = [];
        this.user = this.navParams.get('user');
        this.userDetail = this.navParams.get('userDetail');
        this.userService.comprobarAmistad(this.user, this.userDetail).then(function (result) {
            console.log(result);
            _this.esAmigo = result;
        });
        var fileTransfer = this.transfer.create();
        fileTransfer.download('http://192.168.1.5:3000/avatar/' + this.userDetail.id, this.file.dataDirectory + this.userDetail.id + '.png').then(function (entry) {
            _this.avatarUrl = entry.toURL();
        }, function (error) {
            console.log(error);
        })
            .then(function () {
            _this.userService.getLogros().then(function (data) {
                _this.logros = data;
                for (var _i = 0, _a = _this.logros; _i < _a.length; _i++) {
                    var log = _a[_i];
                    _this.urlLogros.push("./assets/img/logros/" + log.id + "-png");
                }
            });
            _this.userService.getLogrosUser(_this.userDetail).then(function (data) {
                _this.logrosObtennidos = data;
            }).then(function () {
                var aux = false;
                for (var i = 0; i < _this.logros.length; i++) {
                    aux = false;
                    for (var j = 0; j < _this.logrosObtennidos.length; j++) {
                        if (_this.logros[i].id == _this.logrosObtennidos[j].LogroId) {
                            _this.tieneLogros.push(true);
                            aux = true;
                            break;
                        }
                    }
                    if (aux == false) {
                        _this.tieneLogros.push(false);
                    }
                }
            });
        })
            .then(function () {
            _this.getTodo();
        }).then(function () {
            _this.graficaTiempos();
            _this.graficaKcal();
            _this.graficaPesos();
        });
    }
    PerfilUserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PerfilUserPage');
    };
    PerfilUserPage.prototype.getTodo = function () {
        var _this = this;
        this.userService.getEstadisticas(this.userDetail).then(function (data) {
            console.log(data);
            _this.estadisticas = data;
        }).then(function () {
            var aux = '';
            for (var _i = 0, _a = _this.estadisticas; _i < _a.length; _i++) {
                var e = _a[_i];
                aux = e.createdAt.toString();
                _this.fechas.push(aux.substring(0, 10));
                _this.tiempos.push(e.tiempo / 60);
                _this.kcal.push(e.kcal);
            }
        })
            .then(function () {
            _this.userService.getPesos(_this.userDetail).then(function (data) {
                console.log(data);
                _this.estadisticaPesos = data;
            }).then(function () {
                var auxPesos = '';
                for (var _i = 0, _a = _this.estadisticaPesos; _i < _a.length; _i++) {
                    var p = _a[_i];
                    console.log(p);
                    auxPesos = p.createdAt.toString();
                    _this.fechasPesos.push(auxPesos.substring(0, 10));
                    _this.pesos.push(p.peso);
                }
            });
        });
    };
    PerfilUserPage.prototype.graficaTiempos = function () {
        this.tiempolabels = this.fechas;
        this.tiempodata = [
            {
                label: 'Tiempo(min) por entrenamiento',
                //data: [12, 19, 3, 5, 2, 3],
                data: this.tiempos,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ];
    };
    PerfilUserPage.prototype.graficaKcal = function () {
        //this.labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];
        this.kcallabels = this.fechas;
        this.kcaldata = [
            {
                label: 'Kcal quemadas por entrenamiento',
                //data: [12, 19, 3, 5, 2, 3],
                data: this.kcal,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ];
    };
    PerfilUserPage.prototype.graficaPesos = function () {
        this.pesoslabels = this.fechasPesos;
        this.pesosdata = [
            {
                label: 'Evolución de peso',
                //data: [12, 19, 3, 5, 2, 3],
                data: this.pesos,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ];
    };
    PerfilUserPage.prototype.agregarAmigo = function (useramigo) {
        var _this = this;
        //console.log(this.user);
        this.userService.agregarAmigo(this.user, useramigo);
        this.userService.comprobarLogro(this.user, 2).then(function (data) {
            console.log('EL DATA ES');
            console.log(data);
            if (!data) {
                _this.userService.agregarLogro(_this.user, 2).then(function (data) {
                    _this.presentToast('Has añadido a ' + _this.userDetail.nick + ' a tu list de amigos.');
                    _this.esAmigo = true;
                });
            }
        });
    };
    PerfilUserPage.prototype.enviarMensaje = function () {
        var _this = this;
        this.userService.enviarMensaje(this.user, this.userDetail, this.bodymessage).then(function (data) {
            _this.bodymessage = '';
            _this.presentToast('Mensaje enviado con éxito');
        });
    };
    PerfilUserPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 4000,
            position: 'top'
        });
        toast.present();
    };
    return PerfilUserPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineCanvas'),
    __metadata("design:type", Object)
], PerfilUserPage.prototype, "lineCanvas", void 0);
PerfilUserPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-perfil-user',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/perfil-user/perfil-user.html"*/'<!--\n  Generated template for the PerfilUserPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n      <ion-title>Perfil de usuario</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n <div padding>\n  <ion-segment [(ngModel)]="perfil">\n    <ion-segment-button value="informacion">\n      Información\n    </ion-segment-button>\n    <ion-segment-button value="estadisticas">\n      Estadísticas\n    </ion-segment-button>\n    <ion-segment-button value="logros">\n      Logros\n    </ion-segment-button>\n  </ion-segment>\n</div>\n\n<div [ngSwitch]="perfil">\n\n  <ion-list *ngSwitchCase="\'informacion\'" no-lines>\n   <ion-grid>\n      <ion-row>\n      </ion-row>\n      <ion-row>\n        <ion-col col-2></ion-col><ion-col col-8>  \n          <ion-item>\n            <p style="text-align: center">\n              <img class="avatarGrande" [src]="avatarUrl">\n            </p>\n          </ion-item>\n        </ion-col><ion-col col-2></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n            <ion-item>\n                <h2>Nick: {{userDetail.nick}}</h2><br>\n                <p>Email: {{userDetail.email}}</p><br>\n                <p>Fecha registro: {{userDetail.createdAt.substring(0,10)}}</p><br>                \n                <p>Peso: {{userDetail.peso}}</p><br>\n            </ion-item>\n            <ion-item>\n              <button ion-fab color="primary" *ngIf="!esAmigo" (click)="agregarAmigo(userDetail)"><ion-icon name="person-add"></ion-icon></button>\n            </ion-item>\n            <div  *ngIf="esAmigo">\n              <form (ngSubmit)="enviarMensaje()">\n                <ion-list>\n                  <ion-item>\n                    <ion-label floating>Mensaje privado</ion-label>\n                    <ion-input name="bodymessage" [(ngModel)]="bodymessage"></ion-input>\n                  </ion-item>\n                </ion-list>\n                <div padding>\n                  <button ion-button block type="submit">Enviar</button>\n                </div>\n              </form>\n            </div>\n        </ion-col>\n      </ion-row>\n   </ion-grid>\n  </ion-list>\n\n  <ion-list *ngSwitchCase="\'estadisticas\'">\n    <ion-title>Estadísticas</ion-title>\n    <ion-item>\n          <chart [labels]="tiempolabels" \n                  [data]="tiempodata" \n                  type="line"></chart>\n    </ion-item>\n    <ion-item>\n          <chart [labels]="kcallabels" \n                  [data]="kcaldata" \n                  type="line"></chart>\n    </ion-item>\n    <ion-item>\n          <chart [labels]="pesoslabels" \n                  [data]="pesosdata" \n                  type="line"></chart>\n    </ion-item>\n  </ion-list>\n\n  <ion-list *ngSwitchCase="\'logros\'">\n    <ion-title>Logros</ion-title>\n    <ion-item>\n      <h2>Logros</h2>\n    </ion-item>\n  </ion-list>\n</div>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/perfil-user/perfil-user.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_transfer__["a" /* Transfer */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */]])
], PerfilUserPage);

//# sourceMappingURL=perfil-user.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MensajesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the MensajesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MensajesPage = (function () {
    function MensajesPage(navCtrl, navParams, storage, userService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.userService = userService;
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        }).then(function () {
            _this.userService.getMensajesEnviados(_this.user).then(function (data) {
                _this.mensajesEnviados = data;
            });
        })
            .then(function () {
            _this.userService.getMensajesRecibidos(_this.user).then(function (data) {
                _this.mensajesRecibidos = data;
            });
        });
    }
    MensajesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MensajesPage');
    };
    return MensajesPage;
}());
MensajesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-mensajes',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/mensajes/mensajes.html"*/'<!--\n  Generated template for the MensajesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n      <ion-title>Mensajes privados</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n<ion-content padding>\n  <div padding>\n  <ion-segment [(ngModel)]="mensajes">\n    <ion-segment-button value="enviados">\n      Enviados\n    </ion-segment-button>\n    <ion-segment-button value="recibidos">\n      Recibidos\n    </ion-segment-button>\n  </ion-segment>\n</div>\n\n<div [ngSwitch]="mensajes">\n\n  <ion-list *ngSwitchCase="\'enviados\'" no-lines>\n      <ion-item>\n        <ion-list>\n          <ion-item *ngFor="let me of mensajesEnviados">\n                    <ion-card>\n                        <ion-card-header>\n                          {{me.nickReceptor}}\n                        </ion-card-header>\n                        <ion-card-content>\n                         <p style="white-space: pre-line;"> {{me.bodymessage}}</p>\n                        </ion-card-content>\n                    </ion-card>\n          </ion-item>\n        </ion-list>\n      </ion-item>\n  </ion-list>\n\n  <ion-list *ngSwitchCase="\'recibidos\'">\n    <ion-item *ngFor="let mr of mensajesRecibidos">\n                <ion-card>\n                    <ion-card-header>\n                      {{mr.nickEmisor}}\n                    </ion-card-header>\n                    <ion-card-content>\n                      <p style="white-space: pre-line;">{{mr.bodymessage}}</p>\n                    </ion-card-content>\n                </ion-card>\n      </ion-item>\n  </ion-list>\n</div>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/mensajes/mensajes.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserService */]])
], MensajesPage);

//# sourceMappingURL=mensajes.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RetosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_retos_retos__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__detalle_reto_detalle_reto__ = __webpack_require__(157);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RetosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RetosPage = (function () {
    function RetosPage(navCtrl, navParams, retosService, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.retosService = retosService;
        this.storage = storage;
        this.urlImages = [];
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        });
        this.retosService.getRetos().then(function (data) {
            console.log(data);
            _this.retos = data;
            for (var _i = 0, _a = _this.retos; _i < _a.length; _i++) {
                var i = _a[_i];
                _this.urlImages.push("./assets/img/retos/" + i.id + ".png");
            }
        });
    }
    RetosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RetosPage');
    };
    RetosPage.prototype.goDetalleReto = function (reto, imgUrl) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__detalle_reto_detalle_reto__["a" /* DetalleRetoPage */], {
            user: this.user,
            reto: reto,
            imgUrl: imgUrl
        });
    };
    return RetosPage;
}());
RetosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-retos',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/retos/retos.html"*/'<!--\n  Generated template for the RetosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n  <ion-header>\n    <ion-navbar>\n      <ion-title>Retos Olympiapp!</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n  </ion-header>\n\n\n\n<ion-content padding class="card-background-page">\n  <ion-card *ngFor="let reto of retos" (click)="goDetalleReto(reto,urlImages[reto.id-1])">\n    <img [src]="urlImages[reto.id-1]" />\n    <div class="card-title">{{reto.nombre}}</div>\n    <div class="card-subtitle">{{reto.tipo}}</div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/retos/retos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_retos_retos__["a" /* RetosService */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], RetosPage);

//# sourceMappingURL=retos.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalleRetoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_retos_retos__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__detail_exercise_detail_exercise__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DetalleRetoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetalleRetoPage = (function () {
    function DetalleRetoPage(navCtrl, navParams, retosService, trainingService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.retosService = retosService;
        this.trainingService = trainingService;
        this.detailedExercises = [];
        this.user = navParams.get('user');
        this.reto = navParams.get('reto');
        this.imgUrl = navParams.get('imgUrl');
        this.retosService.getExercisesReto(this.reto).then(function (data) {
            _this.exercises = data;
            for (var _i = 0, _a = _this.exercises; _i < _a.length; _i++) {
                var e = _a[_i];
                _this.getExercise(e.ExerciseId);
            }
        });
    }
    DetalleRetoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetalleRetoPage');
    };
    DetalleRetoPage.prototype.getExercise = function (idExercise) {
        var _this = this;
        this.trainingService.getExercise(idExercise).then(function (data) {
            if (data) {
                console.log('Resultado de getExercise');
                console.log(data);
                _this.detailedExercises.push(data);
            }
            else {
                console.log('No hay o no se ha conseguido recoger exercises.');
            }
        });
    };
    DetalleRetoPage.prototype.goDetailExercise = function (detailedExercise) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__detail_exercise_detail_exercise__["a" /* DetailExercisePage */], {
            exercise: detailedExercise,
            exerciseCustom: {
                repeticiones: 0,
                series: 0,
                peso: "",
                tiempo: 0
            },
            enEntrenamiento: false,
            pers: false
        });
    };
    return DetalleRetoPage;
}());
DetalleRetoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-detalle-reto',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/detalle-reto/detalle-reto.html"*/'<!--\n  Generated template for the DetalleRetoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Detalle del reto</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-list>\n      <ion-item>\n        <img [src]="imgUrl"/>\n      </ion-item>\n      <ion-item-divider color="light">Información general</ion-item-divider>\n      <ion-item>\n        <h2 style="padding-top: 2%;font-size: 18px;"> {{reto.nombre}} </h2>\n        <p style="white-space: pre-line; padding-top: 2%;">{{reto.descripcion}}</p>\n        <p style=" padding-top: 2%;"><span style="color: black">Tipo:</span> {{reto.tipo}}</p>\n        <p style=" padding-top: 2%;"><span style="color: black">Duracion:</span> {{reto.duracion}} dias</p>\n        <p style=" padding-top: 2%;"><span style="color: black">Descanso:</span> {{reto.descanso}}  dias</p>\n        <p style=" padding-top: 2%;"><span style="color: black">Progresión:</span> {{reto.progresion}}</p>\n\n      </ion-item>\n      <ion-item-divider color="light">Ejercicios</ion-item-divider>\n      <ion-item *ngFor="let exercise of detailedExercises" (click)="goDetailExercise(exercise)">\n        <p>{{exercise.nombre}}</p>\n      </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/detalle-reto/detalle-reto.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_retos_retos__["a" /* RetosService */],
        __WEBPACK_IMPORTED_MODULE_3__providers_training_training__["a" /* TrainingService */]])
], DetalleRetoPage);

//# sourceMappingURL=detalle-reto.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuscripcionesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detail_training_detail_training__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the SuscripcionesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SuscripcionesPage = (function () {
    function SuscripcionesPage(navCtrl, navParams, storage, trainingService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.trainingService = trainingService;
        this.trainingsSuscritos = [];
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        }).then(function () {
            console.log(_this.user);
            _this.getSuscripciones();
        }, function (error) {
            console.log(error);
        });
    }
    SuscripcionesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SuscripcionesPage');
    };
    SuscripcionesPage.prototype.getSuscripciones = function () {
        var _this = this;
        this.trainingService.getSuscripciones(this.user).then(function (data) {
            _this.suscripciones = data;
            for (var _i = 0, _a = _this.suscripciones; _i < _a.length; _i++) {
                var training = _a[_i];
                _this.trainingService.getTraining(training.TrainingId).then(function (data) {
                    _this.trainingsSuscritos.push(data);
                });
            }
        });
    };
    SuscripcionesPage.prototype.goDetail = function (training) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detail_training_detail_training__["a" /* DetailTrainingPage */], {
            training: training,
            pers: false,
            user: this.user
        });
    };
    return SuscripcionesPage;
}());
SuscripcionesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-suscripciones',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/suscripciones/suscripciones.html"*/'<!--\n  Generated template for the SuscripcionesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n  <ion-header>\n    <ion-navbar>\n      <ion-title>Entrenamientos suscritos</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n  </ion-header>\n\n\n\n<ion-content padding>\n <ion-list>\n    <ion-item *ngFor="let suscripcion of trainingsSuscritos" (click)="goDetail(suscripcion)">\n      <ion-avatar item-start>\n        <img src="assets/img/pesa.png">\n      </ion-avatar>\n      <h2>{{suscripcion.nombre}}</h2>\n      <p>Dificultad: {{suscripcion.dificultad}}</p>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/suscripciones/suscripciones.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */]])
], SuscripcionesPage);

//# sourceMappingURL=suscripciones.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__menu_menu__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_login_login__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_user__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, loginService, userService, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loginService = loginService;
        this.userService = userService;
        this.storage = storage;
        this.newLogin = {
            nick: '',
            password: ''
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.goRegister = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        this.loginService.postLogin(this.newLogin).then(function (data) {
            if (data) {
                //console.log(this.newLogin.nick);
                _this.userService.findUser({ nick: _this.newLogin.nick }).then(function (response) {
                    _this.goMenu(response);
                });
            }
            else {
                _this.goRegister();
            }
        });
    };
    LoginPage.prototype.goMenu = function (user) {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__menu_menu__["a" /* MenuPage */], { user: user });
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div style="margin-top:15%">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-2></ion-col><ion-col col-8>\n            <img src="./assets/img/logo.png" style="width:180px;height:180px;text-align: center">\n        </ion-col><ion-col col-2></ion-col>\n      </ion-row>\n    </ion-grid>\n  <form (ngSubmit)="login()" class="login"> \n    <ion-list>\n      <ion-item>\n        <ion-label floating>Nick</ion-label>\n        <ion-input type="text" name="nick" [(ngModel)]="newLogin.nick"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Password</ion-label>\n        <ion-input type="password" name="password" [(ngModel)]="newLogin.password"></ion-input>\n      </ion-item>\n    </ion-list>\n    <button ion-button type="submit">Acceder</button>\n  </form>\n  <p>¿No tienes cuenta? Regístrate en este <a (click)="goRegister()">enlace</a>!</p>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/login/login.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_4__providers_login_login__["a" /* LoginService */], __WEBPACK_IMPORTED_MODULE_5__providers_user_user__["a" /* UserService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_login_login__["a" /* LoginService */],
        __WEBPACK_IMPORTED_MODULE_5__providers_user_user__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 170:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 170;

/***/ }),

/***/ 213:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-ex-tr/add-ex-tr.module": [
		745,
		23
	],
	"../pages/amigos/amigos.module": [
		763,
		22
	],
	"../pages/create-exercise/create-exercise.module": [
		744,
		21
	],
	"../pages/create-training/create-training.module": [
		752,
		20
	],
	"../pages/detail-exercise/detail-exercise.module": [
		741,
		19
	],
	"../pages/detail-training/detail-training.module": [
		749,
		18
	],
	"../pages/detalle-reto/detalle-reto.module": [
		758,
		17
	],
	"../pages/edit-exercise/edit-exercise.module": [
		746,
		16
	],
	"../pages/edit-training/edit-training.module": [
		748,
		15
	],
	"../pages/exercises-pers/exercises-pers.module": [
		747,
		14
	],
	"../pages/login/login.module": [
		762,
		13
	],
	"../pages/mensajes/mensajes.module": [
		757,
		12
	],
	"../pages/menu/menu.module": [
		761,
		11
	],
	"../pages/news/news.module": [
		751,
		10
	],
	"../pages/perfil-user/perfil-user.module": [
		755,
		9
	],
	"../pages/play-training/play-training.module": [
		743,
		8
	],
	"../pages/register/register.module": [
		740,
		7
	],
	"../pages/retos/retos.module": [
		759,
		6
	],
	"../pages/suscripciones/suscripciones.module": [
		760,
		5
	],
	"../pages/training-finished/training-finished.module": [
		742,
		4
	],
	"../pages/trainings-pers/trainings-pers.module": [
		753,
		3
	],
	"../pages/trainings-publics/trainings-publics.module": [
		754,
		2
	],
	"../pages/trainings/trainings.module": [
		750,
		1
	],
	"../pages/usuarios/usuarios.module": [
		756,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 213;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainingService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the TrainingProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var TrainingService = (function () {
    function TrainingService(http, storage) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        this.pathTrainings = 'http://192.168.1.5:3000/api/trainings';
        this.pathTrainingsPers = 'http://192.168.1.5:3000/api/trainingsPers';
        this.pathExercises = 'http://192.168.1.5:3000/api/exercises';
        this.pathUsers = 'http://192.168.1.5:3000/api/users';
        this.pathMets = 'http://192.168.1.5:3000/api/mets';
        console.log('Hello TrainingProvider Provider');
        this.storage.get('token').then(function (value) {
            _this.token = value;
        });
    }
    TrainingService.prototype.getAllTrainings = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathTrainings + 'Pred', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getAllTrainingsPublicos = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathTrainings + 'Publicos', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                //resolve(JSON.parse(data['_body']));
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getAllTrainingsPers = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post(_this.pathTrainingsPers, { UserId: id }, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                //resolve(JSON.parse(data['_body']));
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getTraining = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathTrainings + '/' + id, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                //resolve(JSON.parse(data['_body']));
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getExercisesOf = function (idTraining) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathTrainings + "/" + idTraining + "/exercises", _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                //resolve(JSON.parse(data['_body']));
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getExercise = function (idExercise) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathExercises + "/" + idExercise, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                //resolve(JSON.parse(data['_body']));
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getImage = function (id, num) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathExercises + '/' + id + '/images/' + num, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getExerciseInTraining = function (idTraining, idExercise) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathTrainings + "/" + idTraining + "/exercises/" + idExercise, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                //resolve(JSON.parse(data['_body']));
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getMets = function (cat) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post(_this.pathMets, { categoria: cat }, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getAllMets = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathMets, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.createTraining = function (training) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post(_this.pathTrainings, training, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.createExercise = function (exercise) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post(_this.pathExercises, exercise, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.addToTraining = function (idtraining, exTr) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post(_this.pathTrainings + '/' + idtraining, exTr, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getExercisesByUser = function (UserId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post(_this.pathExercises + '/findByUser', { UserId: UserId }, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.deleteExercise = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.delete(_this.pathExercises + '/' + id, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.editExercise = function (exercise) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.put(_this.pathExercises + '/' + exercise.id, exercise, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.editTraining = function (training) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.put(_this.pathTrainings + '/' + training.id, training, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.uploadImage = function (exercise, num, ilustracion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post('http://192.168.1.5:3000/exercises/' + exercise.id + '/' + 'images/' + num, ilustracion, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.resetImages = function (exercise) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.delete(_this.pathExercises + '/' + exercise.id + '/images', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.comprobarVoto = function (user, training) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathTrainings + '/' + training.id + '/voto/' + user.id, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.votarEntrenamiento = function (user, training) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post(_this.pathTrainings + '/' + training.id + '/voto/' + user.id, {}, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.comentarEntrenamiento = function (user, training, bodycomment) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            var comentario = {
                nick: user.nick,
                UserId: user.id,
                bodycomment: bodycomment
            };
            _this.http.post(_this.pathTrainings + '/' + training.id + '/comments', comentario, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getComentarios = function (training) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathTrainings + '/' + training.id + '/comments', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.crearEstadistica = function (training, totalTime, totalKcal, user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            var estadistica = {
                UserId: user.id,
                TrainingId: training.id,
                tiempo: totalTime,
                kcal: totalKcal
            };
            _this.http.post(_this.pathUsers + '/' + user.id + '/stats', estadistica, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.getSuscripciones = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsers + '/' + user.id + '/suscripciones', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.addSuscripcion = function (user, training) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            var sus = {
                TrainingId: training.id,
            };
            _this.http.post(_this.pathUsers + '/' + user.id + '/suscripciones', sus, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TrainingService.prototype.comprobarSuscripcion = function (user, training) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsers + '/' + user.id + '/suscripciones/' + training.id, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return TrainingService;
}());
TrainingService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], TrainingService);

//# sourceMappingURL=training.js.map

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_transfer__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = (function () {
    function HomePage(navCtrl, navParams, userService, storage, file, transfer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userService = userService;
        this.storage = storage;
        this.file = file;
        this.transfer = transfer;
        this.tiempos = [];
        this.fechas = [];
        this.kcal = [];
        this.fechasPesos = [];
        this.pesos = [];
        this.urlLogros = [];
        this.urlLogrosFalse = [];
        this.tieneLogros = [];
        //const fileTransfer: FileTransferObject = this.transfer.create();   
        var fileTransfer = this.transfer.create();
        this.storage.get('user').then(function (data) {
            _this.user = JSON.parse(data);
        }).then(function () {
            fileTransfer.download('http://192.168.1.5:3000/avatar/' + _this.user.id, _this.file.dataDirectory + _this.user.id + '.png').then(function (entry) {
                _this.avatarUrl = entry.toURL();
            }, function (error) {
                console.log(error);
            });
        }).then(function () {
            _this.userService.getLogros().then(function (data) {
                _this.logros = data;
                for (var _i = 0, _a = _this.logros; _i < _a.length; _i++) {
                    var log = _a[_i];
                    _this.urlLogros.push("./assets/img/logros/" + log.id + "-true.png");
                    _this.urlLogrosFalse.push("./assets/img/logros/" + log.id + "-false.png");
                }
            });
            _this.userService.getLogrosUser(_this.user).then(function (data) {
                _this.logrosObtennidos = data;
            }).then(function () {
                var aux = false;
                for (var i = 0; i < _this.logros.length; i++) {
                    aux = false;
                    for (var j = 0; j < _this.logrosObtennidos.length; j++) {
                        if (_this.logros[i].id == _this.logrosObtennidos[j].LogroId) {
                            _this.tieneLogros.push(true);
                            aux = true;
                            break;
                        }
                    }
                    if (aux == false) {
                        _this.tieneLogros.push(false);
                    }
                }
            });
        })
            .then(function () {
            _this.getTodo();
        }).then(function () {
            _this.graficaTiempos();
            _this.graficaKcal();
            _this.graficaPesos();
        });
    }
    HomePage.prototype.getTodo = function () {
        var _this = this;
        this.userService.getEstadisticas(this.user).then(function (data) {
            console.log(data);
            _this.estadisticas = data;
        }).then(function () {
            var aux = '';
            for (var _i = 0, _a = _this.estadisticas; _i < _a.length; _i++) {
                var e = _a[_i];
                aux = e.createdAt.toString();
                _this.fechas.push(aux.substring(0, 10));
                _this.tiempos.push(e.tiempo / 60);
                _this.kcal.push(e.kcal);
            }
        })
            .then(function () {
            _this.userService.getPesos(_this.user).then(function (data) {
                console.log(data);
                _this.estadisticaPesos = data;
            }).then(function () {
                var auxPesos = '';
                for (var _i = 0, _a = _this.estadisticaPesos; _i < _a.length; _i++) {
                    var p = _a[_i];
                    console.log(p);
                    auxPesos = p.createdAt.toString();
                    _this.fechasPesos.push(auxPesos.substring(0, 10));
                    _this.pesos.push(p.peso);
                }
            });
        });
    };
    HomePage.prototype.graficaTiempos = function () {
        this.tiempolabels = this.fechas;
        this.tiempodata = [
            {
                label: 'Tiempo(min) por entrenamiento',
                //data: [12, 19, 3, 5, 2, 3],
                data: this.tiempos,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ];
    };
    HomePage.prototype.graficaKcal = function () {
        //this.labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];
        this.kcallabels = this.fechas;
        this.kcaldata = [
            {
                label: 'Kcal quemadas por entrenamiento',
                //data: [12, 19, 3, 5, 2, 3],
                data: this.kcal,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ];
    };
    HomePage.prototype.graficaPesos = function () {
        this.pesoslabels = this.fechasPesos;
        this.pesosdata = [
            {
                label: 'Evolución de peso',
                //data: [12, 19, 3, 5, 2, 3],
                data: this.pesos,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ];
    };
    HomePage.prototype.actualizarPeso = function () {
        this.userService.crearPeso(this.user);
        console.log(this.user);
        this.userService.actualizarUser(this.user);
        //añadir toast
    };
    return HomePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('lineCanvas'),
    __metadata("design:type", Object)
], HomePage.prototype, "lineCanvas", void 0);
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n      <ion-title>Home</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div padding>\n  <ion-segment [(ngModel)]="perfil">\n    <ion-segment-button value="informacion">\n      Información\n    </ion-segment-button>\n    <ion-segment-button value="estadisticas">\n      Estadísticas\n    </ion-segment-button>\n    <ion-segment-button value="logros">\n      Logros\n    </ion-segment-button>\n  </ion-segment>\n</div>\n\n<div [ngSwitch]="perfil">\n\n  <ion-list *ngSwitchCase="\'informacion\'" no-lines>\n   <ion-grid>\n      <ion-row>\n      </ion-row>\n      <ion-row>\n        <ion-col col-2></ion-col><ion-col col-8>  \n          <ion-item>\n            <p style="text-align: center">\n              <img [src]="avatarUrl">\n              <button ion-fab color="primary"><ion-icon name="camera"></ion-icon></button>\n            </p>\n          </ion-item>\n        </ion-col><ion-col col-2></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-12>\n            <ion-item>\n                <h2>Nick: {{user.nick}}</h2><br>\n                <p>Email: {{user.email}}</p><br>\n                <p>Fecha registro: {{user.createdAt.substring(0,10)}}</p><br>                \n            </ion-item>\n            <ion-item>\n              <ion-label floating>Peso (Kg)</ion-label>\n              <ion-input type="text" name="peso" [(ngModel)]="user.peso"></ion-input>\n            </ion-item>\n            <button ion-fab color="primary" (click)="actualizarPeso()"><ion-icon name="body"></ion-icon></button>\n        </ion-col>\n      </ion-row>\n   </ion-grid>\n  </ion-list>\n\n  <ion-list *ngSwitchCase="\'estadisticas\'">\n    <ion-title>Estadísticas</ion-title>\n    <ion-item>\n          <chart [labels]="tiempolabels" \n                  [data]="tiempodata" \n                  type="line"></chart>\n    </ion-item>\n    <ion-item>\n          <chart [labels]="kcallabels" \n                  [data]="kcaldata" \n                  type="line"></chart>\n    </ion-item>\n    <ion-item>\n          <chart [labels]="pesoslabels" \n                  [data]="pesosdata" \n                  type="line"></chart>\n    </ion-item>\n  </ion-list>\n\n  <ion-list *ngSwitchCase="\'logros\'">\n    <ion-title>Logros</ion-title>\n    <ion-item *ngFor="let logro of logros"> \n        <ion-avatar item-start>\n          <img style="width: 64px; height: 64px" *ngIf="tieneLogros[logro.id-1]" [src]="urlLogros[logro.id-1]">\n          <img style="width: 64px; height: 64px" *ngIf="!tieneLogros[logro.id-1]" [src]="urlLogrosFalse[logro.id-1]">\n        </ion-avatar>\n        <h2>{{logro.nombre}}</h2>\n        <p>{{logro.descripcion}}</p>\n    </ion-item>\n  </ion-list>\n</div>\n\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/home/home.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserService */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_transfer__["a" /* Transfer */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the LoginProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var LoginService = (function () {
    function LoginService(http, storage) {
        this.http = http;
        this.storage = storage;
        this.path = 'http://192.168.1.5:3000/login';
        this.login = null;
    }
    LoginService.prototype.postLogin = function (newLogin) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.path, newLogin)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                _this.login = data;
                var token = data.token;
                //console.log(data);
                _this.storage.set('token', token);
                //console.log(data)   ---lo devuelve bien  
                resolve(data);
            }, function (error) {
                //console.log(error)
                reject('Error en el login');
            });
        });
    };
    return LoginService;
}());
LoginService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], LoginService);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NewsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var NewsPage = (function () {
    function NewsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NewsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewsPage');
    };
    return NewsPage;
}());
NewsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-news',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/news/news.html"*/'<!--\n  Generated template for the NewsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n  <ion-header>\n    <ion-navbar>\n      <ion-title>News</ion-title>\n      <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n  </ion-header>\n\n\n\n<ion-content padding>\n<!-- <ion-card *ngFor="let noticia of news" (click)="showNew(noticia)">\n    <img [src]="urlImages[noticia.id-1]" />\n    <div class="card-title">{{noticia.nombre}}</div>\n    <div class="card-subtitle">{{noticia.descricpcion}}</div>\n  </ion-card>-->\n</ion-content>'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/news/news.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], NewsPage);

//# sourceMappingURL=news.js.map

/***/ }),

/***/ 418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AmigosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AmigosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AmigosPage = (function () {
    function AmigosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AmigosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AmigosPage');
    };
    return AmigosPage;
}());
AmigosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-amigos',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/amigos/amigos.html"*/'<!--\n  Generated template for the AmigosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>amigos</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/amigos/amigos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], AmigosPage);

//# sourceMappingURL=amigos.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(424);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(736);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_login_login__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_user_user__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_login_login__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_register_register__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_trainings_trainings__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_news_news__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_detail_training_detail_training__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_detail_exercise_detail_exercise__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_play_training_play_training__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_training_finished_training_finished__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_trainings_pers_trainings_pers__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_create_exercise_create_exercise__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_create_training_create_training__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_exercises_pers_exercises_pers__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_add_ex_tr_add_ex_tr__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_edit_exercise_edit_exercise__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_edit_training_edit_training__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_trainings_publics_trainings_publics__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_usuarios_usuarios__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_amigos_amigos__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_perfil_user_perfil_user__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_mensajes_mensajes__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_retos_retos__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_detalle_reto_detalle_reto__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_suscripciones_suscripciones__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_file__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_file_transfer__ = __webpack_require__(737);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_transfer__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39_ionic_native__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_file_path__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41_ng2_chartjs2__ = __webpack_require__(738);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41_ng2_chartjs2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_41_ng2_chartjs2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__node_modules_chart_js_dist_Chart_bundle_min_js__ = __webpack_require__(739);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__node_modules_chart_js_dist_Chart_bundle_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_42__node_modules_chart_js_dist_Chart_bundle_min_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_retos_retos__ = __webpack_require__(119);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










//paginas


































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__["a" /* MenuPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_news_news__["a" /* NewsPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_trainings_trainings__["a" /* TrainingsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_detail_training_detail_training__["a" /* DetailTrainingPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_detail_exercise_detail_exercise__["a" /* DetailExercisePage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_play_training_play_training__["a" /* PlayTrainingPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_training_finished_training_finished__["a" /* TrainingFinishedPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_trainings_pers_trainings_pers__["a" /* TrainingsPersPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_create_exercise_create_exercise__["a" /* CreateExercisePage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_exercises_pers_exercises_pers__["a" /* ExercisesPersPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_add_ex_tr_add_ex_tr__["a" /* AddExTrPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_create_training_create_training__["a" /* CreateTrainingPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_edit_exercise_edit_exercise__["a" /* EditExercisePage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_edit_training_edit_training__["a" /* EditTrainingPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_trainings_publics_trainings_publics__["a" /* TrainingsPublicsPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_usuarios_usuarios__["a" /* UsuariosPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_amigos_amigos__["a" /* AmigosPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_perfil_user_perfil_user__["a" /* PerfilUserPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_mensajes_mensajes__["a" /* MensajesPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_retos_retos__["a" /* RetosPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_detalle_reto_detalle_reto__["a" /* DetalleRetoPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_suscripciones_suscripciones__["a" /* SuscripcionesPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/detail-exercise/detail-exercise.module#DetailExercisePageModule', name: 'DetailExercisePage', segment: 'detail-exercise', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/training-finished/training-finished.module#TrainingFinishedPageModule', name: 'TrainingFinishedPage', segment: 'training-finished', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/play-training/play-training.module#PlayTrainingPageModule', name: 'PlayTrainingPage', segment: 'play-training', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/create-exercise/create-exercise.module#CreateExercisePageModule', name: 'CreateExercisePage', segment: 'create-exercise', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/add-ex-tr/add-ex-tr.module#AddExTrPageModule', name: 'AddExTrPage', segment: 'add-ex-tr', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/edit-exercise/edit-exercise.module#EditExercisePageModule', name: 'EditExercisePage', segment: 'edit-exercise', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/exercises-pers/exercises-pers.module#ExercisesPersPageModule', name: 'ExercisesPersPage', segment: 'exercises-pers', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/edit-training/edit-training.module#EditTrainingPageModule', name: 'EditTrainingPage', segment: 'edit-training', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/detail-training/detail-training.module#DetailTrainingPageModule', name: 'DetailTrainingPage', segment: 'detail-training', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/trainings/trainings.module#TrainingsPageModule', name: 'TrainingsPage', segment: 'trainings', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/news/news.module#NewsPageModule', name: 'NewsPage', segment: 'news', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/create-training/create-training.module#CreateTrainingPageModule', name: 'CreateTrainingPage', segment: 'create-training', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/trainings-pers/trainings-pers.module#TrainingsPersPageModule', name: 'TrainingsPersPage', segment: 'trainings-pers', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/trainings-publics/trainings-publics.module#TrainingsPublicsPageModule', name: 'TrainingsPublicsPage', segment: 'trainings-publics', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/perfil-user/perfil-user.module#PerfilUserPageModule', name: 'PerfilUserPage', segment: 'perfil-user', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/usuarios/usuarios.module#UsuariosPageModule', name: 'UsuariosPage', segment: 'usuarios', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/mensajes/mensajes.module#MensajesPageModule', name: 'MensajesPage', segment: 'mensajes', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/detalle-reto/detalle-reto.module#DetalleRetoPageModule', name: 'DetalleRetoPage', segment: 'detalle-reto', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/retos/retos.module#RetosPageModule', name: 'RetosPage', segment: 'retos', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/suscripciones/suscripciones.module#SuscripcionesPageModule', name: 'SuscripcionesPage', segment: 'suscripciones', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/amigos/amigos.module#AmigosPageModule', name: 'AmigosPage', segment: 'amigos', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_41_ng2_chartjs2__["ChartModule"]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_news_news__["a" /* NewsPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__["a" /* MenuPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_trainings_trainings__["a" /* TrainingsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_detail_training_detail_training__["a" /* DetailTrainingPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_detail_exercise_detail_exercise__["a" /* DetailExercisePage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_play_training_play_training__["a" /* PlayTrainingPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_training_finished_training_finished__["a" /* TrainingFinishedPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_trainings_pers_trainings_pers__["a" /* TrainingsPersPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_create_exercise_create_exercise__["a" /* CreateExercisePage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_exercises_pers_exercises_pers__["a" /* ExercisesPersPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_add_ex_tr_add_ex_tr__["a" /* AddExTrPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_create_training_create_training__["a" /* CreateTrainingPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_edit_exercise_edit_exercise__["a" /* EditExercisePage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_edit_training_edit_training__["a" /* EditTrainingPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_trainings_publics_trainings_publics__["a" /* TrainingsPublicsPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_usuarios_usuarios__["a" /* UsuariosPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_amigos_amigos__["a" /* AmigosPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_perfil_user_perfil_user__["a" /* PerfilUserPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_mensajes_mensajes__["a" /* MensajesPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_retos_retos__["a" /* RetosPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_detalle_reto_detalle_reto__["a" /* DetalleRetoPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_suscripciones_suscripciones__["a" /* SuscripcionesPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_35__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_36__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_36__ionic_native_file_transfer__["b" /* FileTransferObject */],
            __WEBPACK_IMPORTED_MODULE_37__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_37__ionic_native_transfer__["b" /* TransferObject */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_8__providers_login_login__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_9__providers_user_user__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_38__providers_training_training__["a" /* TrainingService */],
            __WEBPACK_IMPORTED_MODULE_39_ionic_native__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_40__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_43__providers_retos_retos__["a" /* RetosService */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var UserService = (function () {
    function UserService(http, storage) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        this.pathRegistro = 'http://192.168.1.5:3000/register';
        this.pathUsuarios = 'http://192.168.1.5:3000/api/users';
        this.pathLogros = 'http://192.168.1.5:3000/api/logros';
        this.user = null;
        this.storage.get('token').then(function (value) {
            _this.token = value;
        });
    }
    UserService.prototype.findUser = function (nick) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.post(_this.pathUsuarios + '/find', JSON.stringify(nick), _this.opt)
                .subscribe(function (data) {
                resolve(JSON.parse(data['_body']));
            });
        });
    };
    UserService.prototype.getAvatar = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            //this.myHeaders.append('Content-Type' , 'application/json')
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios + '/' + id + '/avatar', _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.postUser = function (newUser) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.pathRegistro, newUser)
                .subscribe(function (data) {
                resolve(_this.user);
            });
        });
    };
    UserService.prototype.getEstadisticas = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios + '/' + user.id + '/stats', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.crearPeso = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            var peso = {
                UserId: user.id,
                peso: user.peso
            };
            _this.http.post(_this.pathUsuarios + '/' + user.id + '/pesos', peso, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.getPesos = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios + '/' + user.id + '/pesos', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.actualizarUser = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.put(_this.pathUsuarios + '/' + user.id, user, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.getUsers = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.agregarAmigo = function (user, userAmigo) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            console.log(user);
            var amigo = { UserAgregado: userAmigo.id };
            _this.http.post(_this.pathUsuarios + '/' + user.id + '/amigos', amigo, _this.opt)
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.comprobarAmistad = function (user, userDetail) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios + '/' + user.id + '/amigos/' + userDetail.id, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.enviarMensaje = function (emisor, receptor, mensaje) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            var MG = {
                nickReceptor: receptor.nick,
                nickEmisor: emisor.nick,
                UserEmisor: emisor.id,
                bodymessage: mensaje
            };
            _this.http.post(_this.pathUsuarios + '/' + receptor.id + '/mensajes', MG, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.getMensajesEnviados = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios + '/' + user.id + '/mensajesEnviados', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.getMensajesRecibidos = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios + '/' + user.id + '/mensajesRecibidos', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.getLogros = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathLogros, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.getLogrosUser = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios + '/' + user.id + '/logros', _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.comprobarLogro = function (user, logro) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            _this.http.get(_this.pathUsuarios + '/' + user.id + '/logros/' + logro, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    UserService.prototype.agregarLogro = function (user, logro) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */];
            var auth = 'JWT ' + _this.token;
            _this.myHeaders.append('Authorization', 'JWT ' + _this.token);
            _this.myHeaders.append('Content-Type', 'application/json');
            _this.opt = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({
                headers: _this.myHeaders
            });
            var logroConseguido = {
                LogroId: logro
            };
            _this.http.post(_this.pathUsuarios + '/' + user.id + '/logros', logroConseguido, _this.opt)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return UserService;
}());
UserService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], UserService);

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTrainingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detail_exercise_detail_exercise__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__play_training_play_training__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__exercises_pers_exercises_pers__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__edit_training_edit_training__ = __webpack_require__(149);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the DetailTrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailTrainingPage = (function () {
    function DetailTrainingPage(navCtrl, navParams, trainingService, modalCtrl, events, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.toastCtrl = toastCtrl;
        this.detailedExercises = [];
        this.vacio = true;
        this.comentable = true;
        this.user = navParams.get('user');
        this.pers = navParams.get('pers');
        this.training = navParams.get('training');
        this.bodycomment = '';
        if (this.training.UserId == this.user.id) {
            this.propietario = true;
        }
        else {
            this.propietario = false;
        }
        this.getExercisesOf(this.training.id).then(function (data) {
            for (var _i = 0, _a = _this.exercises; _i < _a.length; _i++) {
                var e = _a[_i];
                _this.getExercise(e.ExerciseId);
            }
        }).then(function () {
            if (_this.exercises.length > 0)
                _this.vacio = false;
            else
                _this.vacio = true;
            if (_this.training.publico != "pr") {
                _this.trainingService.comprobarVoto(_this.user, _this.training).then(function (data) {
                    _this.votado = data;
                });
            }
            else {
                _this.votado = true;
                _this.comentable = false;
            }
            console.log(_this.vacio);
            _this.getComentarios();
        });
        this.comprobarSuscripcion();
    }
    DetailTrainingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailTrainingPage');
    };
    DetailTrainingPage.prototype.getExercisesOf = function (idTraining) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.trainingService.getExercisesOf(idTraining).then(function (data) {
                if (data) {
                    _this.exercises = data;
                    console.log('Exercises descargados de la api.');
                    resolve(data);
                }
                else {
                    console.log('No hay o no se ha conseguido recoger exercises.');
                    reject('No hay o no se ha conseguido recoger exercises.');
                }
            });
        });
    };
    DetailTrainingPage.prototype.getExercise = function (idExercise) {
        var _this = this;
        this.trainingService.getExercise(idExercise).then(function (data) {
            if (data) {
                console.log('Resultado de getExercise');
                console.log(data);
                _this.detailedExercises.push(data);
            }
            else {
                console.log('No hay o no se ha conseguido recoger exercises.');
            }
        });
    };
    DetailTrainingPage.prototype.goDetailExercise = function (detailedExercise) {
        var _this = this;
        var exercise = null;
        this.trainingService.getExerciseInTraining(this.training.id, detailedExercise.id)
            .then(function (data) {
            exercise = data;
        }).then(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detail_exercise_detail_exercise__["a" /* DetailExercisePage */], {
                exercise: detailedExercise,
                exerciseCustom: exercise,
                enEntrenamiento: true,
                pers: _this.pers
            });
        });
    };
    DetailTrainingPage.prototype.goPlayTraining = function (training, exercises, detailedExercises) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__play_training_play_training__["a" /* PlayTrainingPage */], {
            training: this.training,
            exercises: this.exercises,
            detailedExercises: this.detailedExercises,
            currentEx: 0,
            totalTime: 0,
            totalKcal: 0.0
        });
    };
    DetailTrainingPage.prototype.goEdit = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__edit_training_edit_training__["a" /* EditTrainingPage */], {
            user: this.user,
            training: this.training
        });
    };
    DetailTrainingPage.prototype.vote = function () {
        var _this = this;
        this.training.votos = parseInt(this.training.votos) + 1;
        this.trainingService.editTraining(this.training).then(function () {
            _this.votado = true;
            console.log(_this.training);
            _this.trainingService.votarEntrenamiento(_this.user, _this.training);
            _this.presentToast('Has votado el entrenamiento!');
        });
    };
    DetailTrainingPage.prototype.goExercises = function () {
        var _this = this;
        var chooseModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__exercises_pers_exercises_pers__["a" /* ExercisesPersPage */], { training: this.training });
        chooseModal.onDidDismiss(function (data) {
            console.log(data);
        });
        chooseModal.present();
        this.events.subscribe('reloadDetailTrainingPage', function () {
            /*this.navCtrl.pop().then(()=>{
              this.navCtrl.push(DetailTrainingPage,{
                pers:this.pers,
              get  training:this.training
              });
            });*/
            _this.getExercisesOf(_this.training.id).then(function (data) {
                for (var _i = 0, _a = _this.exercises; _i < _a.length; _i++) {
                    var e = _a[_i];
                    _this.getExercise(e.ExerciseId);
                }
            }).then(function () {
                if (_this.exercises.length > 0)
                    _this.vacio = false;
                else
                    _this.vacio = true;
            });
            _this.presentToast('Ejercicio añadido con éxito');
        });
    };
    DetailTrainingPage.prototype.getComentarios = function () {
        var _this = this;
        this.trainingService.getComentarios(this.training).then(function (results) {
            _this.comentarios = results;
        });
    };
    DetailTrainingPage.prototype.createComment = function () {
        var _this = this;
        this.trainingService.comentarEntrenamiento(this.user, this.training, this.bodycomment).then(function () {
            _this.bodycomment = '';
            _this.presentToast('Se ha comentado con éito.');
            _this.getComentarios();
        });
    };
    DetailTrainingPage.prototype.suscribirse = function () {
        var _this = this;
        this.trainingService.addSuscripcion(this.user, this.training).then(function (data) {
            _this.fav = true;
            var toast = _this.toastCtrl.create({
                message: 'Te has suscrito con éxito.',
                duration: 3000
            });
            toast.present();
        });
    };
    DetailTrainingPage.prototype.comprobarSuscripcion = function () {
        var _this = this;
        this.trainingService.comprobarSuscripcion(this.user, this.training).then(function (data) {
            if (data == true) {
                _this.fav = true;
            }
            else {
                _this.fav = false;
            }
        });
    };
    DetailTrainingPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 4000,
            position: 'top'
        });
        toast.present();
    };
    return DetailTrainingPage;
}());
DetailTrainingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-detail-training',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/detail-training/detail-training.html"*/'<!--\n  Generated template for the DetailTrainingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Detalle del entrenamiento</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<ion-list>\n    <ion-item-divider color="light">Información general</ion-item-divider>\n    <ion-item>\n      <h2 style="padding-top: 2%;font-size: 18px;"> {{training.nombre}} </h2>\n      <p style="white-space: pre-line; padding-top: 2%;">{{training.descripcion}}</p>\n      <p style=" padding-top: 2%;"><span style="color: black">Dificultad:</span> {{training.dificultad}}</p>\n      <p style=" padding-top: 2%;"><span style="color: black">Votos:</span> {{training.votos}}</p>\n\n      <button ion-fab color="primary" *ngIf="pers" (click)="goEdit()"><ion-icon name="create"></ion-icon></button>\n    </ion-item>\n    <ion-item-divider color="light">Ejercicios</ion-item-divider>\n    <ion-item *ngFor="let exercise of detailedExercises" (click)="goDetailExercise(exercise)">\n      <p>{{exercise.nombre}}</p>\n    </ion-item>\n    <ion-item>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-3 style="text-align: left">\n            <button ion-fab color="primary" *ngIf="pers" (click)="goExercises()"><ion-icon name="add"></ion-icon></button>\n          </ion-col>\n          <ion-col col-3 style="text-align: right">\n            <button ion-fab color="primary" *ngIf="!vacio" (click)="goPlayTraining()"><ion-icon name="play"></ion-icon></button>  \n          </ion-col>\n          <ion-col col-3 style="text-align: right">\n            <button ion-fab color="primary" *ngIf="votado==false&&propietario==false" (click)="vote()"><ion-icon name="thumbs-up"></ion-icon></button>  \n          </ion-col>\n          <ion-col col-3 style="text-align: right">\n            <button ion-fab color="primary" *ngIf="!propietario&&!fav" (click)="suscribirse()"><ion-icon name="heart"></ion-icon></button>  \n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n   <ion-item-divider *ngIf="comentable" color="light">Comentarios</ion-item-divider>\n</ion-list>\n    <div  *ngIf="comentable">\n            <form (ngSubmit)="createComment()">\n              <ion-list>\n                <ion-item>\n                  <ion-label floating>Comentario</ion-label>\n                  <ion-input name="bodycomment" [(ngModel)]="bodycomment"></ion-input>\n                </ion-item>\n              </ion-list>\n              <div padding>\n                <button ion-button block type="submit">Comentar</button>\n              </div>\n            </form>\n    </div>\n    <ion-list  *ngIf="comentable">\n      <ion-item *ngFor="let comment of comentarios">\n                <ion-card>\n                    <ion-card-header>\n                      {{comment.nick}}\n                    </ion-card-header>\n                    <ion-card-content>\n                      <p style="white-space: pre-line;">{{comment.bodycomment}}</p>\n                    </ion-card-content>\n                </ion-card>\n      </ion-item>\n  </ion-list>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/detail-training/detail-training.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */]])
], DetailTrainingPage);

//# sourceMappingURL=detail-training.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailExercisePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_training_training__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_native__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__ = __webpack_require__(372);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the DetailExercisePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailExercisePage = (function () {
    function DetailExercisePage(navCtrl, navParams, trainingService, loadingCtrl, alertCtrl, file, actionSheetCtrl, toastCtrl, camera, filePath, 
        // public transfer: FileTransfer
        transfer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.trainingService = trainingService;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.file = file;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.camera = camera;
        this.filePath = filePath;
        this.transfer = transfer;
        this.images = [];
        this.lastImage = null;
        console.log('LLEGAMOS AQUI??');
        this.enEntrenamiento = navParams.get('enEntrenamiento');
        this.exercise = navParams.get('exercise');
        this.exerciseCustom = navParams.get('exerciseCustom');
        this.pers = navParams.get('pers');
        var fileTransfer = this.transfer.create();
        this.updateExerciseInfo();
        if (this.enEntrenamiento) {
            console.log(this.exerciseCustom);
        }
        if (this.exercise.img > 0) {
            for (var i = 1; i <= this.exercise.img; i++) {
                fileTransfer.download('http://192.168.1.5:3000/exercises/' + this.exercise.id + '/images/' + i, this.file.dataDirectory + this.exercise.id + '-' + i + '.png').then(function (entry) {
                    _this.images.push(entry.toURL());
                }, function (error) {
                    console.log(error);
                });
            }
        }
    }
    DetailExercisePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailExercisePage');
    };
    DetailExercisePage.prototype.goToSlide = function () {
        this.slides.slideTo(2, 500);
    };
    DetailExercisePage.prototype.slideChanged = function () {
        var currentIndex = this.slides.getActiveIndex();
        console.log('Current index is', currentIndex);
    };
    DetailExercisePage.prototype.updateExerciseInfo = function () {
        var _this = this;
        this.trainingService.getExercise(this.exercise.id).then(function (data) {
            _this.exercise = data;
        });
    };
    DetailExercisePage.prototype.getImages = function () {
        var _this = this;
        var fileTransfer = this.transfer.create();
        for (var i = 1; i <= this.exercise.img; i++) {
            fileTransfer.download('http://192.168.1.5:3000/exercises/' + this.exercise.id + '/images/' + i, this.file.dataDirectory + 'ejercicio-' + i + '.png').then(function (entry) {
                _this.images.push(entry.toURL());
            }, function (error) {
                console.log(error);
            });
        }
    };
    DetailExercisePage.prototype.accionesImagen = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(__WEBPACK_IMPORTED_MODULE_5_ionic_native__["a" /* Camera */].PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(__WEBPACK_IMPORTED_MODULE_5_ionic_native__["a" /* Camera */].PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    DetailExercisePage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        __WEBPACK_IMPORTED_MODULE_5_ionic_native__["a" /* Camera */].getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (sourceType === __WEBPACK_IMPORTED_MODULE_5_ionic_native__["a" /* Camera */].PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    /*uploadImage(num:any,ilustracion:any){
      this.trainingService.uploadImage(this.exercise,num,ilustracion).then((data)=>{
        console.log(data);
      });
    }*/
    DetailExercisePage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    DetailExercisePage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.rutaCompleta = _this.file.dataDirectory + '/' + newFileName;
        }, function (error) {
            _this.presentToast('Error while storing file.');
        }).then(function () {
            var confirm = _this.alertCtrl.create({
                title: 'Subir imagen',
                message: '¿Seguro que quieres subir la imagen?',
                buttons: [
                    {
                        text: 'SI',
                        handler: function () {
                            console.log('SI clicked');
                            _this.uploadImage();
                        }
                    },
                    {
                        text: 'NO',
                        handler: function () {
                            console.log('NO clicked');
                        }
                    }
                ]
            });
            confirm.present();
        });
    };
    DetailExercisePage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 6000,
            position: 'top'
        });
        toast.present();
    };
    DetailExercisePage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return this.file.dataDirectory + img;
        }
    };
    DetailExercisePage.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        var url = "http://192.168.1.5:3000/exercises/" + this.exercise.id + "/images/" + (parseInt(this.exercise.img) + 1);
        // File for Upload
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(this.rutaCompleta, url, options).then(function (data) {
            _this.loading.dismissAll();
            _this.exercise.img = _this.exercise.img + 1;
            _this.trainingService.editExercise(_this.exercise).then(function () {
                _this.getImages();
            });
            _this.presentToast('Image succesful uploaded.');
        }, function (err) {
            console.log(err);
            _this.loading.dismissAll();
            _this.presentToast('Error while uploading file.');
        });
    };
    DetailExercisePage.prototype.showReset = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Borrar imagenes',
            message: '¿Seguro que quieres borrar las imagenes?',
            buttons: [
                {
                    text: 'SI',
                    handler: function () {
                        console.log('SI clicked');
                        _this.resetImages();
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        console.log('NO clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    DetailExercisePage.prototype.resetImages = function () {
        var _this = this;
        this.trainingService.resetImages(this.exercise).then(function () {
            _this.trainingService.getExercise(_this.exercise.id).then(function (data) {
                _this.exercise = data;
                _this.images = [];
            });
        });
    };
    return DetailExercisePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */])
], DetailExercisePage.prototype, "slides", void 0);
DetailExercisePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-detail-exercise',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/detail-exercise/detail-exercise.html"*/'<!--\n  Generated template for the DetailExercisePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Detalle del ejercicio</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n      <ion-item-divider color="light">Información general</ion-item-divider>\n      <ion-item>\n        <h2 style="padding-top: 2%;font-size: 18px;"> {{exercise.nombre}} </h2>\n        <p style="white-space: pre-line; padding-top: 2%;">{{exercise.descripcion}}</p>\n        <p style=" padding-top: 2%;"><span style="color: black">Tipo:</span> {{exercise.tipo}}</p>\n        <p *ngIf="exerciseCustom.series>0 && enEntrenamiento" style=" padding-top: 2%;"><span style="color: black">Series:</span> {{exerciseCustom.series}}</p>\n        <p *ngIf="exerciseCustom.repeticiones>0 && enEntrenamiento" style=" padding-top: 2%;"><span style="color: black">Repeticiones:</span> {{exerciseCustom.repeticiones}}</p>\n        <p *ngIf="!exerciseCustom.peso && enEntrenamiento" style=" padding-top: 2%;"><span style="color: black">Peso:</span> {{exerciseCustom.peso}}</p>\n        <p *ngIf="exerciseCustom.tiempo>0 && enEntrenamiento" style=" padding-top: 2%;"><span style="color: black">Tiempo:</span> {{exerciseCustom.tiempo}}</p>\n      </ion-item>\n      <ion-item-divider color="light">Imagenes</ion-item-divider>\n      <ion-item>\n        <ion-slides (ionSlideDidChange)="slideChanged()">\n          <ion-slide *ngFor="let image of images">\n            <img [src]="image">\n          </ion-slide>\n        </ion-slides>\n        <p *ngIf="images.length<1">Sin imagenes...</p>\n        <button ion-fab color="primary" *ngIf="exercise.img<4&&pers" (click)="accionesImagen()"><ion-icon name="camera"></ion-icon></button>\n        <button ion-fab color="primary" *ngIf="pers" (click)="showReset()"><ion-icon name="close-circle"></ion-icon></button>\n\n       <!-- <img [src]="rutaCompleta" *ngIf="lastImage" /> -->\n      </ion-item>\n\n\n      \n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/detail-exercise/detail-exercise.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_training_training__["a" /* TrainingService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_native__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__["a" /* FilePath */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__["a" /* Transfer */]])
], DetailExercisePage);

//# sourceMappingURL=detail-exercise.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(159);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__trainings_trainings__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__trainings_pers_trainings_pers__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__trainings_publics_trainings_publics__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__usuarios_usuarios__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__mensajes_mensajes__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__retos_retos__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__suscripciones_suscripciones__ = __webpack_require__(158);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//paginas








/**
 * Generated class for the MenuPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MenuPage = (function () {
    function MenuPage(navCtrl, navParams, storage, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.events = events;
        this.pages = [{
                title: 'Home',
                icon: 'home',
                component: __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]
            }, {
                title: 'Entrenamientos predefinidos',
                icon: 'walk',
                component: __WEBPACK_IMPORTED_MODULE_4__trainings_trainings__["a" /* TrainingsPage */]
            }, {
                title: 'Entrenamientos personalizados',
                icon: 'walk',
                component: __WEBPACK_IMPORTED_MODULE_5__trainings_pers_trainings_pers__["a" /* TrainingsPersPage */]
            }, {
                title: 'TOP10 Entrenamientos públicos',
                icon: 'walk',
                component: __WEBPACK_IMPORTED_MODULE_6__trainings_publics_trainings_publics__["a" /* TrainingsPublicsPage */]
            }, {
                title: 'Lista de usuarios',
                icon: 'walk',
                component: __WEBPACK_IMPORTED_MODULE_7__usuarios_usuarios__["a" /* UsuariosPage */]
            }, {
                title: 'Mensajes privados',
                icon: 'walk',
                component: __WEBPACK_IMPORTED_MODULE_8__mensajes_mensajes__["a" /* MensajesPage */]
            }, {
                title: 'Retos',
                icon: 'walk',
                component: __WEBPACK_IMPORTED_MODULE_9__retos_retos__["a" /* RetosPage */]
            }, {
                title: 'Suscripciones',
                icon: 'walk',
                component: __WEBPACK_IMPORTED_MODULE_10__suscripciones_suscripciones__["a" /* SuscripcionesPage */]
            }
        ];
        this.rootPage = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.user = this.navParams.get('user');
        console.log(this.user);
        this.storage.set('user', JSON.stringify(this.user)).then(function (data) {
            console.log(data);
        });
    }
    MenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenuPage');
    };
    MenuPage.prototype.openPage = function (page) {
        console.log(page);
        this.rootPage = page, { 'user': this.user };
        //this.navCtrl.setRoot(page,{ 'user' : this.user});
        //control de refresco de Entrenamientos personalizados
        /*this.events.subscribe('reloadTrainingsMenu',() => {
            this.rootPage = NewsPage,{'user':this.user};
            this.rootPage = TrainingsPersPage,{'user':this.user};
          });*/
    };
    return MenuPage;
}());
MenuPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-menu',template:/*ion-inline-start:"/home/hector/Desarrollo/tfg/app3/src/pages/menu/menu.html"*/'<!--\n  Generated template for the MenuPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-menu [content]="content">\n  <ion-content>\n    <ion-list>\n      <button *ngFor="let page of pages" menuClose ion-item (click)="openPage(page.component)">{{page.title}}</button>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<ion-nav #content [root]="rootPage"></ion-nav>'/*ion-inline-end:"/home/hector/Desarrollo/tfg/app3/src/pages/menu/menu.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], MenuPage);

//# sourceMappingURL=menu.js.map

/***/ })

},[419]);
//# sourceMappingURL=main.js.map