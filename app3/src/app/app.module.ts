import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule} from '@angular/http';
import { MyApp } from './app.component';
import { LoginService } from '../providers/login/login';
import { UserService } from '../providers/user/user';

//paginas
import { MenuPage } from '../pages/menu/menu';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { TrainingsPage } from '../pages/trainings/trainings';
import { NewsPage } from '../pages/news/news';
import { DetailTrainingPage } from '../pages/detail-training/detail-training';
import { DetailExercisePage } from '../pages/detail-exercise/detail-exercise';
import { PlayTrainingPage } from '../pages/play-training/play-training';
import { TrainingFinishedPage } from '../pages/training-finished/training-finished';
import { TrainingsPersPage } from '../pages/trainings-pers/trainings-pers';
import { CreateExercisePage } from '../pages/create-exercise/create-exercise';
import { CreateTrainingPage } from '../pages/create-training/create-training';
import { ExercisesPersPage } from '../pages/exercises-pers/exercises-pers';
import { AddExTrPage } from '../pages/add-ex-tr/add-ex-tr';
import { EditExercisePage } from '../pages/edit-exercise/edit-exercise';
import { EditTrainingPage } from '../pages/edit-training/edit-training';
import { TrainingsPublicsPage } from '../pages/trainings-publics/trainings-publics';
import { UsuariosPage } from '../pages/usuarios/usuarios';
import { AmigosPage } from '../pages/amigos/amigos';
import { PerfilUserPage } from '../pages/perfil-user/perfil-user';
import { MensajesPage } from '../pages/mensajes/mensajes';
import { RetosPage } from '../pages/retos/retos';
import { DetalleRetoPage } from '../pages/detalle-reto/detalle-reto';
import { SuscripcionesPage } from '../pages/suscripciones/suscripciones';


import { File } from '@ionic-native/file';
import { FileTransfer,FileTransferObject } from '@ionic-native/file-transfer';
import { Transfer,TransferObject } from '@ionic-native/transfer';
import { TrainingService } from '../providers/training/training';
import { Camera } from 'ionic-native';
import { FilePath } from '@ionic-native/file-path';
import { ChartModule } from 'ng2-chartjs2';
import { Chart } from 'chart.js';
import '../../node_modules/chart.js/dist/Chart.bundle.min.js';
import { RetosService } from '../providers/retos/retos'; 


@NgModule({
  declarations: [
    MyApp,
    MenuPage,
    NewsPage,
    HomePage,
    LoginPage,
    RegisterPage,
    TrainingsPage,
    DetailTrainingPage,
    DetailExercisePage,
    PlayTrainingPage,
    TrainingFinishedPage,
    TrainingsPersPage,
    CreateExercisePage,
    ExercisesPersPage,
    AddExTrPage,
    CreateTrainingPage,
    EditExercisePage,
    EditTrainingPage,
    TrainingsPublicsPage,
    UsuariosPage,
    AmigosPage,
    PerfilUserPage,
    MensajesPage,
    RetosPage,
    DetalleRetoPage,
    SuscripcionesPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    ChartModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    NewsPage,
    MenuPage,
    LoginPage,
    RegisterPage,
    TrainingsPage,
    DetailTrainingPage,
    DetailExercisePage,
    PlayTrainingPage,
    TrainingFinishedPage,
    TrainingsPersPage,
    CreateExercisePage,
    ExercisesPersPage,
    AddExTrPage,
    CreateTrainingPage,
    EditExercisePage,
    EditTrainingPage,
    TrainingsPublicsPage,
    UsuariosPage,
    AmigosPage,
    PerfilUserPage,
    MensajesPage,
    RetosPage,
    DetalleRetoPage,
    SuscripcionesPage
  ],
  providers: [
    File,
    FileTransfer,
    FileTransferObject,
    Transfer,
    TransferObject,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoginService,
    UserService,
    TrainingService,
    Camera,
    FilePath,
    RetosService
  ]
})
export class AppModule {}
