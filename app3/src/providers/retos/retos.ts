import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/*
  Generated class for the RetosProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class RetosService {

  user: any;
  pathRetos: string = 'http://192.168.1.5:3000/api/retos';
  pathExercises: string = 'http://192.168.1.5:3000/api/exercises';
  token: any;
  authorization: string;
  opt: RequestOptions;
  myHeaders: Headers;

  constructor(public http: Http,
              public storage: Storage) {
    this.user = null;
    this.storage.get('token').then((value) => {
   
    this.token = value;
     });
  }

  public getRetos() {
     return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                })   
                
                      this.http.get(this.pathRetos,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });   
    }

    public getExercisesReto(reto:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                })   
                
                      this.http.get(this.pathRetos+'/'+reto.id+'/exercises',this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });  
    }

}
