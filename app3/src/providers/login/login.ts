import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LoginService {
  login: any;
  path: string = 'http://192.168.1.5:3000/login';
  
  constructor(public http: Http,
              public storage: Storage) {
    this.login = null;
    
  }

  public postLogin(newLogin: any){
     return new Promise((resolve,reject) => {

      this.http.post(this.path, newLogin)
        .map(res => res.json())
        .subscribe(data => {   
          this.login = data;
          let token = data.token;
          //console.log(data);
          this.storage.set('token', token);
          //console.log(data)   ---lo devuelve bien  
          resolve(data);
        }, error => {
          //console.log(error)
          reject('Error en el login');
        })
    });
  }

}

