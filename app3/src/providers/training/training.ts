import { Injectable } from '@angular/core';
import { Http ,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/*
  Generated class for the TrainingProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TrainingService {

  user: any;
  pathTrainings: string = 'http://192.168.1.5:3000/api/trainings';
  pathTrainingsPers: string = 'http://192.168.1.5:3000/api/trainingsPers';
  pathExercises: string = 'http://192.168.1.5:3000/api/exercises';
  pathUsers: string = 'http://192.168.1.5:3000/api/users';
  pathMets: string = 'http://192.168.1.5:3000/api/mets';
  token: any;
  authorization: string;
  opt: RequestOptions;
  myHeaders: Headers;

  constructor(public http: Http,
              public storage: Storage) {
    console.log('Hello TrainingProvider Provider');
    this.storage.get('token').then((value) => {
      this.token = value;
    });
  }

  getAllTrainings(){
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.get(this.pathTrainings+'Pred',this.opt)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        })
    });    
  }

  getAllTrainingsPublicos(){
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.get(this.pathTrainings+'Publicos',this.opt)
        .map(res => res.json())
        .subscribe(data => {       
          //resolve(JSON.parse(data['_body']));
          resolve(data);
        })
    });    
  }

  getAllTrainingsPers(id:any){
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.post(this.pathTrainingsPers,{ UserId : id },this.opt)
        .map(res => res.json())
        .subscribe(data => {       
          //resolve(JSON.parse(data['_body']));
          resolve(data);
        })
    });    
  }

  getTraining(id:any){
     return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.get(this.pathTrainings+'/'+id,this.opt)
        .map(res => res.json())
        .subscribe(data => {       
          //resolve(JSON.parse(data['_body']));
          resolve(data);
        })
    });   
  }

  getExercisesOf(idTraining: any){
     return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.get(this.pathTrainings+"/"+idTraining+"/exercises",this.opt)
        .map(res => res.json())
        .subscribe(data => {       
          //resolve(JSON.parse(data['_body']));
          resolve(data);
        })
    });    
  }

  getExercise(idExercise:any){
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.get(this.pathExercises+"/"+idExercise,this.opt)
        .map(res => res.json())
        .subscribe(data => {       
          //resolve(JSON.parse(data['_body']));
          resolve(data);
        })
    });    
  }

  getImage(id:any,num:any){
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.get(this.pathExercises+'/'+id+'/images/'+num,this.opt)
        .subscribe(data => {       
          resolve(data);
        })
    });    
  }

  getExerciseInTraining(idTraining:any,idExercise:any){
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.get(this.pathTrainings+"/"+idTraining+"/exercises/"+idExercise,this.opt)
        .map(res => res.json())
        .subscribe(data => {       
          //resolve(JSON.parse(data['_body']));
          resolve(data);
        })
    });    
  }

  getMets(cat:any){
    
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
        this.http.post(this.pathMets, { categoria : cat },this.opt)
        .map(res => res.json())
        .subscribe(data => {      
          resolve(data);
        })
    });    
  }

  getAllMets(){
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
        this.http.get(this.pathMets, this.opt)
        .map(res => res.json())
        .subscribe(data => {      
          resolve(data);
        })
    });    
  }

  createTraining(training:any){
      return new Promise((resolve,reject) => {
            this.myHeaders = new Headers
            let auth: string = 'JWT '+ this.token;
            
            this.myHeaders.append('Authorization', 'JWT '+this.token)
            this.myHeaders.append('Content-Type' , 'application/json')
            
            this.opt = new RequestOptions({
            headers: this.myHeaders
            })   
                this.http.post(this.pathTrainings, training ,this.opt)
                  //.map(res => res.json())
                  .subscribe(data => {       
                    resolve(data);
                  })
            });
  }

  createExercise(exercise:any){
      return new Promise((resolve,reject) => {
            this.myHeaders = new Headers
            let auth: string = 'JWT '+ this.token;
            
            this.myHeaders.append('Authorization', 'JWT '+this.token)
            this.myHeaders.append('Content-Type' , 'application/json')
            
            this.opt = new RequestOptions({
            headers: this.myHeaders
            })   
                this.http.post(this.pathExercises, exercise ,this.opt)
                  //.map(res => res.json())
                  .subscribe(data => {       
                    resolve(data);
                  })
            });
  }

  addToTraining(idtraining:any,exTr:any){
    return new Promise((resolve,reject) => {
            this.myHeaders = new Headers
            let auth: string = 'JWT '+ this.token;
            
            this.myHeaders.append('Authorization', 'JWT '+this.token)
            this.myHeaders.append('Content-Type' , 'application/json')
            
            this.opt = new RequestOptions({
            headers: this.myHeaders
            })   
                this.http.post(this.pathTrainings+'/'+idtraining, exTr ,this.opt)
                 // .map(res => res.json())
                  .subscribe(data => {       
                    resolve(data);
                  })
            });
  } 

  getExercisesByUser(UserId:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                  })   
                      this.http.post(this.pathExercises+'/findByUser', {UserId:UserId} ,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });
    }

    deleteExercise(id:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                  })   
                      this.http.delete(this.pathExercises+'/'+id, this.opt)
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });
    }

    editExercise(exercise:any){
      return new Promise((resolve,reject) => {
            this.myHeaders = new Headers
            let auth: string = 'JWT '+ this.token;
            
            this.myHeaders.append('Authorization', 'JWT '+this.token)
            this.myHeaders.append('Content-Type' , 'application/json')
            
            this.opt = new RequestOptions({
            headers: this.myHeaders
            })   
                this.http.put(this.pathExercises+'/'+exercise.id, exercise ,this.opt)
                 // .map(res => res.json())
                  .subscribe(data => {       
                    resolve(data);
                  })
            });
    }

    editTraining(training:any){
      return new Promise((resolve,reject) => {
            this.myHeaders = new Headers
            let auth: string = 'JWT '+ this.token;
            
            this.myHeaders.append('Authorization', 'JWT '+this.token)
            this.myHeaders.append('Content-Type' , 'application/json')
            
            this.opt = new RequestOptions({
            headers: this.myHeaders
            })   
                this.http.put(this.pathTrainings+'/'+training.id, training ,this.opt)
                 // .map(res => res.json())
                  .subscribe(data => {       
                    resolve(data);
                  })
            });
    }

    uploadImage(exercise:any,num:any,ilustracion:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                  })   
                      this.http.post('http://192.168.1.5:3000/exercises/'+exercise.id+'/'+'images/'+num, ilustracion ,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });
    }

    resetImages(exercise:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                  })   
                      this.http.delete(this.pathExercises+'/'+exercise.id+'/images' ,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });
    }

    comprobarVoto(user:any,training:any){
       return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                  })   
                      this.http.get(this.pathTrainings+'/'+training.id+'/voto/'+user.id ,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });
    }

    votarEntrenamiento(user:any,training:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                  })   
                      this.http.post(this.pathTrainings+'/'+training.id+'/voto/'+user.id ,{},this.opt)
                        //.map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });
    }

    comentarEntrenamiento(user:any,training:any,bodycomment:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                })   
                
                var comentario = {
                    nick: user.nick,
                    UserId: user.id,
                    bodycomment: bodycomment
                };
                      this.http.post(this.pathTrainings+'/'+training.id+'/comments',comentario,this.opt)
                        //.map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });
    }

    getComentarios(training:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                })   
                
                      this.http.get(this.pathTrainings+'/'+training.id+'/comments',this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                  });
    }

    crearEstadistica(training:any,totalTime:any,totalKcal:any,user:any){
          return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                
                var estadistica = {
                  UserId: user.id,
                  TrainingId: training.id,
                  tiempo: totalTime,
                  kcal: totalKcal
                };
                      this.http.post(this.pathUsers+'/'+user.id+'/stats',estadistica,this.opt)
                        //.map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    getSuscripciones(user:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
          
                      this.http.get(this.pathUsers+'/'+user.id+'/suscripciones',this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }
  
      addSuscripcion(user:any,training:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                
                var sus = {
                  TrainingId: training.id,
                };
                      this.http.post(this.pathUsers+'/'+user.id+'/suscripciones',sus,this.opt)
                        //.map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    comprobarSuscripcion(user:any,training:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
            
                      this.http.get(this.pathUsers+'/'+user.id+'/suscripciones/'+training.id,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

}
