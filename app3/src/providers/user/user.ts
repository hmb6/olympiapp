import { Injectable } from '@angular/core';
import { Http ,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UserService {

  user: any;
  pathRegistro: string = 'http://192.168.1.5:3000/register';
  pathUsuarios: string = 'http://192.168.1.5:3000/api/users';
  pathLogros: string = 'http://192.168.1.5:3000/api/logros';
  token: any;
  authorization: string;
  opt: RequestOptions;
  myHeaders: Headers;

  constructor(public http: Http,
              public storage: Storage) {
    this.user = null;
    this.storage.get('token').then((value) => {
   
    this.token = value;
  });
  }

  public findUser(nick: any) {
    return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   
      this.http.post(this.pathUsuarios+'/find',JSON.stringify(nick),this.opt)
        .subscribe(data => {       
          resolve(JSON.parse(data['_body']));
        })
      });    
    }
  
public getAvatar(id: any){
  return new Promise((resolve,reject) => {
      this.myHeaders = new Headers
      let auth: string = 'JWT '+ this.token;
      
      this.myHeaders.append('Authorization', 'JWT '+this.token)
      //this.myHeaders.append('Content-Type' , 'application/json')
      
      this.opt = new RequestOptions({
       headers: this.myHeaders
       })   

      this.http.get(this.pathUsuarios+'/'+id+'/avatar',this.opt)
        .subscribe(data => {       
          resolve(data);
        })
    });
}
  
 public postUser(newUser: any) {
    return new Promise((resolve,reject) => {
      this.http.post(this.pathRegistro, newUser )
        .subscribe(data => {       
          resolve(this.user);
        })
    });
  }

  getEstadisticas(user:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
              
                      this.http.get(this.pathUsuarios+'/'+user.id+'/stats',this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

crearPeso(user:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                
                var peso = {
                  UserId: user.id,
                  peso: user.peso
                };
                      this.http.post(this.pathUsuarios+'/'+user.id+'/pesos',peso,this.opt)
                        //.map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    getPesos(user:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                
                      this.http.get(this.pathUsuarios+'/'+user.id+'/pesos',this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    actualizarUser(user:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                
                
                      this.http.put(this.pathUsuarios+'/'+user.id,user,this.opt)
                        //.map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    getUsers(){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                
                
                      this.http.get(this.pathUsuarios,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    agregarAmigo(user:any,userAmigo:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
               console.log(user);
               let amigo = { UserAgregado: userAmigo.id };
                            
                      this.http.post(this.pathUsuarios+'/'+user.id+'/amigos',amigo,this.opt)
                        //.map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    comprobarAmistad(user:any, userDetail:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                                        
                      
                      this.http.get(this.pathUsuarios+'/'+user.id+'/amigos/'+userDetail.id,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    enviarMensaje(emisor:any,receptor:any,mensaje:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                                        
                   let MG = {
                      nickReceptor: receptor.nick,
                      nickEmisor: emisor.nick,
                      UserEmisor: emisor.id,
                      bodymessage: mensaje
                   };  
                      this.http.post(this.pathUsuarios+'/'+receptor.id+'/mensajes',MG,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    getMensajesEnviados(user:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                                        
                      this.http.get(this.pathUsuarios+'/'+user.id+'/mensajesEnviados',this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    getMensajesRecibidos(user:any){
        return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                                        
                      this.http.get(this.pathUsuarios+'/'+user.id+'/mensajesRecibidos',this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    getLogros(){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                                        
                      this.http.get(this.pathLogros,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    getLogrosUser(user:any){
      return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                                        
                      this.http.get(this.pathUsuarios+'/'+user.id+'/logros',this.opt)
                        .map(res => res.json())
                        .subscribe(data => {       
                          resolve(data);
                        })
                });
    }

    comprobarLogro(user:any,logro:any){
         return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                                        
                      this.http.get(this.pathUsuarios+'/'+user.id+'/logros/'+logro,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {      
                          resolve(data);
                        })
                });
    }

    agregarLogro(user:any,logro:any){
         return new Promise((resolve,reject) => {
                  this.myHeaders = new Headers
                  let auth: string = 'JWT '+ this.token;
                  
                  this.myHeaders.append('Authorization', 'JWT '+this.token)
                  this.myHeaders.append('Content-Type' , 'application/json')
                  
                  this.opt = new RequestOptions({
                  headers: this.myHeaders
                }); 
                let logroConseguido = {
                  LogroId: logro
                }                        
                      this.http.post(this.pathUsuarios+'/'+user.id+'/logros',logroConseguido,this.opt)
                        .map(res => res.json())
                        .subscribe(data => {     
                          resolve(data);
                        })
                });
    }
}