import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,ToastController } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CreateExercisePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-exercise',
  templateUrl: 'create-exercise.html',
})
export class CreateExercisePage {

  //training:any;
  newExercise:any;

  user:any;
  exAux:any;
  mets:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public storage: Storage,
              public toastCtrl: ToastController,
              public events: Events
              ) {
        this.newExercise = {
            nombre: '',
            descripcion: '',
            tipo: '',
            UserId: ''
        };
        
        //this.training = this.navParams.get('training');
        this.storage.get('user').then((data) => {
              this.user = JSON.parse(data);
          }).then(()=>{
                this.newExercise = {
                    nombre: '',
                    descripcion: '',
                    tipo: '',
                    UserId: this.user.id
                };
                /*this.newExTr = {
                    repeticiones: '',
                    series:'',
                    peso:'',
                    tiempo:'',
                    TrainingId:this.training.id,
                    ExerciseId:''
                };*/
          }, (error) => {
                console.log(error);
          });
        this.getAllMets();
        
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateExercisePage');
  }

  private getAllMets(){
        this.trainingService.getAllMets().then((results)=>{
        this.mets = results;
        })
  }

  private createExercise(){
        this.trainingService.createExercise(this.newExercise).then(()=>{
            this.events.publish('reloadExercisesPersPage');
            this.presentToast('Ejercicio creado con éxito');
            this.navCtrl.pop();
        });

  }

  presentToast(text:any) {
      let toast = this.toastCtrl.create({
        message: text,
        duration: 4000,
        position: 'top'
      });
      toast.present();
    }
}
