import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Events} from 'ionic-angular';
import { Storage } from '@ionic/storage';

//paginas
import { HomePage } from '../home/home';
import { TrainingsPage } from '../trainings/trainings';
import { NewsPage } from '../news/news';
import { TrainingsPersPage } from '../trainings-pers/trainings-pers';
import { TrainingsPublicsPage } from '../trainings-publics/trainings-publics'
import { UsuariosPage } from '../usuarios/usuarios';
import { MensajesPage } from '../mensajes/mensajes';
import { RetosPage } from '../retos/retos';
import { SuscripcionesPage } from '../suscripciones/suscripciones';

/**
 * Generated class for the MenuPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  //rootPage = NewsPage;
  rootPage:any;
  user: any;
  pages: any[] = [{
    title: 'Home',
    icon: 'home',
    component: HomePage
  },{
    title: 'Entrenamientos predefinidos',
    icon: 'walk',
    component: TrainingsPage
  },{
    title: 'Entrenamientos personalizados',
    icon: 'walk',
    component: TrainingsPersPage
  },{
    title: 'TOP10 Entrenamientos públicos',
    icon: 'walk',
    component: TrainingsPublicsPage
  },{
    title: 'Lista de usuarios',
    icon: 'walk',
    component: UsuariosPage
  },{
    title: 'Mensajes privados',
    icon: 'walk',
    component: MensajesPage
  },{
    title: 'Retos',
    icon: 'walk',
    component: RetosPage
  },{
    title: 'Suscripciones',
    icon: 'walk',
    component: SuscripcionesPage
  }
  
  ] 

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public storage: Storage,
              public events: Events) {
  this.rootPage = HomePage;
  this.user = this.navParams.get('user');
  console.log(this.user);
    this.storage.set('user', JSON.stringify(this.user)).then((data) => {
      console.log(data);
    });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  openPage(page: any){
    console.log(page);
    this.rootPage = page,{'user':this.user};
    //this.navCtrl.setRoot(page,{ 'user' : this.user});
    
    //control de refresco de Entrenamientos personalizados
    /*this.events.subscribe('reloadTrainingsMenu',() => {
        this.rootPage = NewsPage,{'user':this.user};
        this.rootPage = TrainingsPersPage,{'user':this.user};
      });*/

  }




}
