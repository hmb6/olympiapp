import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events} from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
 
/**
 * Generated class for the AddExTrPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-ex-tr',
  templateUrl: 'add-ex-tr.html',
})
export class AddExTrPage {


  user:any;
  training:any;
  exercise:any;
  newExTr:any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public events: Events) {
          this.user = navParams.get('user');
          this.training = navParams.get('training');
          this.exercise = navParams.get('exercise');  
          this.newExTr = {
                  repeticiones: '',
                  series:'',
                  peso:'',
                  tiempo:'',
                  TrainingId:this.training.id,
                  ExerciseId:this.exercise.id
          };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddExTrPage');
  }

  saveResult(){
    this.trainingService.addToTraining(this.training.id,this.newExTr).then(()=>{
            this.events.publish('itemSelected');
            this.navCtrl.pop();
        });
  }
}
