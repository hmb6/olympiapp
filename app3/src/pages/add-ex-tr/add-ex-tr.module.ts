import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddExTrPage } from './add-ex-tr';

@NgModule({
  declarations: [
    AddExTrPage,
  ],
  imports: [
    IonicPageModule.forChild(AddExTrPage),
  ],
})
export class AddExTrPageModule {}
