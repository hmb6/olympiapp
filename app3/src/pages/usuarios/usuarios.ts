import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../providers/user/user';
import { PerfilUserPage } from '../perfil-user/perfil-user';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the UsuariosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-usuarios',
  templateUrl: 'usuarios.html',
})
export class UsuariosPage {

  user:any;
  userList:any;
  searchQuery: string = '';

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public userService: UserService,
              public storage: Storage
              ) {
      this.storage.get('user').then((data)=>{
        this.user = JSON.parse(data);
      });


      this.getUsers();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsuariosPage');
  }

  getUsers(){
    this.userService.getUsers().then((data)=>{
      this.userList = data;
    })
  }

  goProfile(userDetail:any){
    console.log(userDetail);
    this.navCtrl.push(PerfilUserPage, {
      user:this.user,
      userDetail: userDetail
    });
  }

  getItems(ev: any) {
      // set val to the value of the searchbar
      let val = ev.target.value;

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.userList = this.userList.filter((otherUser) => {
          return (otherUser.nick.toLowerCase().includes(val.toLowerCase()));
        })
      }else{
        this.getUsers();
      }
  }

}
