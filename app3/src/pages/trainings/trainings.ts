import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { DetailTrainingPage } from '../detail-training/detail-training';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the TrainingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainings',
  templateUrl: 'trainings.html',
})
export class TrainingsPage {

  trainings: any;
  user:any; 

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public storage: Storage) {
      this.storage.get('user').then((data)=>{
        this.user = data;
      })
      this.getTrainings();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingsPage');
  }

  private getTrainings(){
    this.trainingService.getAllTrainings().then(data => {
      if(data){
        this.trainings = data;
        console.log('Trainings descargados de la api.');
      }
      else{
        console.log('No hay o no se ha conseguido recoger trainings.');
      }
    })
  }
  

  goDetail(training:any){
    this.navCtrl.push(DetailTrainingPage, {
                                      training: training,
                                      pers: false,
                                      user:this.user});
  }
}
