import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';

/**
 * Generated class for the EditTrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-training',
  templateUrl: 'edit-training.html',
})
export class EditTrainingPage {


  user:any;
  training:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public alertCtrl: AlertController,
              public events: Events) {
        this.user = navParams.get('user');
        this.training = navParams.get('training');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditTrainingPage');
  }
  
  showConfirm(training:any) {
    let confirm = this.alertCtrl.create({
      title: 'Edit',
      message: '¿Seguro que quieres editar el entrenamiento?',
      buttons: [
        {
          text: 'SI',
          handler: () => {
            console.log('SI clicked');
            this.editTraining(training)
          }
        },
        {
          text: 'NO',
          handler: () => {
            console.log('NO clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  editTraining(training:any){
      this.trainingService.editExercise(training).then((data)=>{
      console.log(data);
      this.events.publish('refreshTraining');
      this.navCtrl.pop();
    });
  }
}
