import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditTrainingPage } from './edit-training';

@NgModule({
  declarations: [
    EditTrainingPage,
  ],
  imports: [
    IonicPageModule.forChild(EditTrainingPage),
  ],
})
export class EditTrainingPageModule {}
