import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../providers/user/user';
/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
  providers: [ UserService ]
  
})
export class RegisterPage {
 
  newUser: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public userService: UserService) {
    this.newUser =  {
                        nick: '',
                        email: '',
                        password: '',
                        peso:0.0
                    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  private registerUser(){
      this.userService.postUser( this.newUser );
    }

    private mostrarUser(){
      console.log(this.newUser)
    }
}
