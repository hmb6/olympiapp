import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import { Storage } from '@ionic/storage';
import { TrainingService } from '../../providers/training/training'; 

/**
 * Generated class for the TrainingFinishedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-training-finished',
  templateUrl: 'training-finished.html',
})
export class TrainingFinishedPage {

  user:any;
  training:any;
  totalTime:any;
  totalKcal:any;
  horas:any;
  minutos:any;
  segundos:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public storage: Storage,
              public trainingService: TrainingService) {
      
      this.storage.get('user').then((data) => {
        this.user = JSON.parse(data);
      });
      this.totalKcal = this.navParams.get('totalKcal');
      this.totalTime = this.navParams.get('totalTime');
      this.training = this.navParams.get('training');
      this.formatTime();
      this.totalKcal = Math.round(this.totalKcal * 100)/100;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingFinishedPage');
  }

  formatTime(){
    if(Math.floor((this.totalTime/60)/60)<10){
          this.horas = "0"+(Math.floor((this.totalTime/60)/60)).toString();
        }else{
          this.horas = (Math.floor((this.totalTime/60)/60)).toString();
        }
        
        if(Math.floor((this.totalTime/60)%60)<10){
          this.minutos = "0"+Math.floor((this.totalTime/60)%60).toString();
        }else{
          this.minutos = Math.floor((this.totalTime/60)%60).toString();
        }
        
        if(this.totalTime%60<10){
          this.segundos = "0"+ this.totalTime%60;          
        }else{
          this.segundos = this.totalTime%60;
        }
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: '¿Quieres guardar tu entrenamiento?',
      message: '¡Enhorabuena por finalizar el entrenamiento! ¿Desea guardar el entrenamiento en sus estadísticas?',
      buttons: [
        {
          text: 'SI',
          handler: () => {
            console.log('SI clicked');
            this.guardarEstadistica();
          }
        },
        {
          text: 'NO',
          handler: () => {
            console.log('NO clicked');
            this.navCtrl.setRoot(MenuPage,{
              user:this.user
            });
          }
        }
      ]
    });
    confirm.present();
  }

  guardarEstadistica(){
      this.trainingService.crearEstadistica(this.training,this.totalTime,this.totalKcal,this.user)
      .then((data)=>{
          //mandar toast
          this.navCtrl.setRoot(MenuPage,{
              user:this.user
            });
            
      });
  }
}


