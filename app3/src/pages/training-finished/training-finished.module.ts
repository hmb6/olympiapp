import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingFinishedPage } from './training-finished';

@NgModule({
  declarations: [
    TrainingFinishedPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingFinishedPage),
  ],
})
export class TrainingFinishedPageModule {}
