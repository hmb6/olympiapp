import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { DetailTrainingPage } from '../detail-training/detail-training';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the TrainingsPublicsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainings-publics',
  templateUrl: 'trainings-publics.html',
})
export class TrainingsPublicsPage {
  
  
  trainings: any;
  user:any; 


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public storage: Storage,
              public trainingService: TrainingService) {
        this.storage.get('user').then((data)=>{
          this.user = JSON.parse(data);
        });
        this.trainingService.getAllTrainingsPublicos().then((results) =>{
          this.trainings = results;
        })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingsPublicsPage');
  }

  goDetail(training:any){
    this.navCtrl.push(DetailTrainingPage, {
                                      training: training,
                                      pers: false,
                                      user:this.user});
  }

}
