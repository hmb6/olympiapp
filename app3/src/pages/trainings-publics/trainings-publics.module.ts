import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingsPublicsPage } from './trainings-publics';

@NgModule({
  declarations: [
    TrainingsPublicsPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingsPublicsPage),
  ],
})
export class TrainingsPublicsPageModule {}
