import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingsPersPage } from './trainings-pers';

@NgModule({
  declarations: [
    TrainingsPersPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingsPersPage),
  ],
})
export class TrainingsPersPageModule {}
