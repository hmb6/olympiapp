import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { DetailTrainingPage } from '../detail-training/detail-training';
import { Storage } from '@ionic/storage';
import { CreateTrainingPage } from '../create-training/create-training';


/**
 * Generated class for the TrainingsPersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trainings-pers',
  templateUrl: 'trainings-pers.html',
})
export class TrainingsPersPage {
  
  trainings: any;
  user:any;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public storage: Storage,
              public events: Events) {
        
          this.storage.get('user').then((data) => {
              this.user = JSON.parse(data);
          }).then(()=>{
                console.log(this.user);
                this.getTrainingsPers();
          }, (error) => {
                console.log(error);
          });
        
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingsPersPage');

  }

  private getTrainingsPers(){
    this.trainingService.getAllTrainingsPers(this.user.id).then(data => {
      if(data){
        this.trainings = data;
        console.log('Trainings descargados de la api.');
      }
      else{
        console.log('No hay o no se ha conseguido recoger trainings.');
      }
    })
  }
  goDetail(training:any){
    this.navCtrl.push(DetailTrainingPage, {
                                            training: training,
                                            pers: true,
                                            user:this.user
                                          });
  }

  goCreate(){
    this.navCtrl.push(CreateTrainingPage,{
      user: this.user
    });
    this.events.subscribe('reloadTrainingsPersPage',() => {
        //this.events.publish('reloadTrainingsMenu');
        this.getTrainingsPers();
      });
  }

}
