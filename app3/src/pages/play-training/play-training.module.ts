import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayTrainingPage } from './play-training';

@NgModule({
  declarations: [
    PlayTrainingPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayTrainingPage),
  ],
})
export class PlayTrainingPageModule {}
