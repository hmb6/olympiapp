import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { Observable } from 'rxjs/Rx';
import { File } from '@ionic-native/file';
import { FileTransfer,FileTransferObject } from '@ionic-native/file-transfer';
import { Storage } from '@ionic/storage';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Transfer , TransferObject } from '@ionic-native/transfer';
import { TrainingFinishedPage } from '../training-finished/training-finished';

/**
 * Generated class for the PlayTrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-play-training',
  templateUrl: 'play-training.html',
})
export class PlayTrainingPage {

  training:any;
  exercises: any;
  detailedExercises: any[]=[];
  currentEx:any;
  subscription;
  time = 0;
  totalTime:any;
  totalKcal:any;
  started: boolean = false;
  kcal:any=0.0;
  MET:any;
  user:any;
  lastEx: boolean = false;
  images:any[]=[];

  //cronometro
  horas:any;
  minutos:any;
  segundos:any;

  @ViewChild(Slides) slides: Slides;


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public storage: Storage,
              public file: File,
              public transfer: Transfer) {
     this.storage.get('user').then((data) => {
      this.user = JSON.parse(data);
     });      
    this.training = this.navParams.get('training');
    this.exercises = this.navParams.get('exercises');
    this.detailedExercises = this.navParams.get('detailedExercises');
    this.currentEx = this.navParams.get('currentEx');
    this.totalTime = this.navParams.get('totalTime');
    this.totalKcal = this.navParams.get('totalKcal');
    if(this.currentEx == this.detailedExercises.length-1){
        this.lastEx=true;
    }
    this.trainingService.getMets(this.detailedExercises[this.currentEx].tipo).then(data => {
          if(data){
            this.MET = data;
            console.log('METS descargados.');
          }
          else{
            console.log('No hay o no se ha conseguido recoger METS.');
          }
        });
    const fileTransfer: TransferObject = this.transfer.create();
    if(this.detailedExercises[this.currentEx].img>0){
      for(var i=1;i<=this.detailedExercises[this.currentEx].img;i++){
          fileTransfer.download('http://192.168.1.5:3000/exercises/'+this.detailedExercises[this.currentEx].id+'/images/'+i, this.file.dataDirectory +this.detailedExercises[this.currentEx].id+'-'+i+'.png').then((entry) => {
          this.images.push(entry.toURL());
        }, (error) => {
          console.log(error);
        });
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayTrainingPage');
  }

  startTimer(){
    this.started = true;
    this.subscription = Observable.interval(10).subscribe(x => {
      // the number 1000 is on miliseconds so every second is going to have an iteration of what is inside this code.
        console.log (this.time);
        
        if(Math.floor((this.time/60)/60)<10){
          this.horas = "0"+(Math.floor((this.time/60)/60)).toString();
        }else{
          this.horas = (Math.floor((this.time/60)/60)).toString();
        }
        
        if(Math.floor((this.time/60)%60)<10){
          this.minutos = "0"+Math.floor((this.time/60)%60).toString();
        }else{
          this.minutos = Math.floor((this.time/60)%60).toString();
        }
        
        if(this.time%60<10){
          this.segundos = "0"+ this.time%60;          
        }else{
          this.segundos = this.time%60;
        }
        
        this.time++;
      });
  }

  stopTimer(){
    this.started = false;
    this.subscription.unsubscribe ();
    console.log('El tiempo ha sido: ' + this.time);
  }

  calculateKcal(){ 
    let total = (this.time/60)*(this.MET.mets*0.0175*this.user.peso);
    console.log('Quemadas '+total+' KCAL.');
    return total;
  }

  goNext(){
    this.kcal = this.calculateKcal();
    this.navCtrl.push(PlayTrainingPage,{
        training: this.training,
        exercises: this.exercises,
        detailedExercises: this.detailedExercises,
        currentEx: this.currentEx+1,
        totalTime: this.totalTime+this.time,
        totalKcal:this.totalKcal+this.kcal
    });
  }

  goFinish(){
    this.calculateKcal();
    this.navCtrl.push(TrainingFinishedPage,{
      training: this.training,
      totalTime: this.totalTime+this.time,
      totalKcal: this.totalKcal+this.kcal
    });
  }

   goToSlide() {
    this.slides.slideTo(2, 500);
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
  }
}
