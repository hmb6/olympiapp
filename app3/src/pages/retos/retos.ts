import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RetosService } from '../../providers/retos/retos';
import { Storage } from '@ionic/storage';
import { DetalleRetoPage } from '../detalle-reto/detalle-reto';

/**
 * Generated class for the RetosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-retos',
  templateUrl: 'retos.html',
})
export class RetosPage {

  user:any;
  retos:any;
  urlImages:any[]=[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public retosService: RetosService,
              public storage: Storage) {

        this.storage.get('user').then((data) => {
      this.user = JSON.parse(data);
    });
        this.retosService.getRetos().then((data)=>{
          console.log(data);
          this.retos = data;
          for(let i of this.retos){
              this.urlImages.push("./assets/img/retos/"+i.id+".png");
          }
        });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RetosPage');
  }

  goDetalleReto(reto:any, imgUrl:any){
    this.navCtrl.push(DetalleRetoPage,{
      user: this.user,
      reto: reto,
      imgUrl: imgUrl
    });
  }
}
