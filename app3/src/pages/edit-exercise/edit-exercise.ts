import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Events, AlertController} from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { Camera } from 'ionic-native';
/**
 * Generated class for the EditExercisePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-exercise',
  templateUrl: 'edit-exercise.html',
})
export class EditExercisePage {
  
  
  user:any;
  exercise:any;
  mets:any;
  base64Image:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public events: Events,
              public alertCtrl: AlertController,
              public trainingService: TrainingService) {
        this.user = navParams.get('user');
        this.exercise = navParams.get('exercise');
        this.getAllMets();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditExercisePage');
  }

getAllMets(){
    this.trainingService.getAllMets().then((results)=>{
      this.mets = results;
    })
}

editExercise(exercise:any){
    this.trainingService.editExercise(exercise).then((data)=>{
      console.log(data);
      this.events.publish('refreshExercises');
      this.navCtrl.pop();
    });
  }

  showConfirm(exercise:any) {
    let confirm = this.alertCtrl.create({
      title: 'Edit',
      message: '¿Seguro que quieres editar el ejercicio?',
      buttons: [
        {
          text: 'SI',
          handler: () => {
            console.log('SI clicked');
            this.editExercise(exercise)
          }
        },
        {
          text: 'NO',
          handler: () => {
            console.log('NO clicked');
          }
        }
      ]
    });
    confirm.present();
  }

}
