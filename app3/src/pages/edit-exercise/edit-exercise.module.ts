import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditExercisePage } from './edit-exercise';

@NgModule({
  declarations: [
    EditExercisePage,
  ],
  imports: [
    IonicPageModule.forChild(EditExercisePage),
  ],
})
export class EditExercisePageModule {}
