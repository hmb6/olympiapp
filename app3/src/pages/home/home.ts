import { Component, ViewChild} from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { UserService } from '../../providers/user/user'
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { FileTransfer,FileTransferObject } from '@ionic-native/file-transfer';
import { Transfer , TransferObject } from '@ionic-native/transfer';
import { Chart } from 'ng2-chartjs2';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ UserService ]
})
export class HomePage {

  
  @ViewChild('lineCanvas') lineCanvas;
  
  
  user: any;
  avatarUrl: any;
  
  //estadisticas de entrenamiento
  estadisticas:any;
  tiempos: any[]=[];
  fechas:any[]=[];
  kcal:any[]=[];

  tiempodata: Chart.Dataset[];
  tiempolabels: string[];
  kcaldata: Chart.Dataset[];
  kcallabels: string[];

  //estadisticas de graficaPesos
  estadisticaPesos:any;
  fechasPesos:any[]=[];
  pesos:any[]=[];

  pesosdata: Chart.Dataset[];
  pesoslabels: string[];

  urlLogros: any[]=[];
  urlLogrosFalse:any[]=[];

  //logros
  logros:any;
  logrosObtennidos:any;
  tieneLogros:any[]=[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public userService: UserService,
              public storage: Storage,
              public file: File,
              public transfer: Transfer
              ) {
    //const fileTransfer: FileTransferObject = this.transfer.create();   
    const fileTransfer: TransferObject = this.transfer.create();
    this.storage.get('user').then((data) => {
      this.user = JSON.parse(data);
    }).then(()=>{
      fileTransfer.download('http://192.168.1.5:3000/avatar/'+this.user.id, this.file.dataDirectory + this.user.id +'.png').then((entry) => {
      this.avatarUrl= entry.toURL();
      }, (error) => {
        console.log(error);
      });
    }).then(()=>{
            this.userService.getLogros().then((data)=>{
            this.logros = data;
            for(let log of this.logros){
              this.urlLogros.push("./assets/img/logros/"+log.id+"-true.png");
              this.urlLogrosFalse.push("./assets/img/logros/"+log.id+"-false.png");
            }
          });
          this.userService.getLogrosUser(this.user).then((data)=>{
            this.logrosObtennidos = data;
          }).then(()=>{
            let aux = false;
            for(let i=0;i<this.logros.length;i++){
              aux=false;
              for(let j=0;j<this.logrosObtennidos.length;j++){
                if(this.logros[i].id == this.logrosObtennidos[j].LogroId)
                {
                  this.tieneLogros.push(true);
                  aux=true;
                  break;
                }
              }
              if(aux == false)
              {
                this.tieneLogros.push(false);
              }
            }
          });
    })  
    .then(()=>{
      this.getTodo();
    }).then(()=>{
      this.graficaTiempos();
      this.graficaKcal();
      this.graficaPesos();
    });

    
  }

  getTodo(){
    this.userService.getEstadisticas(this.user).then((data)=>{
      console.log(data);
      this.estadisticas = data;
    }).then(()=>{
      var aux = '';
      for(let e of this.estadisticas){
        aux = e.createdAt.toString(); 
        this.fechas.push(aux.substring(0,10));
        this.tiempos.push(e.tiempo/60);
        this.kcal.push(e.kcal);
      }      
    })
    .then(()=>{
      this.userService.getPesos(this.user).then((data)=>{
        console.log(data);
        this.estadisticaPesos = data;
      }).then(()=>{
        var auxPesos = '';
      for(let p of this.estadisticaPesos){
        console.log(p);
        auxPesos = p.createdAt.toString(); 
        this.fechasPesos.push(auxPesos.substring(0,10));
        this.pesos.push(p.peso);
      }
      })
    });
  }

  graficaTiempos(){
    this.tiempolabels = this.fechas;
    this.tiempodata = [
      {
        label: 'Tiempo(min) por entrenamiento',
        //data: [12, 19, 3, 5, 2, 3],
        data: this.tiempos,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }
    ];
  }

  graficaKcal(){
    //this.labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];
    this.kcallabels = this.fechas;
    this.kcaldata = [
      {
        label: 'Kcal quemadas por entrenamiento',
        //data: [12, 19, 3, 5, 2, 3],
        data: this.kcal,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }
    ];
  }

  graficaPesos(){
    this.pesoslabels = this.fechasPesos;
    this.pesosdata = [
      {
        label: 'Evolución de peso',
        //data: [12, 19, 3, 5, 2, 3],
        data: this.pesos,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }
    ];
  }

  actualizarPeso(){
    this.userService.crearPeso(this.user);
    console.log(this.user);
    this.userService.actualizarUser(this.user);

    //añadir toast
    
  }
}
