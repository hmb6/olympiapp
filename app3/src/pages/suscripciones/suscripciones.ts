import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { DetailTrainingPage } from '../detail-training/detail-training';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SuscripcionesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-suscripciones',
  templateUrl: 'suscripciones.html',
})
export class SuscripcionesPage {


  suscripciones:any;
  trainingsSuscritos:any[] = [];
  user:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              public trainingService: TrainingService) {

          this.storage.get('user').then((data) => {
              this.user = JSON.parse(data);
          }).then(()=>{
                console.log(this.user);
                this.getSuscripciones();
          }, (error) => {
                console.log(error);
          });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuscripcionesPage');
  }

  getSuscripciones(){
      this.trainingService.getSuscripciones(this.user).then((data)=>{
          this.suscripciones = data;
          for(let training of this.suscripciones){
            this.trainingService.getTraining(training.TrainingId).then((data)=>{
              this.trainingsSuscritos.push(data);
            });
          }
      });
  }

  goDetail(training:any){
    this.navCtrl.push(DetailTrainingPage, {
                                            training: training,
                                            pers: false,
                                            user:this.user
                                          });
  }


}
