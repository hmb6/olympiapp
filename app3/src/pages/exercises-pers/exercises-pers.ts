import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController,Events, AlertController} from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { Storage } from '@ionic/storage';
import { CreateExercisePage } from '../create-exercise/create-exercise';
import { AddExTrPage } from '../add-ex-tr/add-ex-tr';
import { EditExercisePage } from '../edit-exercise/edit-exercise';
import { DetailExercisePage } from '../detail-exercise/detail-exercise';

/**
 * Generated class for the ExercisesPersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-exercises-pers',
  templateUrl: 'exercises-pers.html',
})
export class ExercisesPersPage {
  user:any;
  exercises:any;

  training:any;

  onlyDetail: boolean;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public storage: Storage,
              public viewController: ViewController,
              public alertCtrl: AlertController,
              public events: Events) {
          this.onlyDetail = navParams.get('onlyDetail');
          this.training = navParams.get('training');
          this.storage.get('user').then((data) => {
            this.user = JSON.parse(data);
          }).then(()=>{ 
               this.getExercises();
          });
          

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExercisesPersPage');
    
  }
  getExercises(){
    this.trainingService.getExercisesByUser(this.user.id).then(data => {
                if(data){
                  this.exercises = data;
                  console.log('Exercises personalizados descargados de la api.');
                }
                else{
                  console.log('No hay o no se ha conseguido recoger exercises.');
                }
              });
  }
  goCreate(){
      this.navCtrl.push(CreateExercisePage);
      this.events.subscribe('reloadExercisesPersPage',() => {
        this.getExercises();
      });
  }

  goDetailExercise(detailedExercise:any){
        let exercise = null;

        this.trainingService.getExerciseInTraining(this.training.id,detailedExercise.id)
        .then(data => {
          exercise = data;
        });
        
        this.navCtrl.push(DetailExercisePage,{
          exercise: detailedExercise,
          exerciseCustom: {
            series: "",
            repeticiones: "",
            peso: "",
            tiempo: ""
          },
          enEntrenamiento: false,
          pers:true
        });
    }

  selectExercise(exercise:any){
      this.navCtrl.push(AddExTrPage,{
        exercise:exercise,
        training:this.training,
        user:this.user
      });

      this.events.subscribe('itemSelected',() => {
        this.events.publish('reloadDetailTrainingPage');
        this.viewController.dismiss();
      });

  }

  goEdit(exercise:any){
      this.navCtrl.push(EditExercisePage,{
        exercise:exercise,
        user: this.user
      });
  }

  deleteExercise(id:any){
    console.log('La id es: '+ id);
    this.trainingService.deleteExercise(id).then((data)=>{
      console.log(data);
      this.getExercises();
    });
  }

  showDelete(id:any) {
    let confirm = this.alertCtrl.create({
      title: 'DELETE',
      message: '¿Seguro que quieres borrar el ejercicio?',
      buttons: [
        {
          text: 'SI',
          handler: () => {
            console.log('SI clicked');
            this.deleteExercise(id)
          }
        },
        {
          text: 'NO',
          handler: () => {
            console.log('NO clicked');
            
          }
        }
      ]
    });
    confirm.present();
  }

}
