import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExercisesPersPage } from './exercises-pers';

@NgModule({
  declarations: [
    ExercisesPersPage,
  ],
  imports: [
    IonicPageModule.forChild(ExercisesPersPage),
  ],
})
export class ExercisesPersPageModule {}
