import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RetosService } from '../../providers/retos/retos';
import { TrainingService } from '../../providers/training/training';
import { DetailExercisePage } from '../detail-exercise/detail-exercise';
/**
 * Generated class for the DetalleRetoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalle-reto',
  templateUrl: 'detalle-reto.html',
})
export class DetalleRetoPage {

  user: any;
  reto:any;
  imgUrl:any;

  exercises:any;
  detailedExercises:any[]=[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public retosService: RetosService,
              public trainingService: TrainingService) {
        this.user=navParams.get('user');
        this.reto=navParams.get('reto');
        this.imgUrl=navParams.get('imgUrl');

        this.retosService.getExercisesReto(this.reto).then((data)=>{
          this.exercises = data;
          for(var e of this.exercises){
            this.getExercise(e.ExerciseId);
          }
        });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalleRetoPage');
  }

  private getExercise(idExercise:any){
    this.trainingService.getExercise(idExercise).then(data => {
      if(data){
        console.log('Resultado de getExercise');
         console.log(data);
        this.detailedExercises.push(data);
      }
      else{
        console.log('No hay o no se ha conseguido recoger exercises.');
      }
    })
  }

  goDetailExercise(detailedExercise:any){
 
    this.navCtrl.push(DetailExercisePage,{
            exercise: detailedExercise,
            exerciseCustom: {
              repeticiones: 0,
              series: 0,
              peso: "",
              tiempo: 0
            },
            enEntrenamiento: false,
            pers: false
          });
    
  }

}
