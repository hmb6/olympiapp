import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events,ToastController } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
/**
 * Generated class for the CreateTrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-training',
  templateUrl: 'create-training.html',
})
export class CreateTrainingPage {

  user:any;
  newTraining:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public toastCtrl: ToastController,
              public events: Events) {
          this.user=navParams.get('user')
          this.newTraining = {
            nombre: '',
            descripcion: '',
            publico: '',
            dificultad: '',
            UserId: this.user.id
          }
          
          
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateTrainingPage');
  }

  createTraining(){
    this.trainingService.createTraining(this.newTraining).then(()=>{
        this.events.publish('reloadTrainingsPersPage');
        this.presentToast('Entrenamiento creado con éxito.');
        this.navCtrl.pop();
    });
  }

  presentToast(text:any) {
      let toast = this.toastCtrl.create({
        message: text,
        duration: 4000,
        position: 'top'
      });
      toast.present();
    }
}
