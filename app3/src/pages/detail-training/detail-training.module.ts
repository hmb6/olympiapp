import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailTrainingPage } from './detail-training';

@NgModule({
  declarations: [
    DetailTrainingPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailTrainingPage),
  ],
})
export class DetailTrainingPageModule {}
