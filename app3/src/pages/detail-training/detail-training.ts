import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController,Events,ToastController} from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { DetailExercisePage } from '../detail-exercise/detail-exercise';
import { PlayTrainingPage } from '../play-training/play-training';
import { ExercisesPersPage } from '../exercises-pers/exercises-pers';
import { EditTrainingPage } from '../edit-training/edit-training';


/**
 * Generated class for the DetailTrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-training',
  templateUrl: 'detail-training.html',
})
export class DetailTrainingPage {

  training: any;
  exercises: any;
  detailedExercises: any[]=[];
  pers: boolean;
  vacio: boolean = true;
  user:any;
  votado:any;
  propietario:any;
  comentarios:any;
  bodycomment:any;
  comentable:boolean=true;
  fav:boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public modalCtrl: ModalController,
              public events: Events,
              public toastCtrl: ToastController) {
      this.user = navParams.get('user');
      this.pers= navParams.get('pers');
      this.training = navParams.get('training');
      this.bodycomment='';
      if(this.training.UserId==this.user.id){
        this.propietario=true;
      }else{
        this.propietario=false;
      }
      this.getExercisesOf(this.training.id).then(data => {
        for(var e of this.exercises){
            this.getExercise(e.ExerciseId);
        }
      }).then(()=>{
        if(this.exercises.length>0)
          this.vacio = false;
        else
          this.vacio = true;
          
        if(this.training.publico!="pr"){
          this.trainingService.comprobarVoto(this.user,this.training).then((data)=>{
          this.votado = data;});
        }else{
          this.votado = true;
          this.comentable = false;
        }
          
          console.log(this.vacio);
          this.getComentarios();
      });
      this.comprobarSuscripcion();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailTrainingPage');
  }

  private getExercisesOf(idTraining: any){
    return new Promise((resolve,reject) => {
        this.trainingService.getExercisesOf(idTraining).then(data => {
          if(data){
            this.exercises = data;
            console.log('Exercises descargados de la api.');   
            resolve(data); 
          }
          else{
            console.log('No hay o no se ha conseguido recoger exercises.');
            reject('No hay o no se ha conseguido recoger exercises.');
          }
        });
      });
  }

private getExercise(idExercise:any){
    this.trainingService.getExercise(idExercise).then(data => {
      if(data){
        console.log('Resultado de getExercise');
         console.log(data);
        this.detailedExercises.push(data);
      }
      else{
        console.log('No hay o no se ha conseguido recoger exercises.');
      }
    })
  }

  goDetailExercise(detailedExercise:any){
      let exercise = null;

      this.trainingService.getExerciseInTraining(this.training.id,detailedExercise.id)
      .then(data => {
        exercise = data;
      }).then(()=>{
          this.navCtrl.push(DetailExercisePage,{
                  exercise: detailedExercise,
                  exerciseCustom: exercise,
                  enEntrenamiento: true,
                  pers:this.pers
                });
      });
      
      
  }

  goPlayTraining(training:any,exercises:any,detailedExercises:any){
    this.navCtrl.push(PlayTrainingPage,{
        training: this.training,
        exercises: this.exercises,
        detailedExercises: this.detailedExercises,
        currentEx: 0,
        totalTime: 0,
        totalKcal:0.0
    });
  }

  goEdit(){
    this.navCtrl.push(EditTrainingPage,{
      user: this.user,
      training: this.training
    })
  }

  vote(){
    this.training.votos = parseInt(this.training.votos)+1;
    this.trainingService.editTraining(this.training).then(()=>{
        this.votado = true;
        console.log(this.training);
        this.trainingService.votarEntrenamiento(this.user,this.training);
        this.presentToast('Has votado el entrenamiento!');
    });
  }

  goExercises(){
    let chooseModal = this.modalCtrl.create(ExercisesPersPage,{training:this.training});
   chooseModal.onDidDismiss(data => {
     console.log(data);
   });
   chooseModal.present();

   this.events.subscribe('reloadDetailTrainingPage',() => {
        /*this.navCtrl.pop().then(()=>{
          this.navCtrl.push(DetailTrainingPage,{
            pers:this.pers,
          get  training:this.training
          });
        });*/
        this.getExercisesOf(this.training.id).then(data => {
          for(var e of this.exercises){
              this.getExercise(e.ExerciseId);
          }
        }).then(()=>{
          if(this.exercises.length>0)
            this.vacio = false;
          else
            this.vacio = true;
        });
        this.presentToast('Ejercicio añadido con éxito');
      });
  }

  getComentarios(){
    this.trainingService.getComentarios(this.training).then((results)=>{
      this.comentarios = results;
    })
  }

  createComment(){
    this.trainingService.comentarEntrenamiento(this.user,this.training,this.bodycomment).then(()=>{
      this.bodycomment = '';
      this.presentToast('Se ha comentado con éito.');
      this.getComentarios();
    })
  }

  suscribirse(){
    this.trainingService.addSuscripcion(this.user,this.training).then((data)=>{
        this.fav=true;
        let toast = this.toastCtrl.create({
          message: 'Te has suscrito con éxito.',
          duration: 3000
        });
        toast.present();
    });
  }

  comprobarSuscripcion(){
    this.trainingService.comprobarSuscripcion(this.user,this.training).then((data)=>{
      if(data==true){
        this.fav = true;
      }
      else{
        this.fav = false;
      }
    })
  }


  presentToast(text:any) {
      let toast = this.toastCtrl.create({
        message: text,
        duration: 4000,
        position: 'top'
      });
      toast.present();
    }

}
