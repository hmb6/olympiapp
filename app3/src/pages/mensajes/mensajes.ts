import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UserService } from '../../providers/user/user'

/**
 * Generated class for the MensajesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mensajes',
  templateUrl: 'mensajes.html',
})
export class MensajesPage {

  user:any;
  mensajesEnviados:any;
  mensajesRecibidos:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public storage: Storage,
              public userService: UserService) {
      this.storage.get('user').then((data) => {
          this.user = JSON.parse(data);
    }).then(()=>{
      this.userService.getMensajesEnviados(this.user).then((data)=>{
        this.mensajesEnviados = data;
      })}) 
      .then(()=>{
          this.userService.getMensajesRecibidos(this.user).then((data)=>{
            this.mensajesRecibidos = data;
        });
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MensajesPage');
  }

}
