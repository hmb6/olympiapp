import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailExercisePage } from './detail-exercise';

@NgModule({
  declarations: [
    DetailExercisePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailExercisePage),
  ],
})
export class DetailExercisePageModule {}
