import { Component } from '@angular/core';
import { IonicPage,LoadingController,AlertController, NavController, NavParams,ActionSheetController,ToastController } from 'ionic-angular';
import { TrainingService } from '../../providers/training/training';
import { File } from '@ionic-native/file';
import { FileTransfer,FileTransferObject } from '@ionic-native/file-transfer';
import { Transfer , TransferObject } from '@ionic-native/transfer';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { FilePath } from '@ionic-native/file-path';

/**
 * Generated class for the DetailExercisePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-exercise',
  templateUrl: 'detail-exercise.html',
})
export class DetailExercisePage {

  exercise:any;
  exerciseCustom:any;
  images:any[]=[];
  enEntrenamiento: boolean;
  base64Image:any;
  pers:any;
  lastImage: string = null;
  loading:any;

  rutaCompleta:any;
  @ViewChild(Slides) slides: Slides;


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public trainingService: TrainingService,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public file: File,
              public actionSheetCtrl: ActionSheetController,
              public toastCtrl: ToastController,
              public camera: Camera,
              public filePath: FilePath,
             // public transfer: FileTransfer
              public transfer: Transfer) {
    console.log('LLEGAMOS AQUI??');
    this.enEntrenamiento = navParams.get('enEntrenamiento');
    this.exercise = navParams.get('exercise');
    this.exerciseCustom = navParams.get('exerciseCustom');
    this.pers = navParams.get('pers');
    const fileTransfer: TransferObject = this.transfer.create();
    this.updateExerciseInfo();
    if(this.enEntrenamiento){
        console.log(this.exerciseCustom);
    }
    
    if(this.exercise.img>0){
      for(var i=1;i<=this.exercise.img;i++){
          fileTransfer.download('http://192.168.1.5:3000/exercises/'+this.exercise.id+'/images/'+i, this.file.dataDirectory +this.exercise.id+'-'+i+'.png').then((entry) => {
          this.images.push(entry.toURL());
        }, (error) => {
          console.log(error);
        });
      }
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailExercisePage');
  }

  goToSlide() {
    this.slides.slideTo(2, 500);
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
  }

  updateExerciseInfo(){
    this.trainingService.getExercise(this.exercise.id).then((data) =>{
      this.exercise = data;
    });
  }

  getImages(){
     const fileTransfer: TransferObject = this.transfer.create();
    for(var i=1;i<=this.exercise.img;i++){
          fileTransfer.download('http://192.168.1.5:3000/exercises/'+this.exercise.id+'/images/'+i, this.file.dataDirectory +'ejercicio-'+i+'.png').then((entry) => {
          this.images.push(entry.toURL());
        }, (error) => {
          console.log(error);
        });
      }
  }

  public accionesImagen() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(Camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(Camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType:any){
       // Create options for the Camera Dialog
        var options = {
          quality: 100,
          sourceType: sourceType,
          saveToPhotoAlbum: false,
          correctOrientation: true
        };
      
        // Get the data of an image
        Camera.getPicture(options).then((imagePath) => {
          // Special handling for Android library
          if (sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath)
              .then(filePath => {
                let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
              });
          } else {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            
          }
          
        }, (err) => {
          this.presentToast('Error while selecting image.');
        });
    }

    /*uploadImage(num:any,ilustracion:any){
      this.trainingService.uploadImage(this.exercise,num,ilustracion).then((data)=>{
        console.log(data);
      });
    }*/

    createFileName() {
      var d = new Date(),
      n = d.getTime(),
      newFileName =  n + ".jpg";
      return newFileName;
    }
    
    
    copyFileToLocalDir(namePath:any, currentName:any, newFileName:any) {
      this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.lastImage = newFileName;
        this.rutaCompleta = this.file.dataDirectory+'/'+newFileName
      }, error => {
        this.presentToast('Error while storing file.');
      }).then(()=>{
      
            let confirm = this.alertCtrl.create({
              title: 'Subir imagen',
              message: '¿Seguro que quieres subir la imagen?',
              buttons: [
                {
                  text: 'SI',
                  handler: () => {
                    console.log('SI clicked');
                    this.uploadImage();
                  }
                },
                {
                  text: 'NO',
                  handler: () => {
                    console.log('NO clicked');
                  }
                }
              ]
            });
            confirm.present();
        });
    }
    
    presentToast(text:any) {
      let toast = this.toastCtrl.create({
        message: text,
        duration: 6000,
        position: 'top'
      });
      toast.present();
    }
    
    public pathForImage(img:any) {
      if (img === null) {
        return '';
      } else {
        return this.file.dataDirectory + img;
      }
    }

    public uploadImage() {
      // Destination URL
      var url = "http://192.168.1.5:3000/exercises/"+this.exercise.id+"/images/"+(parseInt(this.exercise.img)+1);
    
      // File for Upload
      var targetPath = this.pathForImage(this.lastImage);
    
      // File name only
      var filename = this.lastImage;
    
      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'fileName': filename}
      };
    
      const fileTransfer: TransferObject = this.transfer.create();
    
      this.loading = this.loadingCtrl.create({
        content: 'Uploading...',
      });
      this.loading.present();
    
      // Use the FileTransfer to upload the image
      fileTransfer.upload(this.rutaCompleta, url, options).then(data => {
        this.loading.dismissAll();
        this.exercise.img = this.exercise.img+1;
        this.trainingService.editExercise(this.exercise).then(()=>{
          this.getImages();
        });
        this.presentToast('Image succesful uploaded.');
      }, err => {
        console.log(err);
        this.loading.dismissAll()
        this.presentToast('Error while uploading file.');
      });
    }

    showReset(){
      let confirm = this.alertCtrl.create({
              title: 'Borrar imagenes',
              message: '¿Seguro que quieres borrar las imagenes?',
              buttons: [
                {
                  text: 'SI',
                  handler: () => {
                    console.log('SI clicked');
                    this.resetImages();
                  }
                },
                {
                  text: 'NO',
                  handler: () => {
                    console.log('NO clicked');
                  }
                }
              ]
            });
            confirm.present();
    }

    resetImages(){
      this.trainingService.resetImages(this.exercise).then(()=>{
          this.trainingService.getExercise(this.exercise.id).then((data)=>{
            this.exercise = data;
            this.images = [];
          });
      });
    }
}
