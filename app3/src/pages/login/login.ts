import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { MenuPage } from '../menu/menu';
import { LoginService } from '../../providers/login/login';
import { UserService } from '../../providers/user/user';

import { Storage } from '@ionic/storage';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [ LoginService , UserService]
})
export class LoginPage {

  newLogin: any;
  userLogged: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams
              ,
              public loginService: LoginService,
              public userService: UserService,
              public storage: Storage) {
               this.newLogin = {
                          nick: '',
                          password: ''
                        }
  
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goRegister(){
    this.navCtrl.push(RegisterPage);
  }

  private login(){
    this.loginService.postLogin(this.newLogin).then(data => {
      if(data){
        //console.log(this.newLogin.nick);
        this.userService.findUser({nick: this.newLogin.nick}).then(response => {
          this.goMenu(response);
        })  
      }
      else{
        this.goRegister();
      }
    })
  }

 public goMenu(user: any){
    this.navCtrl.setRoot(MenuPage,{user: user});
  }

}
